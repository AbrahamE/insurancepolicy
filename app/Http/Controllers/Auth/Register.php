<?php

namespace App\Http\Controllers\Auth;


use App\Abstracts\Http\Controller;
use App\Models\Auth\User;
use App\Http\Requests\Auth\Register as Request;
use App\Http\Requests\Banking\Account;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Jobs\Auth\CreateUser;
use Artisan;
use App\Traits\Jobs;
use App\Jobs\Common\CreateCompany;
use App\Jobs\Banking\CreateAccount;
use Setting;

class Register extends Controller
{
  use AuthenticatesUsers;
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest', ['except' => 'destroy']);
  }

  public function create()
  {
    return view('auth.login.register');
  }


  private function createCompany()
  {
  
    $company = $this->dispatch(new CreateCompany([
      'name' => 'My Company',
      'email' => 'my@company.com',
      'domain' => 'company.com',
      'address' => 'New Street 1254',
      'currency' => 'USD',
      'locale' => 'en-ES',
      'enabled' => '1',
      'settings' => [
        'schedule.send_invoice_reminder' => '1',
        'schedule.send_bill_reminder' => '1',
        'wizard.completed' => '0',
        'email.protocol' => 'array',
            
           
      ],

    ]));
    $company->makeCurrent(true);
           
  }


  public function register(Request $request)
  {

    //Verificar Si el usuario ya se encuentra registrado

    $user = User::where('email',  $request->input('email'))->count();

    if ($user > 0) {
      return response()->json([
        'status' => null,
        'success' => false,
        'error' => true,
        'message' => trans('auth.error.user_exists'),
        'data' => null,
        'redirect' => null,
      ]);
    }

    $this->createCompany();

    $user = $this->dispatch(new CreateUser(array_merge(
      $request->only([
        "name",
        "email",
        "password",

      ]),
      [
        'companies' => [company_id()],
        'roles' => ['1'],
        'enabled' => '1',
        'locale' => env("APP_LOCALE"),

      ]
    )));

    $this->modulesInstall();

    return response()->json([
      'status' => null,
      'success' => true,
      'message' => trans('auth.success_to.success_register'),
      'data' => null,
      'redirect' => '/',
    ]);
  }
 
  public function modulesInstall()
  {

    Artisan::call('module:install', [
      'alias'     => 'inventory',
      'company'   => company_id(),
      'locale'    => session('locale', company(company_id())->locale),
    ]);

    Artisan::call('module:install', [
      'alias'     => 'double-entry',
      'company'   => company_id(),
      'locale'    => session('locale', company(company_id())->locale),
    ]);

    Artisan::call('module:install', [
      'alias'     => 'employees',
      'company'   => company_id(),
      'locale'    => session('locale', company(company_id())->locale),
    ]);

    
     Artisan::call('module:install', [
       'alias'     => 'pos',
       'company'   => company_id(),
       'locale'    => session('locale', company(company_id())->locale),
     ]);
  }
}
