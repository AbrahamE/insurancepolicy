<?php

namespace App\Http\Controllers\Policy;

use App\Abstracts\Http\Controller;
use App\Exports\Sales\Invoices as Export;
use App\Http\Requests\Common\Import as ImportRequest;
// use App\Http\Requests\Document\DocumentPolicy as Request;
use App\Imports\Sales\Invoices as Import;
use App\Jobs\Document\CreateDocument;
use App\Jobs\Document\DeleteDocument;
use App\Jobs\Document\DuplicateDocument;
use App\Jobs\Document\UpdateDocument;
use App\Models\Document\DocumentPolicy;
// use App\Models\Document\Document;
use App\Notifications\Sale\Invoice as Notification;
use App\Traits\Documents;
use App\Utilities\InvoicePrint;
use Illuminate\Http\Request;

// use App\Notifications\Sale\Invoice as Notification;

class Document extends Controller
{
    use Documents;

    public function __construct()
    {
        $this->middleware('admin')->only(['index', 'store']);
    }

    public function index()
    {
        $policy = DocumentPolicy::all();

        return view('portal.policy.index', compact('policy'));
    }


    public function create()
    {
        return view('portal.policy.create');
    }

    public function edit(DocumentPolicy $document)
    {
        $document->spouse = (array) json_decode($document->spouse, true);
        $document->dependents = parse_json_policy($document->dependents);
        $document->income = parse_json_policy($document->income);
        $document->bank = (array) json_decode($document->bank, true);
        $document->credit_bank = (array) json_decode($document->credit_bank, true);

        return view('portal.policy.update', compact('document'));
    }

    public function update(Request $request, DocumentPolicy $document)
    {
        $response = [];
        try {
            if ($document->update($request->all())) {
                $response['success'] = true;

                // $response['redirect'] = route('invoices.show', $response['data']->id);
                $response['redirect'] = route('policys.list');

                $message = trans('messages.success.added', ['type' => trans_choice('general.invoices', 1)]);

                flash($message)->success();
            } else {
                $response['redirect'] = route('policys.create');

                $message = $response['message'];

                flash($message)->error()->important();
            }
        } catch (\Throwable $th) {
            $response['redirect'] = route('policys.create');

            $message = $th['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }

    public function store(Request $request)
    {
        $response = $this->ajaxDispatch(new CreateDocument($request));

        try {
            if ($response['success']) {
                // $response['redirect'] = route('policys.list', $response['data']->id);
                $response['redirect'] = route('policys.list');

                $message = trans('messages.success.added', ['type' => trans_choice('general.invoices', 1)]);

                flash($message)->success();
            } else {
                $response['redirect'] = route('policys.create');

                $message = $response['message'];

                flash($message)->error()->important();
            }
        } catch (\Throwable $th) {
            $response['redirect'] = route('policys.create');

            $message = $th['message'];

            flash($message)->error()->important();
        }

        return response()->json($response);
    }
}
