<?php

namespace App\Http\Requests\Document;

use App\Abstracts\Http\FormRequest;
use App\Models\Document\Document as Model;
use App\Utilities\Date;
use Illuminate\Support\Str;

class DocumentPolicy extends FormRequest
{
    protected $items_quantity_size = [];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $company_logo = 'nullable';
        // $attachment = 'nullable';

        $type = $this->request->get('type', Model::INVOICE_TYPE);

        $type = config('type.' . $type . '.route.parameter');

        // // Check if store or update
        // if ($this->getMethod() == 'PATCH') {
        //     $model = $this->isApi() ? 'document' : $type;

        //     $id = is_numeric($this->$model) ? $this->$model : $this->{$model}->getAttribute('id');
        // } else {
        //     $id = null;
        // }

        // if ($this->files->get('company_logo')) {
        //     $company_logo = 'mimes:' . config('filesystems.mimes') . '|between:0,' . config('filesystems.max_size') * 1024 . '|dimensions:max_width=1000,max_height=1000';
        // }

        // if ($this->files->get('attachment')) {
        //     $attachment = 'mimes:' . config('filesystems.mimes') . '|between:0,' . config('filesystems.max_size') * 1024;
        // }

        // // Get company id
        // $company_id = (int) $this->request->get('company_id');

        $rules = [
            "name" => 'required|string',
            "last_name" => 'required|string',
            "fdn" => 'required|string',
            "email" => 'required|string',
            "applicant" => 'required|integer',
            "dependence" => 'required|integer',
            "phone" => 'required|integer',
            "gender" => 'required|string',
            "you_do_taxes" => 'required|integer',
            "legal_status" => 'required|integer',
            "ssn" => 'required|string',
            "number_doc" => 'required|integer',
            "number_register" =>  'required|integer',
            "number_card" => 'required|integer',
            "category" => 'required|string',
            "category_policy" => 'required|string',
            "expire_card" => 'required|integer',
            "note" => 'required|string',
            "spouse" => 'required|integer|array',
            "dependents" => 'required|integer|array',
            "income" => 'required|integer|array',
            "company" => 'required|string',
            "identity" => 'required|integer',
            "plan" => 'required|integer',
            "type" => 'required|integer',
            "price" => 'required|amount',
            "address" => 'required|integer',
            "city" => 'required|integer',
            "zip_code" => 'required|integer',
            "department" => 'required|integer',
            "state" => 'required|string',
            "type_bank" => 'required|integer',
            "bank" => 'required|integer',
            "route_bank" => 'required|integer',
            "account_number_bank" => 'required|integer',
            "card_number_bank" => 'required|integer',
            "name_bank" => 'required|integer',
            "vec_bank" => 'required|integer',
            "cvc_bank" => 'required|integer',
        ];

        $items = $this->request->get('items');

        if ($items) {
            foreach ($items as $key => $item) {
                $size = 5;

                if (Str::contains($item['quantity'], ['.', ','])) {
                    $size = 7;
                }

                $rules['items.' . $key . '.quantity'] = 'required|max:' . $size;
                $this->items_quantity_size[$key] = $size;
            }
        }

        return $rules;
    }

    public function withValidator($validator)
    {
        if ($validator->errors()->count()) {
            // Set date
            $issued_at = Date::parse($this->request->get('issued_at'))->format('Y-m-d');
            $due_at = Date::parse($this->request->get('due_at'))->format('Y-m-d');

            $this->request->set('issued_at', $issued_at);
            $this->request->set('due_at', $due_at);
        }
    }

    public function messages()
    {
        $messages = [
            'items.*.name.required' => trans('validation.required', ['attribute' => Str::lower(trans('general.name'))]),
            'items.*.quantity.required' => trans('validation.required', ['attribute' => Str::lower(trans('invoices.quantity'))]),
            'items.*.price.required' => trans('validation.required', ['attribute' => Str::lower(trans('invoices.price'))]),
            'items.*.currency.required' => trans('validation.custom.invalid_currency'),
            'items.*.currency.string' => trans('validation.custom.invalid_currency'),
        ];

        if ($this->items_quantity_size) {
            foreach ($this->items_quantity_size as $key => $quantity_size) {
                $messages['items.' . $key . '.quantity.max'] = trans('validation.size', ['attribute' => Str::lower(trans('invoices.quantity')), 'size' => $quantity_size]);
            }
        }

        return $messages;
    }
}
