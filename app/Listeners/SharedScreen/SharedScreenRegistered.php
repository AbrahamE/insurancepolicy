<?php

namespace App\Listeners\SharedScreen;

use App\Events\SharedScreen\SharedScreenCreated;
use App\Utilities\Console;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;


class SharedScreenRegistered
{
    public $WEBSOCKETS_INIT = 'php artisan websockets:serve';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SharedScreenCreated  $event
     * @return void
     */
    public function handle(SharedScreenCreated $event)
    {
        // Console::run($this->WEBSOCKETS_INIT);
    }
}
