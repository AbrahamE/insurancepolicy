<?php

namespace App\Utilities;

use Illuminate\Support\Facades\URL;

class Invoice
{
  private $ejecutable = '/var/www/html/erp/storage/invoices/IntTFHKA.exe';
  private $path = '/var/www/html/erp/storage/invoices/';

  public $InvoiceTemplate;
  public $InvoiceData;

  public function __construct($InvoiceTemplate)
  {
    $this->InvoiceTemplate = $InvoiceTemplate;
  }


  public function get()
  {
    return $this->InvoiceTemplate;
  }

  // function to check the type of payment

  public static function getPaymentType($paymentType)
  {
    $data = [
      'USD' => 'Efectivo',
      'credit' => 'Tarjeta de Crédito',
      'debit' => 'Tarjeta de Débito',
      'check' => 'Cheque',
      'transfer' => 'Transferencia',
      'other' => 'Otro'
    ];

    return $data[$paymentType];
  }


  // function to create template of an invoice

  public function invoice($data)
  {

    return [
      'Numero de factura' => $data['document_number'],
      'Producto' => $data["items"][0]["name"],
      'Cantidad' => $data["items"][0]["quantity"],
      'Precio' => $data["items"][0]["price"] . '$',
      'Total' => $data["items"][0]["grand_total"] . '$',
      'Pago' => $this::getPaymentType($data["currency_code"]),
      'Descripcion' => $data["items"][0]["description"],
      'A nombre De' => $data["contact_name"],
      'tipo de factura' => $data["type"],
      'Fecha de factura' => $data['issued_at'],
    ];
  }

  // function to create template of an order

  public function order($data)
  {
    return [
      'Numero de factura' => $data['document_number'],
      'Producto' => $data["items"][0]["name"],
      'Cantidad' => $data["items"][0]["quantity"],
      'Precio' => $data["items"][0]["price"] . '$',
      // 'Total' => $data["items"][0]["grand_total"] . '$',
      'Pago' => $this::getPaymentType($data["currency_code"]),
      'Descripcion' => $data["items"][0]["description"],
      'A nombre De' => $data["contact_name"],
      'tipo de factura' => $data["type"],
      'Fecha de factura' => $data['issued_at'],
    ];
  }

  public function create($type)
  {
    $_SERVER['HTTP_HOST'] != 'localhost'
      ? $file = $this->path . 'invoice.txt'
      : $file = URL::to('storage/invoices/invoice.txt');

    $fp = fopen($file, "w+");

    $this->InvoiceData = array_map(
      function ($data) use ($type) {
        return $this->$type($data);
      },
      ['data' => $this->InvoiceTemplate->allAttributes]
    );

    array_walk(
      $this->InvoiceData['data'],
      function ($value, $key) use ($fp) {
        fputs($fp, $key . ': ' . $value . "\n");
      }
    );

    fclose($fp);

    return  $file;
  }

  /**
   * Create a new event instance.
   *
   * @param String $txt
   */
  public function print($txt)
  {
    $sentence = $this->ejecutable . " SendFileCmd(" . $txt . ")";

    $respuesta = shell_exec($sentence);

    return $respuesta;
  }
}
