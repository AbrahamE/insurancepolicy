<?php

namespace App\Jobs\Document;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use App\Models\Document\DocumentPolicy as Document;
use App\Models\Document\DocumentItem;
use App\Models\Document\DocumentItemTax;
use App\Models\Setting\Tax;
use Illuminate\Support\Str;

class CreateDocumentItemDependents extends Job implements HasOwner, HasSource, ShouldCreate
{
    protected $document;

    protected $request;

    public function __construct(Document $document, $request)
    {
        $this->document = $document;
        $this->request = $request;

        parent::__construct($document, $request);
    }

    public function handle(): DocumentItem
    {
        return  DocumentItem::create($this->request);
    }
}
