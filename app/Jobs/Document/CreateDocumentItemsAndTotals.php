<?php

namespace App\Jobs\Document;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use App\Jobs\Document\CreateDocumentItemDependents;
use App\Models\Document\DocumentPolicy as Document;
use App\Models\Document\DocumentTotal;
use App\Traits\Currencies;
use App\Traits\DateTime;
use Illuminate\Support\Str;

class CreateDocumentItemsAndTotals extends Job implements HasOwner, HasSource, ShouldCreate
{
    use Currencies, DateTime;

    protected $document;

    public function __construct(Document $document, $request)
    {
        $this->document = $document;

        parent::__construct($request);
    }

    public function handle(): void
    {
        $precision = config('money.' . $this->document->currency_code . '.precision');

        // list($sub_total, $discount_amount_total, $taxes) = $this->createItems();

        $this->createItems();
        $this->createIncomeTotal();
    }

    protected function createIncomeTotal(): array
    {
        $incomes = [];

        if (empty($this->request['income'])) {
            return $incomes;
        }

        foreach ((array) json_decode($this->request['income'], true) as $key => $item) {
            // Add sub total
            foreach ((array) json_decode(
                $this->request['income'],
                true
            )[$key] as $k => $value) {
                $incomes[$k][$key] = $value;
                $incomes[$k]['income_id'] = $k;
            }
        }

        foreach ($incomes as $income) {
            $income['company_id'] = $this->document->company_id;
            $income['document_id'] = $this->document->id;
            $income['type'] = $this->document->type;
            $income['code'] = 'sub_total';
            $income['created_from'] = $this->request['created_from'];
            $income['created_by'] = $this->request['created_by'];
            $income['name'] = $this->request['name'];

            DocumentTotal::create($income);
        }

        return $income;
    }

    protected function createItems(): array
    {
        $dependents = [];

        if (empty($this->request['dependents'])) {
            return $dependents;
        }

        foreach ((array) json_decode($this->request['dependents'], true) as $key => $item) {
            foreach ((array) json_decode(
                $this->request['dependents'],
                true
            )[$key] as $k => $value) {
                $dependents[$k][$key] = $value;
            }
        }

        foreach ($dependents as $key => $dependent) {

            $dependent['document_id'] = $this->document->id;
            $dependent['company_id'] =  $this->request['company_id'];
            $dependent['item_id'] = $key;
            $dependent['type'] = $this->document->type;
            $dependent['created_from'] = $this->request['created_from'];
            $dependent['created_by'] = $this->request['created_by'];

            $this->dispatch(new CreateDocumentItemDependents(
                $this->document,
                $dependent
            ));
        }

        return $dependents;
    }
}
