<?php

namespace App\Jobs\Document;

use App\Abstracts\Job;
use App\Events\Document\DocumentCreated;
use App\Events\Document\DocumentCreating;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use App\Jobs\Document\CreateDocumentItemsAndTotals;
use App\Models\Document\DocumentPolicy as Document;
use Illuminate\Support\Str;
use PhpParser\Node\Expr\Cast\String_;

class CreateDocument extends Job implements HasOwner, HasSource, ShouldCreate
{
    public function handle(): Document
    {
        $this->request['company_id'] = company_id();
        $this->request['type_document'] = Document::POLICY_TYPE;
        $this->request['currency_code'] = Document::POLICY_TYPE;
        $this->request["spouse"] = json_encode($this->request["spouse"]);
        $this->request["dependents"] = json_encode($this->request["dependents"]);
        $this->request["income"] = json_encode($this->request["income"]);
        $this->request["bank"] = json_encode($this->request["bank"]);
        $this->request["credit_bank"] = json_encode($this->request["credit_bank"]);
      
        event(new DocumentCreating($this->request));

        \DB::transaction(function () {
            $this->model = Document::create($this->request->all());
            // Upload attachment
            if ($this->request->file('attachment')) {
                foreach ($this->request->file('attachment') as $attachment) {
                    $media = $this->getMedia($attachment, Str::plural($this->model->type));

                    $this->model->attachMedia($media, 'attachment');
                }
            }

            $this->dispatch(new CreateDocumentItemsAndTotals($this->model, $this->request));

            $this->model->update($this->request->all());
            
            $this->model->createRecurring($this->request->all());
        });
        
        event(new DocumentCreated($this->model, $this->request));
    
        return $this->model;
    }
}
