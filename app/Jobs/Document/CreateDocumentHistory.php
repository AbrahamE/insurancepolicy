<?php

namespace App\Jobs\Document;

use App\Abstracts\Job;
use App\Interfaces\Job\HasOwner;
use App\Interfaces\Job\HasSource;
use App\Interfaces\Job\ShouldCreate;
use App\Models\Document\DocumentPolicy as Document;
use App\Models\Document\DocumentHistory;
use Illuminate\Http\Request;

class CreateDocumentHistory extends Job implements HasOwner, HasSource, ShouldCreate
{
    protected $document;

    protected $notify;

    protected $description;

    public function __construct(Document $document, $request = null)
    {
        $this->document = $document;
        $this->request = $request;

        parent::__construct($document);
    }

    public function handle(): DocumentHistory
    {
        // $description = $this->description ?: trans_choice('general.payments', 1);

        $document_history = DocumentHistory::create([
            'company_id' => $this->document->company_id,
            'document_id' => $this->document->id,
            'type_document' => $this->document->type_document,
            'type' => $this->document->type,
            // 'status' => $this->document->status,
            "name" => $this->document->name,
            "last_name" => $this->document->last_name,
            'created_from' => $this->getCustomSourceName(),
            'created_by' => user_id(),
        ]);
        return $document_history;
    }

    public function getCustomSourceName(): string
    {
        if (empty($this->request)) {
            return $this->getSourceName();
        }

        if (is_array($this->request)) {
            if (empty($this->request['created_from'])) {
                return $this->getSourceName();
            }
        } elseif ($this->request instanceof Request) {
            if ($this->request->missing('created_from')) {
                return $this->getSourceName();
            }
        }

        return $this->request['created_from'];
    }
}
