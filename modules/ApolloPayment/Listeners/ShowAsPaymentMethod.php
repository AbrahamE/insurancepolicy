<?php

namespace Modules\ApolloPayment\Listeners;

use App\Events\Module\PaymentMethodShowing as Event;

class ShowAsPaymentMethod
{
  /**
   * Handle the event.
   *
   * @param  Event $event
   * @return void
   */
  public function handle(Event $event)
  {
    $method = setting('paypal-express', ["name" => 'Pago por tarjeta (Apollo)']);

    $method['code'] = 'apollo-payment';

    $event->modules->payment_methods[] = $method;
  }
}
