<?php

namespace Modules\ApolloPayment\Models;

use App\Abstracts\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class IdentityCard extends Model
{

    use HasFactory;

    public $table = 'apollo_customers_identity_cards';

    protected $fillable = ['number', 'type'];

    public function customer()
    {
        $this->hasOne('contacts', 'id', 'customer_id');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return IdentityCard::new();
    }
}
