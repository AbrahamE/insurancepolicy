<?php

namespace Modules\ApolloPayment\Http\ViewComposers;

use Illuminate\View\View;

class Transactions
{
  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return mixed
   */
  public function compose(View $view)
  {
    // Push to a stack
    // var_dump($view->getFactory());
    // var_dump($view->getFactory()->);
    // die;
    $view->getFactory()->startPush('row_footer_transactions_body_td_end', view('apollo-payment::order.show'));
  }
}
