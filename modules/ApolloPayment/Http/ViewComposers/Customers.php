<?php

namespace Modules\ApolloPayment\Http\ViewComposers;

use Illuminate\View\View;

class Customers
{
  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return mixed
   */
  public function compose(View $view)
  {
    // Push to a stack
    $view->getFactory()->startPush('create_user_input_start', view('apollo-payment::customer'));
  }
}
