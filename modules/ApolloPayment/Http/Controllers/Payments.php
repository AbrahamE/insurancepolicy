<?php

namespace Modules\ApolloPayment\Http\Controllers;

use App\Abstracts\Http\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class Payments extends PaymentController
{
    const BASE_URL = 'https://services.cleatsi.com:9092';

    const PARAM_METHOD = 'method';
    const METHOD_LAST_TRANSACTION_DATE = 'getLastTransactionDate';
    const METHOD_GET_TRANSACTION = 'getTransactions';

    const PARAM_TRANS_ID = 'id_trans';

    const PARAM_OPERATION = 'operacion';
    const OPERATION_SELL = 'COMPRA';
    const OPERATION_VOID = 'ANULACION';

    const REFERENCE_KEY = 'reference';

    const ERROR_STATUS = 'error';
    const SUCCESS_STATUS = 'success';
    public $alias = 'ApolloCLEA';

    public function saveLastPaymentReference(Request $req)
    {
        $req->validate([
            'session' => 'required'
        ]);
        try {
            $this->saveReference($this->getLastPaymentTimestamp());
        } catch (\Exception $error) {
            return response()->json([
                "status" => self::ERROR_STATUS,
                "message" => $error->getMessage()
            ], Response::HTTP_OK);
        }

        return response()->json([
            "status" => self::SUCCESS_STATUS,
            "message" => ''
        ], Response::HTTP_OK);
    }


    public function getSellByTransId($transactionId)
    {
        try {;
            return response()->json([
                "status" => self::SUCCESS_STATUS,
                "data" => $this->getTransactionFromServices($this->buildGetSellOperationParams($transactionId))->json()
            ], Response::HTTP_OK);
        } catch (\Exception $error) {
            return response()->json([
                "status" => self::ERROR_STATUS,
                "message" => $error->getMessage()
            ], Response::HTTP_OK);
        }
    }
    public function getVoidByTransId($transactionId)
    {
        try {
            return response()->json([
                "status" => self::SUCCESS_STATUS,
                "data" => $this->getTransactionFromServices($this->buildGetVoidOperationParams($transactionId))->json()
            ], Response::HTTP_OK);
        } catch (\Exception $error) {
            return response()->json([
                "status" => self::ERROR_STATUS,
                "message" => $error->getMessage()
            ], Response::HTTP_OK);
        }
    }

    private function buildGetSellOperationParams($transactionId)
    {
        return [
            self::PARAM_METHOD => self::METHOD_GET_TRANSACTION,
            self::PARAM_OPERATION => self::OPERATION_SELL,
            self::PARAM_TRANS_ID => $transactionId
        ];
    }
    private function buildGetVoidOperationParams($transactionId)
    {
        return [
            self::PARAM_METHOD => self::METHOD_GET_TRANSACTION,
            self::PARAM_OPERATION => self::OPERATION_VOID,
            self::PARAM_TRANS_ID => $transactionId
        ];
    }

    private function getTransactionFromServices(array $body)
    {
        return Http::withoutVerifying()->asForm()->post(self::BASE_URL . '/api/merchant', $body);
    }
}
