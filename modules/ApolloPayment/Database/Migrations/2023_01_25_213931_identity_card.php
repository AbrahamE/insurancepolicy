<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IdentityCard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apollo_customers_identity_cards', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->string('type');
            $table->bigInteger('customer_id')->index();

            $table->timestamps();
            $table->softDeletes();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apollo_customers_identity_cards');
    }
}
