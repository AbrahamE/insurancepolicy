<?php

namespace Modules\ApolloPayment\Providers;

use App\Models\Common\Contact;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider as Provider;

class Main extends Provider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutes();
        $this->loadViews();
        $this->loadViewComposers();
        $this->loadObservers();
    }

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrations();
    }

    /**
     * Load migrations.
     *
     * @return void
     */
    public function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Load routes.
     *
     * @return void
     */
    public function loadRoutes()
    {
        if (app()->routesAreCached()) {
            return;
        }

        $routes = [
            'admin.php',
        ];

        foreach ($routes as $route) {
            $this->loadRoutesFrom(__DIR__ . '/../Routes/' . $route);
        }
    }
    private function loadViews()
    {
        // "hola maldito");
        // die;
        // echo "<pre>";
        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'apollo-payment');
    }
    private function loadViewComposers()
    {

        View::composer(
            ['sales.customers.create'],
            'Modules\ApolloPayment\Http\ViewComposers\Customers'
        );

        View::composer(
            ['pos::orders.show'],
            'Modules\ApolloPayment\Http\ViewComposers\Transactions'
        );
    }
    private function loadObservers()
    {
        Contact::observe('Modules\ApolloPayment\Observers\Common\Contacts');
    }
}
