<?php

use Illuminate\Support\Facades\Route;

/**
 * 'admin' middleware and 'apollo-payment' prefix applied to all routes (including names)
 *
 * @see \App\Providers\Route::register
 */

Route::admin('apollo-payment', function () {
    Route::controller('Payments')->group(function () {
        Route::get('/sell/{trans_id}', 'getSellByTransId')->whereAlphaNumeric('trans_id');
        Route::get('/void/{trans_id}', 'getVoidByTransId')->whereAlphaNumeric('trans_id');
    });
});
