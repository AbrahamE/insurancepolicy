<div class="row px-3">
    <div class="col-4 py-2">
        <input class="form-control mt-2 mb-2 col-md-12"
        onkeypress='validate(event)'
        type="text"
        wire:model.lazy="base_currency_rate"
        wire:loading.attr="disabled" wire:target="base_currency, second_currency, base_currency_rate, second_currency_rate">

        <input class="form-control mb-2 col-md-12"
        onkeypress='validate(event)'
        type="text"
        wire:model.lazy="second_currency_rate"
        wire:loading.attr="disabled" wire:target="base_currency, second_currency, base_currency_rate, second_currency_rate">
    </div>

    <div class="col-8 py-2">
      
        <select
        class="form-control mt-2 mb-2"
        name="base_curreny"
        wire:model.debounce.1ms="base_currency"
        wire:loading.attr="disabled" wire:target="base_currency, second_currency, base_currency_rate, second_currency_rate">
            @foreach($base_currency_data as $currency)
                <option value="{{ $currency['id'] }}">{{ $currency['name'] }}</option>
            @endforeach
        </select>

        <select
        class="form-control"
        name="second_curreny"
        wire:model.debounce.1ms="second_currency"
        wire:loading.attr="disabled" wire:target="base_currency, second_currency, base_currency_rate, second_currency_rate">
            @foreach($second_currency_data as $currency)
                <option value="{{ $currency['id'] }}">{{ $currency['name'] }}</option>
            @endforeach
        </select>
    </div>
</div>

@push('scripts')
    <script>
        function validate(evt) {
            var theEvent = evt || window.event;

            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }

            var regex = /[0-9]|\./;

            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@endpush
