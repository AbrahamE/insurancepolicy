<?php

return [

    'name'              => 'Métricas de ventas',
    'description'       => 'Este es mi impresionante módulo',
   

    'widgets'           => [
        'top_customers_revenue_based'   => 'Los 5 principales clientes en función de los ingresos',
        'top_customers_profit_based'    => 'Los 5 principales clientes se basan en los beneficios',
        'top_items_revenue_based'       => 'Las 5 principales partidas se basan en los ingresos',
        'top_items_profit_based'        => 'Los 5 mejores artículos basados en los beneficios',
        'top_items_profit_margin_based' => 'Las 5 principales partidas se basan en el margen de beneficios',
    ]

];