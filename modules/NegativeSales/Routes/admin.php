<?php

use Illuminate\Support\Facades\Route;

/**
 * 'admin' middleware and 'negative-sales' prefix applied to all routes (including names)
 *
 * @see \App\Providers\Route::register
 */

Route::admin('negative-sales', function () {
    Route::get('/', 'Main@index');
});
