<?php

return [
    'male'   => 'Hombre',
    'other'  => 'Otros',
    'female' => 'Mujer',

    'hired_at'            => 'Fecha de contratación',
    'birth_day'           => 'Día de nacimiento',
    'gender'              => 'Género',
    'bank_account_number' => 'Número de cuenta bancaria',

    'personal_information' => 'Información personal',
    'salary'               => 'Salario',

    'messages' => [
        'contact_missing' => 'No hay ningún contacto asociado para el usuario actual',
    ],
];
