<?php

return [

    'name'        => 'Empleados',
    'description' => 'Administrar empleados',

    'empty'       => 'Los empleados son necesarios si se quiere utilizar la nómina.',

    'hr' => 'RR. HH',

    'total' => 'Total :type',

    'employees' => 'Empleado|Empleados',
    'positions' => 'Posición | Posiciones',
];
