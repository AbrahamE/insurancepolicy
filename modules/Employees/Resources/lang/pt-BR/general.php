<?php

return [

    'name'        => 'Colaboradores',
    'description' => 'Gerenciar colaboradores',

    'hr' => 'RRHH',

    'total' => 'Total :type',

    'employees' => 'Colaborador|Colaboradores',
    'positions' => 'Posição|Posições',
];
