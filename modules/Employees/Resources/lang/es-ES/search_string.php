<?php

return [
    'columns' => [
        'hired_at'  => 'Contratado en',
        'birth_day' => 'Día de nacimiento',
        'contacts'  => [
            'enabled' => 'Habilitado',
        ],
        'gender'    => 'Género',
    ]
];
