<?php return array(
    'root' => array(
        'pretty_version' => '3.1.9',
        'version' => '3.1.9.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '6701b6ce2aef15d93376b8178ea8c6613146df43',
        'name' => '__root__',
        'dev' => false,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '3.1.9',
            'version' => '3.1.9.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '6701b6ce2aef15d93376b8178ea8c6613146df43',
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'picqer/php-barcode-generator' => array(
            'pretty_version' => 'v2.2.1',
            'version' => '2.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../picqer/php-barcode-generator',
            'aliases' => array(),
            'reference' => '49022793ff09569c57a2621ba58a191db5a421c4',
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
    ),
);
