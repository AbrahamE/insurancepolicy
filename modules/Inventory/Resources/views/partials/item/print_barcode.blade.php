@extends('layouts.print')

@section('content')
    <div class="text-center">
        <img src="{{ $barcode ? Storage::url($barcode->id) : asset('modules/Inventory/Resources/assets/img/barcode/code_128.png') }}" class="image-style">
    </div>
    <div class="text-monospace text-center">
        {{ $item->inventory()->value('barcode') ?? 'brcd123456789' }}
    </div>
@endsection
