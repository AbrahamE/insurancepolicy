<?php

return [
    'assets'        => 'Activos',
    'liabilities'   => 'Pasivo',
    'expenses'      => 'Gastos',
    'income'        => 'Ingresos',
    'equity'        => 'Patrimonio neto'
];
