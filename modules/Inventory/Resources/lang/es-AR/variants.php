<?php

return [
    'values'        => 'Valores',

    'types' => [
        'choose'    => 'Escoger',
        'select'    => 'Seleccione',
        'radio'     => 'Radio',
        'checkbox'  => 'Caja',
        'input'     => 'Entrada',
        'text'      => 'Texto',
        'textarea'  => 'área de texto',
    ]
];
