<?php

return [
    'track_inventory'       => 'Seguimiento del inventario',
    'opening_stock'         => 'Stock de apertura',
    'opening_stock_value'   => 'Valor de stock iniciales',
    'reorder_level'         => 'Nivel de pedido',
];
