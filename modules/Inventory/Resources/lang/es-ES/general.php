<?php

return [
  'Cash'                   => 'Efectivo',
  'Bank Card'              => 'Tarjeta',
  'Bank Transfer'          => 'Transferencia Bancaria',

  'warehouse'              => 'Almacén',
  'name'                      => 'Inventario',
  'description'               => 'Contabilidad y gestión de inventario bajo un mismo techo',
  'created_at'               => 'Creado en',
  'items'                     => 'Artículo|Artículos',
  'inventories'               => 'Inventario|Inventarios',
  'variants'                  => 'Variante|Variantes',
  'manufacturers'             => 'Fabricante|Fabricantes',
  'transfer_orders'           => 'Orden de transferencia|Órdenes de transferencia',
  'adjustments'               => 'Ajuste|Ajustes',
  'warehouses'                => 'Almacén|Almacenes',
  'histories'                 => 'Historia|Historias',
  'item_groups'               => 'Grupo|Grupos',
  'barcode'                   => 'Código de barra',
  'sku'                       => 'Código de artículo',
  'quantity'                  => 'Cantidad',
  'add_warehouse'             => 'Añadir almacén',
  'edit_warehouse'            => 'Editar almacén',
  'default'                   => 'Por defecto',
  'stock'                     => 'Stock',
  'information'               => 'Información',
  'default_warehouse'         => 'Almacén predeterminado',
  'track_inventory'           => 'Seguimiento de inventario',
  'negatif_stock'             => 'Acciones negativas',
  'expented_income'           => 'Ingresos Gastados',
  'sale_item_quantity'        => 'Cantidad de artículos en venta',
  'sale_item_amount'          => 'Importe del artículo de venta',
  'purchase_item_quantity'    => 'Cantidad de artículo de compra',
  'purchase_item_amount'      => 'Importe del artículo de compra',
  'income'                    => 'Ingreso',
  'invalid_stock'             => 'Stock en almacén :stock',
  'low_stock'                 => ':name Stock bajas (:count - :warehouse)',
  'unit'                      => 'Unidad',
  'returnable'                => 'Artículo retornable',
  'overview'                  => 'Visión de conjunto',
  'action'                    => 'Acción',
  'record'                    => 'Registro',
  'required_fields'           => 'El campo :attribute es obligatorio.',
  'print_barcode'             => 'Imprimir Codigo de barra',

  'menu' => [
    'inventory'             => 'Inventario',
    'item_groups'           => 'Groupos',
    'variants'              => 'Variantes',
    'manufacturers'         => 'Fabricante',
    'warehouses'            => 'Almacenes',
    'histories'             => 'Historias',
    'reports'               => 'Informes',
  ],

  'notifications' => [
    'reorder_level'         => ':count Nivel de pedido de artículos',
  ],

  'document' => [
    'detail'                => 'Un almacén de :class se utiliza para la contabilidad adecuada de su :type y para mantener la precisión de sus informes.',
  ],

  'empty' => [
    'adjustments'           => "Debido a algunas razones, como artículos dañados y artículos robados, etc.,
                                    las acciones reales de su empresa y las acciones registradas pueden no ser iguales.
                                    El ajuste de inventario le permite registrar los artículos que faltan.",
    'warehouses'            => 'Puede agregar administrar múltiples almacenes.
                                    También puedes realizar un seguimiento del control de stock de todos tus artículos por almacenes.
                                    La descripción general y el historial del almacén le brindan información sobre las operaciones de los almacenes.',
    'transfer-orders'       => 'Orden de transferencia le permite realizar un seguimiento del movimiento de artículos de un almacén a otro.',
    'variants'              => 'En la sección Variantes, puede agregar y administrar variantes que describan mejor sus artículos.
                                    Puede crear un grupo de artículos que tengan las mismas variantes, como color, tamaño, etc.',
    'item-groups'           => 'En la sección Grupos, puede administrar sus artículos que pueden considerarse dentro del mismo grupo.
                                    Puede seleccionar variantes, agregar nuevos artículos y administrar sus detalles desde grupos.',
    'title' => [
      'adjustments'       => 'Ajustamiento',
      'warehouses'        => 'Almacenes',
      'transfer-orders'   => 'Órdenes de transferencia',
      'variants'          => 'variantes',
      'item-groups'       => 'Grupos de artículos',
    ]
  ],

  'reports' => [
    'name' => [
      'stock_status'      => 'Estado del Stock',
      'sale_summary'      => 'Resumen de venta',
      'purchase_summary'  => 'Resumen de compra',
      'item_summary'      => 'Resumen del artículo',
      'profit_loss'       => 'Pérdidas y ganancias (Inventario)',
      'income_summary'    => 'Resumen de Ingresos (Inventario)',
      'expense_summary'   => 'Resumen de gastos (Inventario)',
      'income_expense'    => 'Ingresos vs Gastos (Inventario)',
    ],

    'description' => [
      'stock_status'      => 'Seguimiento de stock de artículos.',
      'sale_summary'      => 'Seguimiento de stock de artículos de venta.',
      'purchase_summary'  => 'Seguimiento de stock de artículos de compras.',
      'item_summary'      => 'La lista de la información del artículo',
      'profit_loss'       => 'Ganancias y pérdidas trimestrales por inventario.',
      'income_summary'    => 'Resumen de ingresos mensuales por inventario.',
      'expense_summary'   => 'Resumen de gastos mensuales por inventario.',
      'income_expense'    => 'Ingresos vs gastos mensuales por inventario.',
    ],
  ],


];
