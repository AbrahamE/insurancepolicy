<?php

return [
    'values'        => 'Valores',

    'types' => [
        'choose'    => 'Escoger',
        'select'    => 'Selección',
        'radio'     => 'Radio',
        'checkbox'  => 'Casillas de Verificación',
        'input'     => 'Entrada',
        'text'      => 'Texto',
        'textarea'  => 'Área de texto',
    ]
];
