<?php

return [
    'track_inventory'               => 'Seguimiento de inventario',
    'opening_stock'                 => 'Stock de apertura',
    'opening_stock_value'           => 'Valor inicial de stock',
    'reorder_level'                 => 'Nivel de pedido',
    'reorder_level_notification'    => 'Notificación de nivel de pedido',
    'total_stock'                   => 'Total Stock',
    'add_variants'                  => 'Añadir variantes',
    'price'                         => 'Precio',
    'low_stock'                     => 'Bajo Stock',
    'created_item'                  => 'Artículo creado',
    'returnable'                    => 'Retornable',
    'general_information'           => 'Información general',
    'can_not_be_negative'           => 'No puede ser negativo',
    

    'Box'                           => 'Caja',
    'Dozen'                         => 'Docena',
    'Grams'                         => 'Gramos',
    'Kilograms'                     => 'Kilogramos',
    'Meters'                        => 'Metros',
    'Units'                         => 'Unidades',
    'Pairs'                         => 'Pares',
    'Pieces'                        => 'Piezas',
    'Tablets'                       => 'Tabletas',
    'Liters'                        => 'Litros',

    'unit' => [
        '0' => 'Box',
        '1' => 'Dozen',
        '2' => 'Grams',
        '3' => 'Kilograms',
        '4' => 'Meters',
        '5' => 'Units',
        '6' => 'Pairs',
        '7' => 'Pieces',
        '8' => 'Tablets',
        '9' => 'Liters'
    ],
];
