<?php

return [
    'adjustment_number' => 'Número de ajuste',
    'adjusted_quantity' => 'Cantidad ajustada',
    'new_quantity'      => 'Nueva cantidad',
    'number_error'      => 'El número debe ser menor que la cantidad del artículo',
 
    'Stock on fire' => 'Stock en llamas',
    'Stolen Items' => 'Artículos robados',
    'Damaged Items' => 'Artículos dañados',
     'Other' => 'Otros',

     'reason' => [
      '0' => 'Stock en llamas',
      '1' => 'Artículos robados',
      '2' => 'Artículos dañados',
      '3' => 'Otros',
  ],




];
