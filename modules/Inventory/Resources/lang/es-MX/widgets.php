<?php

return [
    'total_stock'                   => 'Total de Stock',
    'total_stock_value'             => 'Valor total de Stock',
    'total_income'                  => 'Total de ingresos',
    'total_expense'                 => 'Total de gastos',
    'stock_line_chart'              => 'Gráfico de líneas de Stock',
    'tracked_items_cash_flow'       => 'Flujo de caja de los artículos rastreados',
    'warehouse_list'                => 'Lista del almacén',
    'low_stock_items'               => 'Artículos de bajo stock',
    'top_seller_items'              => 'Artículos más vendidos',
    'reorder_level'                 => 'Nivel de pedido',
    'total_item'                    => 'Total de artículos',
    'total_item_income'             => 'Ingreso total de artículos',
    'total_item_expense'            => 'Total de gastos del artículo',
    'total_current_stock'           => 'Total de stock actuales',
    'sales_quantity_by_warehouse'   => 'Cantidad de ventas por almacén',
];
