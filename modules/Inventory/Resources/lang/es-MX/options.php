<?php

return [
    'values'        => 'Valores',

    'types' => [
        'choose'    => 'Seleccionable',
        'select'    => 'Seleccionar',
        'radio'     => 'Opción',
        'checkbox'  => 'Lista',
        'input'     => 'Llenable',
        'text'      => 'Texto simple',
        'textarea'  => 'Texto',
    ]
];
