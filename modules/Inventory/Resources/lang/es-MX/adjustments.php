<?php

return [
    'adjustment_number' => 'Número de ajuste',
    'adjusted_quantity' => 'Cantidad ajustada',
    'new_quantity'      => 'Nueva cantidad',
    'number_error'      => 'El número debe ser menor que la cantidad del artículo',

    'reasons' => [
        '0' => 'Stock en llamas',
        '1' => 'Artículos robados',
        '2' => 'Artículos dañados',
        '3' => 'Otros',
    ],
];
