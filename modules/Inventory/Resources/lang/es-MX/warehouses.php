<?php

return [
    'main_warehouse'            => 'Almacén principal',
    'default'                   => 'Almacen predeterminado',
    'default_warehouse_delete'  => 'Almacén por defecto no se puede eliminar',
    'country'                   => 'País',
    'city'                      => 'Ciudad',
    'total_item'                => 'Artículo total',
];
