<?php

return [
    'track_inventory'               => 'Seguimiento de inventario',
    'opening_stock'                 => 'Stock de apertura',
    'opening_stock_value'           => 'Valor de stock de apertura',
    'reorder_level'                 => 'Reordenar nivel',
    'reorder_level_notification'    => 'Notificación de nivel de pedido',
    'total_stock'                   => 'Total Stock',
    'add_variants'                  => 'Añadir variantes',
    'price'                         => 'Precio',
    'low_stock'                     => 'Bajo Stock',
    'created_item'                  => 'Artículo creado',
    'returnable'                    => 'Retornable',
    'general_information'           => 'Información general',
    'can_not_be_negative'           => 'No puede ser negativo',

    'Box'                           => 'Caja',
    'Dozen'                         => 'Docena',
    'Grams'                         => 'Gramos',
    'Kilograms'                     => 'Kilogramos',
    'Meters'                        => 'Metros',
    'Units'                         => 'Unidades',
    'Pairs'                         => 'Pares',
    'Pieces'                        => 'Piezas',
    'Tablets'                       => 'Tabletas',
    'Liters'                        => 'Litros',
];
