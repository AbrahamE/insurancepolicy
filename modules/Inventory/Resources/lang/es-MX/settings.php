<?php

return [
    'default_unit'          => 'Unidad por defecto',
    'barcode_type'          => 'Tipo de código de barras',
    'show_invoice_barcode'  => 'Mostrar el código de barras de la factura',
    'show_bill_barcode'     => 'Mostrar código de barras de la factura',
    'example'               => 'Ejemplo:',
    
    'number' => [
        'digit'     => 'Dígito del número',
        'next'      => 'Número siguiente',
        'prefix'    => 'Prefijo del número',
    ],

    'error' => [
        'unit_null'     => 'No se puede enviar la unidad nula',
        'unit_empty'    => 'No se puede enviar la unidad vacía',
        'reason_null'   => 'No se puede enviar la razón nula',
        'reason_empty'  => 'No se puede enviar la razón vacía',
    ],
];
