<?php

return [
    'values'        => 'Values',

    'types' => [
        'choose'    => 'choose',
        'select'    => 'Selection',
        'radio'     => 'Radio',
        'checkbox'  => 'Checkboxes',
        'input'     => 'Input',
        'text'      => 'Text',
        'textarea'  => 'text area',
    ]
];
