<?php

namespace Modules\Pos\Services;

use App\Traits\Modules;

class InventoryIntegration
{
    use Modules;

    private $is_integration_enabled;

    public function __construct()
    {
        $this->is_integration_enabled = $this->moduleIsEnabled('inventory') && setting('pos.general.enable_inventory_integration');
    }

    public function integration_is_enabled(): bool
    {
        return $this->is_integration_enabled;
    }

    public function get_assigned_warehouse()
    {
        $warehouse_class = '\Modules\Inventory\Models\Warehouse';

        $empty = new $warehouse_class();
        $empty->id = null;
        $empty->name = '';

        if (!$this->is_integration_enabled) {
            return $empty;
        }

        return optional(user()->user_warehouse)->warehouse
            ?? $warehouse_class::enabled()->findOrFail(setting('inventory.default_warehouse', 1))
            ?? $empty;
    }
}
