<?php

namespace Modules\Pos\Http\ViewComposers;

use App\Traits\Modules;
use Illuminate\View\View;

class WhiteLabelIntegration
{
    use Modules;

    public function compose(View $view): void
    {
        if (!$this->moduleIsEnabled('white-label')) {
            return;
        }

        $class = '\Modules\WhiteLabel\Http\ViewComposers\Colors';

        (new $class())->compose($view);
    }
}
