<?php

namespace Modules\Pos\Http\Controllers;

use App\Abstracts\Http\Controller;
use App\Models\Auth\User;
use App\Models\Banking\Account;
use App\Models\Common\Contact;
use App\Models\Setting\Category;
use Modules\Pos\Models\UserWarehouse;
use App\Utilities\Modules;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Str;
use Modules\Pos\Http\Requests\Settings as Request;
use Modules\Pos\Jobs\CreateUserWarehouse;
use stdClass;

class Settings extends Controller
{
    use \App\Traits\Modules;

    public function edit()
    {
        $customers = Contact::customer()
            ->enabled()
            ->orderBy('name')
            ->take(setting('default.select_limit'))
            ->pluck('name', 'id');

        $income_categories = Category::income()
            ->enabled()
            ->orderBy('name')
            ->take(setting('default.select_limit'))
            ->pluck('name', 'id');

        $expense_categories = Category::expense()
            ->enabled()
            ->orderBy('name')
            ->take(setting('default.select_limit'))
            ->pluck('name', 'id');

        $accounts = Account::enabled()
            ->orderBy('name')
            ->take(setting('default.select_limit'))
            ->pluck('name', 'id');

        $payment_methods = Modules::getPaymentMethods();

        $inventory_module_enabled = $this->moduleIsEnabled('inventory');

        $users_warehouses = company()->users()->with('user_warehouse')->get();

        $warehouses = $inventory_module_enabled
            ? call_user_func('\Modules\Inventory\Models\Warehouse::enabled')->pluck('name', 'id')
            : [];

        return view('pos::settings', compact(
            'customers',
            'income_categories',
            'expense_categories',
            'accounts',
            'payment_methods',
            'inventory_module_enabled',
            'users_warehouses',
            'warehouses'
        ));
    }

    public function update(Request $request): JsonResponse
    {
        $alias = 'pos.pos_order';

        $fields = $request->only(['number_prefix', 'number_digit', 'number_next']);

        foreach ($fields as $key => $value) {
            setting()->set($alias . '.' . $key, $value);
        }

        $alias = 'pos.general';

        $fields = $request->only([
            'cash_account_id',
            'cash_payment_method',
            'card_account_id',
            'card_payment_method',
            'guest_customer_id',
            'sale_category_id',
            'change_category_id',
            'printer_paper_size',
            'use_paper_cutter',
            'negative_inventory_control', // new field to check an inventory with negative numbers
            'show_logo_in_receipt',
            'use_barcode_scanner',
            'enable_inventory_integration',
        ]);

        foreach ($fields as $key => $value) {
            setting()->set($alias . '.' . $key, $value);
        }

        setting()->save();

        $this->assignWarehousesToSalesclerks($request);

        $message = trans('messages.success.updated', ['type' => trans_choice('general.settings', 2)]);

        $response = [
            'status'   => null,
            'success'  => true,
            'error'    => false,
            'message'  => $message,
            'data'     => null,
            'redirect' => route('settings.index')
        ];

        flash($message)->success();

        return response()->json($response);
    }

    private function assignWarehousesToSalesclerks(Request $request): void
    {
        if (!$this->moduleIsEnabled('inventory')) {
            return;
        }

        $request->collect()
            ->filter(function ($value, $key) {
                return Str::startsWith($key, 'warehouse_');
            })
            ->each(function ($value, $key) {
                if (UserWarehouse::where('id', '=', Str::after($key, 'warehouse_'))->get()) {
                    UserWarehouse::where(
                        'id',
                        '=',
                        Str::after($key, 'warehouse_')
                    )
                        ->update(['warehouse_id' => $value]);
                } else {
                    $this->dispatch(new CreateUserWarehouse([
                        'company_id'   => company_id(),
                        'user_id'      => Str::after($key, 'warehouse_'),
                        'warehouse_id' => $value,
                    ]));
                }
            });
    }

    public function storeConfigs(HttpRequest $request): JsonResponse
    {
        $alias = 'pos.pos_order';
        // var_dump($request->input());
        setting()->set(
            "$alias.config",
            json_encode($request->input())
        );
        setting()->save();
        return response()->json(["success" => true]);
    }

    public function getConfigs(): JsonResponse
    {
        $config = json_decode(setting("pos.pos_order.config"));
        $config->isActiveApollo = setting('pos.general.card_payment_method') ==
            "apollo-payment";
        return response()->json($config);
    }
}
