<?php

namespace Modules\Pos\Http\Controllers\Api;

use App\Abstracts\Http\ApiController;
use App\Jobs\Document\DeleteDocument;
use App\Jobs\Document\UpdateDocument;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Pos\Jobs\CreateOrder;
use Modules\Pos\Models\Order as Document;

class Orders extends ApiController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Dingo\Api\Http\Response
   */
  public function index()
  {
    $orders = Document::with('contact', 'transactions', 'items')
      ->get();

    return collect($orders);
  }

  public function store(Request $request): JsonResponse
  {
    $request['type_case'] = 'orders';

    $response = $this->ajaxDispatch(new CreateOrder($request));

    try {
      if ($response['success']) {
        $response['message'] = trans('messages.success.added', ['type' => trans_choice('pos::general.orders', 1)]);
      } else {
        $message = $response['message'];

        flash($message)->error()->important();
      }
    } catch (\Throwable $th) {
      $message = $th['message'];

      flash($message)->error()->important();
    }

    return response()->json($response);
  }

  public function show(Document $order): JsonResponse
  {
    // Get Order Totals
    foreach ($order->totals_sorted as $order_total) {
      $order->{$order_total->code} = $order_total->amount;
    }

    $currency_code = $order->currency_code;

    $total = money($order->total, $currency_code, true)->format();
    $order->grand_total = money($total, $currency_code)->getAmount();

    return response()->json($order);
  }

  public function update(Document $order, Request $request): JsonResponse
  {
    $response = $this->ajaxDispatch(new UpdateDocument($order, $request));
    
    if ($response['success']) {
      $message = trans(
        'messages.success.deleted',
        ['type' => trans_choice('pos::general.orders', 1)]
      );

      flash($message)->success();
    } else {
      $message = $response['message'];

      flash($message)->error()->important();
    }

    return response()->json($response);
  }

  public function destroy(Document $order): JsonResponse
  {

    $response = $this->ajaxDispatch(new DeleteDocument($order));

    if ($response['success']) {
      $message = trans(
        'messages.success.deleted',
        ['type' => trans_choice('pos::general.orders', 1)]
      );

      flash($message)->success();
    } else {
      $message = $response['message'];

      flash($message)->error()->important();
    }

    return response()->json($response);
  }

  public function showReceipts(Document $order)
  {
    return view('pos::orders.receipt', compact('order'));
  }
}
