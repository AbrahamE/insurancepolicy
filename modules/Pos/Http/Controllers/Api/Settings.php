<?php

namespace Modules\Pos\Http\Controllers\Api;

use App\Abstracts\Http\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Settings extends Controller
{
    use \App\Traits\Modules;

    protected $alias_pos_order = "pos.pos_order";
    protected $alias_general = "pos.general";
    protected $alias_apollo_clea = "pos.pos_order.config";
    protected $alias_card_payment_method = "pos.general.card_payment_method";

    public function index()
    {
        return response()->json(setting($this->alias_pos_order));
    }

    public function updateConfigsPos(Request $request): JsonResponse
    {
        $fields = $request->only(['number_prefix', 'number_digit', 'number_next']);

        foreach ($fields as $key => $value) {
            setting()->set($this->alias_pos_order . '.' . $key, $value);
        }

        setting()->save();

        $message = trans(
            'messages.success.updated',
            ['type' => trans_choice('general.settings', 2)]
        );

        $response = [
            'status'   => null,
            'success'  => true,
            'error'    => false,
            'message'  => $message,
            'data'     => null,
        ];

        flash($message)->success();

        return response()->json($response);
    }

    public function updateConfigsClea(Request $request): JsonResponse
    {
        $cleaConfig = json_decode(
            setting($this->alias_apollo_clea)
        );

        $newConfig = [];

        foreach ((array) $cleaConfig as $key => $value) {
            $newConfig[$key] = $request->input($key) ?? $value;
        }
        setting()->set($this->alias_apollo_clea, json_encode($newConfig));

        setting()->save();

        $message = trans(
            'messages.success.updated',
            ['type' => trans_choice('general.settings', 2)]
        );

        $response = [
            'status'   => null,
            'success'  => true,
            'error'    => false,
            'message'  => $message,
            'data'     => null,
        ];

        flash($message)->success();

        return response()->json($response);
    }

    public function storePosOrder(Request $request): JsonResponse
    {
        foreach ($request->all() as $key => $value) {
            setting()->set($this->alias_pos_order . '.' . $key, $value);
        }

        setting()->save();

        return response()->json(
            [
                "success" => true,
                "setting" => response()->json(
                    setting($this->alias_pos_order)
                )
            ]
        );
    }

    public function storeApoloClea(Request $request): JsonResponse
    {
        $cleaConfig = (array) json_decode(setting($this->alias_apollo_clea));

        $cleaConfig = array_merge($cleaConfig, $request->all());

        setting()->set($this->alias_apollo_clea, json_encode($cleaConfig));

        setting()->save();

        return response()->json(
            [
                "success" => true,
                "setting" => json_decode(
                    setting($this->alias_apollo_clea)
                )
            ]
        );
    }

    public function deleteConfigPos(Request $request): JsonResponse
    {
        setting()->forget($this->alias_pos_order . "." . $request->input('delete'));

        setting()->save();

        return response()->json(
            setting($this->alias_pos_order)
        );
    }

    public function deleteConfigApolloClea(Request $request): JsonResponse
    {
        $cleaConfig = (array) json_decode(setting($this->alias_apollo_clea));

        unset($cleaConfig[$request->input('delete')]);

        setting()->set($this->alias_apollo_clea, json_encode($cleaConfig));

        setting()->save();

        return response()->json(
            [
                "success" => true,
                "setting" => json_decode(
                    setting($this->alias_apollo_clea)
                )
            ]
        );
    }

    public function getConfigs(): JsonResponse
    {
        $config = json_decode(setting($this->alias_apollo_clea));

        $config->isActiveApollo =
            setting($this->alias_card_payment_method) == "apollo-payment";

        return response()->json($config);
    }
}
