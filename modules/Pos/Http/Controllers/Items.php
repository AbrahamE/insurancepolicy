<?php

namespace Modules\Pos\Http\Controllers;

use App\Abstracts\Http\Controller;
use App\Models\Common\Item;
use App\Traits\Modules;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use Modules\Pos\Services\InventoryIntegration;
use Modules\Inventory\Models\Item as InventoryItem;
// use Modules\Inventory\Models\Item;

class Items extends Controller
{
    use Modules;

    public function __construct()
    {
        $this->middleware('permission:read-common-items')->only('index');
    }

    public function index(InventoryIntegration $inventory_integration_service): JsonResponse
    {
        $items = Item::select([
            'items.id as id',
            'items.name as name',
            'items.sku as sku',
            'items.description as description',
            'items.category_id as category_id',
            'items.sale_price as price',
            'inventory_items.warehouse_id as warehouse_id'
        ])
        ->join('inventory_items', 'items.id', '=', 'inventory_items.item_id')
        ->with(['taxes', 'category', 'pos_barcode'])
        ->where('warehouse_id', $inventory_integration_service->get_assigned_warehouse()->id)
        ->when($inventory_integration_service->integration_is_enabled(), function ($query) use ($inventory_integration_service) {
            $query->withSum([
                'inventory as stock' => function (Builder $query) use ($inventory_integration_service) {
                    $query->where('warehouse_id', $inventory_integration_service->get_assigned_warehouse()->id);
                },
            ], 'opening_stock');
        })
        ->collect();

        foreach ($items as $item) {
            $item->pos_ean_upc_barcode = $item->pos_barcode->code;
            $item->img = $item->picture ? Storage::url($item->picture->id) : null;//url('/modules/Pos/Resources/assets/img/item-placeholder.png');
        }

        return response()->json($items);
    }
}
