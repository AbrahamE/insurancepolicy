<?php

namespace Modules\Pos\Providers;

use App\Models\Auth\User;
use App\Models\Common\Item;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider as Provider;
use Modules\Pos\Models\Barcode;
use Modules\Pos\Models\UserWarehouse;

class Main extends Provider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViews();
        $this->loadTranslations();
        $this->loadMigrations();
        $this->loadConfig();
        $this->registerDynamicRelations();
        $this->registerObservers();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutes();
    }

    /**
     * Load views.
     *
     * @return void
     */
    public function loadViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'pos');
    }

    /**
     * Load translations.
     *
     * @return void
     */
    public function loadTranslations()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'pos');
    }

    /**
     * Load migrations.
     *
     * @return void
     */
    public function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    public function loadConfig()
    {
        $replaceConfigs = ['setting', 'search-string', 'type'];

        foreach ($replaceConfigs as $config) {
            Config::set($config, array_merge_recursive(
                Config::get($config),
                require __DIR__ . '/../Config/' . $config . '.php'
            ));
        }

//        $this->mergeConfigFrom(__DIR__ . '/../Config/config.php', 'expenses');
    }

    public function registerDynamicRelations()
    {
        Item::resolveRelationUsing('pos_barcode', function ($item) {
            return $item->hasOne(Barcode::class, 'item_id', 'id')
                ->withDefault(['code' => '']);
        });
        User::resolveRelationUsing('user_warehouse', function ($user) {
            return $user->hasOne(UserWarehouse::class, 'user_id', 'id');
        });
    }

    public function registerObservers()
    {
        Item::observe('Modules\Pos\Observers\Item');
        User::observe('Modules\Pos\Observers\User');
    }

    /**
     * Load routes.
     *
     * @return void
     */
    public function loadRoutes()
    {
        if (app()->routesAreCached()) {
            return;
        }

        $routes = [
            'admin.php',
            'api.php'
        ];

        foreach ($routes as $route) {
            $this->loadRoutesFrom(__DIR__ . '/../Routes/' . $route);
        }
    }
}
