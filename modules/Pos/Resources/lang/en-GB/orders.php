<?php

return [
    'issued_at'       => 'Processed Date',
    'document_number' => 'Order Number',

    'change' => 'Change',

    'messages' => [
        'marked_cancelled' => 'Order marked as cancelled!',
        'negative_inventory_control' => 'Cannot process an order with negative inventory!',
    ],
];
