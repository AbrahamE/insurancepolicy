<?php

return [

    'bank_card_payment_method' => [
        'name'        => 'Tarjeta bancaria',
        'description' => 'Pago con tarjeta bancaria. Este método de pago se utiliza en la aplicación PoS.',
    ],

    'categories' => [
        'pos_sale'   => 'PoS Venta',
        'pos_change' => 'PoS Cambio',
    ],

    'accounts' => [
        'cash' => 'Efectivo',
        'bank' => 'Banco',
    ],

    'order' => [
        'order_numbering' => 'Numeración de pedidos',
        'prefix'          => 'Prefijo numérico',
        'digit'           => 'Dígito numérico',
        'next'            => 'Número siguiente',
    ],

    'cash' => [
        'payments_by_cash' => 'Pagos en Efectivo',
    ],

    'card' => [
        'payments_by_card' => 'Pagos Con Tarjeta',
    ],

    'general' => [
        'other'                        => 'Otro',
        'inventory_integration'        => 'Integración de inventario',
        'guest_customer'               => 'Cliente invitado',
        'sale_category'                => 'Categoría de venta',
        'change_category'              => 'Cambiar categoría',
        'account'                      => 'Cuenta',
        'printer_paper_size'           => 'Tamaño del papel de la impresora',
        'use_paper_cutter'             => 'Usar cortador de papel',
        'negative_inventory_control'   => 'Control de inventario negativo', // new field to check an inventory with negative numbers
        'show_logo_in_receipt'         => 'Mostrar el logotipo de la empresa en el recibo',
        'use_barcode_scanner'          => 'Usar escáner de código de barras',
        'enable_inventory_integration' => 'Habilitar la integración de inventario',
    ],

    'inventory' => [
        'assign_warehouses_to_salesclerks' => 'Asignar almacenes a vendedores',
    ],

    'email' => [
        'templates' => [
            'order_receipt_customer' => 'Plantilla de recibo de pedido (enviado al cliente)',
        ],
    ],

];
