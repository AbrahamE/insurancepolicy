<?php

return [
    'clear_customer'  => 'Borrar Cliente',
    'search_customer' => 'Buscar Cliente',
    'qty'             => 'Qty',
    'disc'            => 'Disc',
    'price'           => 'Price',
    'total'           => 'Total',
    'back'            => 'Atrás',
    'validate'        => 'Validar',
    'remaining'       => 'Restante',
    'change'          => 'Cambiar',
    'total_due'       => 'Total a pagar',
    'cash'            => 'Efectivo',
    'card'            => 'Tarjeta',
    'receipt'         => 'Recibo',
    'discount'        => 'Discount',
    'served_by'       => 'Atendido por',
];
