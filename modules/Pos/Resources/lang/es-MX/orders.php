<?php

return [
    'issued_at'       => 'Fecha de procesamiento',
    'document_number' => 'Número de orden',

    'change' => 'Cambiar',

    'messages' => [
        'marked_cancelled' => 'Pedido marcado como cancelado!',
        'negative_inventory_control' => 'No se puede procesar una orden con inventario negativo!',
    ],
];
