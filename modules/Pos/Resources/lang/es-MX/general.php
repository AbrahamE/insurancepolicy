<?php

return [

    'name'        => 'PoS',
    'description' => 'Automatice su comercio con la aplicación Punto de Venta',

    'orders' => 'Orden|Órdenes',

    'barcode' => 'Código de Barra',

    'empty' => [
        'orders' => 'Los pedidos guardan datos financieros de las compras realizadas a través de PoS. Revisa la documentación para más detalles.',
    ],
];
