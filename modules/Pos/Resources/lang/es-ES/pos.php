<?php

return [
    'clear_customer'  => 'Borrar Cliente',
    'search_customer' => 'Buscar Cliente',
    'qty'             => 'Cant',
    'disc'            => 'Desc',
    'price'           => 'Precio',
    'total'           => 'Total',
    'back'            => 'Atrás',
    'validate'        => 'Validar',
    'remaining'       => 'Restante',
    'change'          => 'Cambiar',
    'total_due'       => 'Total a pagar',
    'cash'            => 'Efectivo',
    'card'            => 'Tarjeta',
    'receipt'         => 'Recibo',
    'discount'        => 'Descuento',
    'served_by'       => 'Atendido por',
];
