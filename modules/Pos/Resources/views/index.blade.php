<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8; charset=ISO-8859-1" />

    <title>@setting('company.name')</title>

    <base href="{{ config('app.url') . '/' }}">

    <!-- Favicon -->
    @if ($custom_icon = setting('white-label.icon', false))
        <link rel="icon" href="{{ asset(route('uploads.get', $custom_icon)) }}" type="image/png">
    @else
        <link rel="icon" href="{{ asset('public/img/favicon.ico') }}" type="image/png">
    @endif

    <!-- Font -->
    <link rel="stylesheet" href="{{ asset('public/vendor/opensans/css/opensans.css?v=' . version('short')) }}"
        type="text/css">

    <!-- Icons -->
    <link rel="stylesheet" href="{{ asset('public/vendor/nucleo/css/nucleo.css?v=' . version('short')) }}"
        type="text/css">
    <link rel="stylesheet" href="{{ asset('public/vendor/fontawesome/css/all.min.css?v=' . version('short')) }}"
        type="text/css">

    <!-- Css -->
    {{-- <link rel="stylesheet" href="{{ asset('public/css/argon.css?v=' . version('short')) }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('public/css/akaunting-color.css?v=' . version('short')) }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('public/css/custom.css?v=' . version('short')) }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('public/css/element.css?v=' . version('short')) }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('modules/Pos/Resources/assets/css/app.css?v=' . module_version('pos')) }}" type="text/css"> --}}
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet"
        href="{{ asset('modules/TestVue/Resources/assets/css/app.min.css?v=' . module_version('pos')) }}"
        type="text/css">


    @stack('stylesheet')

    <script type="text/javascript">
        var url = '{{ url('/' . company_id()) }}';
        var app_url = '{{ config('app.url') }}';
        var aka_currency = {!! !empty($currency) ? $currency : 'false' !!};
    </script>

    <script type="text/javascript">
        window.Laravel = {{ Illuminate\Support\Js::from([
            'csrfToken' => csrf_token(),
        ]) }};
        window.company_id = {{ company_id() }};
        window.Device = navigator.userAgent.match(/Android/i) ? true : false;

        if (navigator.userAgent.match(/Android/i)) document.documentElement.requestFullscreen();
    </script>
</head>

<body>
    <div id="pos">
        {{-- <app company-name="{{ setting('company.name') }}" user-name="{{ user()->name }}"
            :currency="{{ $currency }}" :translations="{{ json_encode($translations) }}"
            :use-barcode-scanner="{{ setting('pos.general.use_barcode_scanner') === '0' ? 'false' : 'true' }}"
            :inventory-integration="{{ $enable_inventory_integration ? 'true' : 'false' }}"
            @if ($warehouse) :warehouse="{{ json_encode($warehouse) }}" @endif>
        </app> --}}

        <div id="app">
            <pos company-name="{{ setting('company.name') }}" user-name="{{ user()->name }}"
                :taxes="{{ json_encode($taxes) }}" :currency="{{ $currency }}"
                :translations="{{ json_encode($translations) }}"
                :use-barcode-scanner="{{ setting('pos.general.use_barcode_scanner') === '0' ? 'false' : 'true' }}"
                :inventory-integration="{{ $enable_inventory_integration ? 'true' : 'false' }}"
                @if ($warehouse) :warehouse="{{ json_encode($warehouse) }}" @endif>
            </pos>
        </div>

    </div>

    {{-- <script src="{{ asset('modules/Pos/Resources/assets/js/app.min.js?v=' . module_version('pos')) }}"></script> --}}

    <script src="{{ asset('modules/TestVue/Resources/assets/js/app.min.js?v=0.9') }}"></script>
</body>

</html>
