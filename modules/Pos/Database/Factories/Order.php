<?php

namespace Modules\Pos\Database\Factories;

use App\Models\Common\Contact;
use App\Models\Common\Item;
use App\Traits\Documents;
use Database\Factories\Document as DocumentFactory;
use Modules\Pos\Models\Order as Model;

class Order extends DocumentFactory
{
    use Documents;

    protected $model = Model::class;

    public function definition(): array
    {
        $contact = Contact::customer()->enabled()->inRandomOrder()->first();
        if (!$contact) {
            $contact = Contact::factory()->customer()->enabled()->create();
        }

        return array_merge(parent::definition(), [
            'type'               => Model::TYPE,
            'document_number'    => $this->getNextDocumentNumber(Model::TYPE),
            'category_id'        => setting('pos.general.sale_category_id'),
            'customer_id'        => $contact->id,
            'contact_id'         => $contact->id,
            'contact_name'       => $contact->name,
            'contact_email'      => $contact->email,
            'contact_tax_number' => $contact->tax_number,
            'contact_phone'      => $contact->phone,
            'contact_address'    => $contact->address,
            'status'             => 'processed',
            'change'             => $this->faker->randomFloat(2, 0.1, 1000000),
        ]);
    }

    public function items(): Order
    {
        return $this->state(function (array $attributes) {
            $item = Item::enabled()->inRandomOrder()->first();
            if (!$item) {
                $item = Item::factory()->enabled()->create();
            }

            return [
                'items' => [
                    [
                        'item_id'       => $item->id,
                        'price'         => $this->faker->randomFloat(2, 0.1, 1000000),
                        'quantity'      => 1,
                        'discount'      => $this->faker->randomNumber(2),
                        'discount_type' => 'percentage',
                        'name'          => $item->name,
                        'description'   => $item->description,
                    ],
                ],
            ];
        });
    }

    public function payments(): Order
    {
        return $this->state(function (array $attributes) {
            return [
                'payments' => [
                    [
                        'type'   => 'cash',
                        'amount' => $this->faker->randomNumber(3),
                    ],
                ],
            ];
        });
    }
}
