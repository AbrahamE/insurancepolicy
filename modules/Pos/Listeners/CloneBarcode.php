<?php

namespace Modules\Pos\Listeners;

class CloneBarcode
{
    public function handle($clone, $original)
    {
        $clone->pos_barcode()->create([
            'company_id' => $clone->company_id,
            'code'       => $original->pos_barcode->code,
        ]);
    }
}
