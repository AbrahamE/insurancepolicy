<?php

namespace Modules\Pos\Listeners\Update;

use App\Abstracts\Listeners\Update as Listener;
use App\Events\Install\UpdateFinished as Event;
use App\Traits\Modules;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Modules\Pos\Models\Barcode;

class Version1100 extends Listener
{
    use Modules;

    const ALIAS = 'pos';

    const VERSION = '1.10.0';

    public function handle(Event $event)
    {
        if ($this->skipThisUpdate($event)) {
            return;
        }

        $this->runMigrations();

        $this->replicateInventoryBarcodes();
    }

    private function runMigrations()
    {
        Artisan::call('module:migrate', ['alias' => self::ALIAS, '--force' => true]);
    }

    private function replicateInventoryBarcodes()
    {
        if (!$this->moduleIsEnabled('inventory')) {
            return;
        }

        try {
            DB::transaction(function () {
                $inventory_barcodes = DB::table('inventory_items')->whereNotNull('barcode')->cursor();

                foreach ($inventory_barcodes as $inventory_barcode) {
                    Barcode::updateOrCreate(
                        [
                            'company_id' => $inventory_barcode->company_id,
                            'item_id'    => $inventory_barcode->item_id,
                        ],
                        [
                            'code' => $inventory_barcode->barcode,
                        ]
                    );
                }
            });
        } catch (\Throwable $e) {
            \Log::info('PoS::unable to copy the Inventory\'s barcodes during the module update');
        }
    }
}
