<?php

namespace Modules\Pos\Observers;

use App\Abstracts\Observer;
use App\Models\Auth\User as Model;

class User extends Observer
{
    public function deleted(Model $user)
    {
        $user->user_warehouse()->delete();
    }
}
