<?php

use Illuminate\Support\Facades\Route;

/**
 * 'api' middleware and 'api/my-blog' prefix applied to all routes (including names)
 *
 * @see \App\Providers\Route::register
 */

Route::api('pos', function ($api) {
    // // NOTE: Ordes 
    $api->post('orders/create', 'Orders@store')->name('.pos.order.create');
    $api->get('orders/receipts/{order}', 'Orders@showReceipts')->name('.pos.order.receipts');
    $api->put('orders/update/{order}', 'Orders@update')->name('.pos.order.update');
    $api->delete('orders/destroy/{order}', 'Orders@destroy')->name('.pos.order.destroy');
    $api->resource('orders/list', 'Orders');

    // // NOTE: Settings pos
    $api->resource('settings/list_pos', 'Settings');
    $api->get('settings/list_clea', 'Settings@getConfigs');

    $api->post('settings/create_configs_pos', 'Settings@storePosOrder')->name('.pos.settings.create_configs_pos');
    $api->post('settings/create_configs_clea', 'Settings@storeApoloClea')->name('.pos.settings.create_configs_clea');

    $api->put('settings/update_configs_pos', 'Settings@updateConfigsPos')->name('.pos.settings.update_configs_pos');
    $api->put('settings/update_configs_clea', 'Settings@updateConfigsClea')->name('.pos.settings.update_configs_clea');
    
    $api->delete('settings/destroy_configs_pos', 'Settings@deleteConfigPos')->name('.pos.settings.destroy_configs_pos');
    $api->delete('settings/destroy_configs_clea', 'Settings@deleteConfigApolloClea')->name('.pos.settings.destroy_configs_clea');
   
});
