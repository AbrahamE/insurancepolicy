<?php

namespace Modules\Pos\Jobs;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldCreate;
use App\Jobs\Document\CreateDocument;
use App\Models\Common\Contact;
use App\Traits\Documents;
use Modules\Pos\Models\Order;

class CreateOrder extends Job implements ShouldCreate
{
    use Documents;

    public function handle()
    {
        $this->prepareRequest();
        \DB::transaction(function () {
            $this->model = $this->dispatch(new CreateDocument($this->request));
            
            $this->dispatch(new CreateOrderTransactions($this->model, $this->request));
        });

        return $this->model;
    }

    protected function prepareRequest()
    {
        $currency_code = setting('default.currency');
        $contact = Contact::find($this->request['customer_id'] ?? setting('pos.general.guest_customer_id'));

        $this->request['currency_code'] = $currency_code;
        $this->request['currency_rate'] = config('money.' . $currency_code . '.rate');
        $this->request['document_number'] = $this->getNextDocumentNumber(Order::TYPE);
        $this->request['contact_id'] = $contact->id;
        $this->request['contact_name'] = $contact->name;
        $this->request['contact_email'] = $contact->email;
        $this->request['contact_tax_number'] = $contact->tax_number;
        $this->request['contact_phone'] = $contact->phone;
        $this->request['contact_address'] = $contact->address;
        $this->request['category_id'] = setting('pos.general.sale_category_id');
    }
}
