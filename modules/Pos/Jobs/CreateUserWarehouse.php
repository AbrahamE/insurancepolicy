<?php

namespace Modules\Pos\Jobs;

use App\Abstracts\Job;
use App\Interfaces\Job\ShouldCreate;
use Modules\Pos\Models\UserWarehouse;

class CreateUserWarehouse extends Job implements ShouldCreate
{
    public function handle()
    {
        \DB::transaction(function () {
            $this->model = UserWarehouse::create($this->request->all());
        });

        return $this->model;
    }
}
