<?php

namespace Modules\Pos\Models;

use App\Abstracts\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserWarehouse extends Model
{
    protected $table = 'pos_user_warehouse';

    protected $fillable = ['company_id', 'user_id', 'warehouse_id'];

    public function warehouse(): BelongsTo
    {
        return $this->belongsTo('\Modules\Inventory\Models\Warehouse');
    }
}
