@extends('layouts.admin')

@section('title', trans_choice('inventory::general.adjustments', 2))


@section('content')
   <div id='app'>
        <app></app>
   </div>
@endsection

@push('scripts_start')
    <script src="{{ asset('modules/InvoiceModeling/Resources/assets/js/app.js?v=' . module_version('invoice-modeling')) }}"></script>   
@endpush

