<?php

use Illuminate\Support\Facades\Route;

/**
 * 'admin' middleware and 'invoice-modeling' prefix applied to all routes (including names)
 *
 * @see \App\Providers\Route::register
 */

Route::admin('invoice-modeling', function () {
    Route::get('/', 'Main@index')->name('modeling');
    // Route::get('invoice-modeling', 'Main')

    // Route::get('items', 'Items@index')->name('items.index');
});
