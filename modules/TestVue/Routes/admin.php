<?php

use Illuminate\Support\Facades\Route;

/**
 * 'admin' middleware and 'test-vue' prefix applied to all routes (including names)
 *
 * @see \App\Providers\Route::register
 */

Route::admin('pos', function () {
    Route::get('pos/{any}', "Main");
});
