const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.alias({
//   "@": "/resources/src/*",
//   "@lib/*": "/resources/src/lib/*",
//   "@view/*": "/resources/src/view/*",
//   "@widget/*": "resources/src/widget/*",
//   "@components/*": "resources/src/components/*",
// });
mix
  .js("Resources/src/main.js", "Resources/assets/js/app.min.js")
  .postCss("Resources/assets/css/app.css", "Resources/assets/css/app.min.css")
  .vue({
    version: "3",
    options: {
      compilerOptions: {
        isCustomElement: (tag) => ["md-linedivider"].includes(tag),
      },
    },
  })//.sourceMaps()
