import { createApp } from "vue";
import { createPinia } from "pinia";

import emitter from "tiny-emitter/instance";
import piniaPersist from "pinia-plugin-persist";

import VueLazyLoad from "vue3-lazyload";
import { MotionPlugin } from "@vueuse/motion";
import ElementPlus from "element-plus";

import App from "./App.vue";

import elementSpanishLocale from "element-plus/dist/locale/es.min.mjs";
import * as FiscalPrinter from "./lib/plugins/Clea/FiscalPrinter";

import "moment/locale/es";
import moment from "moment";

import { router } from "./routes";

import { createRippleDirective } from "vue-create-ripple";

import "element-plus/dist/index.css";
import "../assets/css/app.css";

const pinia = createPinia();

globalThis.window.Pusher = require("pusher-js");

pinia.use(piniaPersist);

moment.locale("es");

const app = createApp({
  mounted: () => console.log("POS sale started, welcome"),
})
  .directive(
    "Ripple",
    createRippleDirective({ class: "bg-black/75 opacity-25" })
  )
  .component("pos", App)
  .use(ElementPlus, { locale: elementSpanishLocale })
  .use(MotionPlugin)
  .use(VueLazyLoad, {})
  .use(router)
  .use(pinia);

app.provide("EventBus", emitter);

if ("CleaPrinter" in window) {
  FiscalPrinter.init(emitter);
}

app.mount("#app");
