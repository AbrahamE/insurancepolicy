import {
  filter,
  sum,
  sumBy,
  isArray,
  forEach,
  mergeWith,
  result,
  findIndex,
  update,
  remove,
  isEmpty,
  find,
} from "lodash";

export const calculateAmountsTaxes = function (state) {
  let currentOrder = state ?? [];
  let groupTaxePrice = [];
  let reverseLookup = [];
  let defaultTaxe = [0];
  let groupPrice = [];
  let groupDiscount = [];
  let taxLookup = [];
  let taxNew = [];
  let taxesNewColletion = [];

  const PRODUCT_MIN = 1;
  const DEFAULT_TOTAL = "0.00";

  if (!orderProcess()) return;

  function orderProcess() {
    return currentOrder.getCurrent();
  }

  function orderProcessByID({ idItem }) {
    return find(orderProcess().items, { id: idItem }) ?? [];
  }

  function taxTotal({ price, quantity, taxesList }) {
    return (price * quantity * sumBy(taxesList, "rate")) / 100;
  }

  function filterTax({ mainTax, comparisonTax, tax, comparison, item }) {
    return filter(
      arraysBiggerThan({
        mainArray: mainTax,
        comparisonArray: comparisonTax,
        arrayReturns: tax,
        arrayComparisonReturns: comparison,
      }),
      ({ rate }) => rate == item.rate
    );
  }

  function arraysBiggerThan({
    mainArray,
    comparisonArray,
    arrayReturns,
    arrayComparisonReturns,
  }) {
    return mainArray.length > comparisonArray.length
      ? arrayReturns ?? mainArray
      : arrayComparisonReturns ?? comparisonArray;
  }

  function getProduct({ product, idProduct, filter }) {
    return result(product, filter).filter(
      (item) => item.idProduct == idProduct
    );
  }

  function justificateTax({ taxesList, idProduct, order }) {
    return taxesList.map(({ title, rate, id }) => {
      return {
        name: title,
        rate: rate,
        tax_id: id,
        total: [
          {
            idProduct,
            taxTotal: taxTotal({
              price: order.price,
              quantity: order.quantity,
              taxesList: filter(taxesList, ["id", id]),
            }),
          },
        ],
      };
    });
  }

  function taxes({ taxLookup, reverseLookup, item }) {
    return [
      filterTax({
        mainTax: taxLookup,
        comparisonTax: reverseLookup,
        tax: reverseLookup,
        comparison: taxLookup,
        item,
      }),
      filterTax({
        mainTax: taxLookup,
        comparisonTax: reverseLookup,
        tax: reverseLookup,
        comparison: taxLookup,
        item,
      }),
    ];
  }

  function taxResultLength({ tax, id }) {
    return !!result(tax, "total").filter((taxItem) => taxItem.idProduct == id)
      .length;
  }

  return {
    pricesTotal() {
      orderProcess()
        .items?.map(({ id, price, quantity, discount }) => {
          const taxes = this.taxesList({ idProduct: id, list: true });
          const sumOfTaxesRates = sumBy(taxes, "rate");
          const subtotal = price * quantity - discount;
          return subtotal + (subtotal * sumOfTaxesRates) / 100;
        })
        .forEach((price) => groupPrice.push(price));
      console.log(
        "🚀 ~ file: calculateAmountsTaxes.js:127 ~ pricesTotal ~ groupPrice:",
        groupPrice
      );

      return sum(groupPrice) ?? DEFAULT_TOTAL;
    },
    pricesSubTotal() {
      orderProcess()
        .items?.map(({ quantity, price }) => price * quantity)
        .forEach((price) => groupPrice.push(price));

      return sum(groupPrice) ?? DEFAULT_TOTAL;
    },
    pricesDiscount() {
      orderProcess()
        .items?.map(({ discount }) => discount)
        .forEach((discount) => groupDiscount.push(discount));

      return sum(groupDiscount) ?? DEFAULT_TOTAL;
    },
    pricesTaxeTotal() {
      orderProcess()
        .items?.map(({ id, price, quantity, discount }) => {
          const taxes = this.taxesList({ idProduct: id, list: true });
          const sumOfTaxesRates = sumBy(taxes, "rate");
          const subtotal = price * quantity - discount;
          return (subtotal * sumOfTaxesRates) / 100;
        })
        .forEach((price) => groupTaxePrice.push(price));

      return sum(groupTaxePrice) ?? DEFAULT_TOTAL;
    },
    selectedItemTaxes({ idItem, list }) {
      return !list
        ? orderProcessByID({ idItem: idItem }).tax_ids ?? defaultTaxe
        : orderProcessByID({ idItem: idItem }).taxe ?? defaultTaxe;
    },
    taxesList({ idProduct, list }) {
      return [...this.selectedItemTaxes({ idItem: idProduct, list })].map(
        (idItemTaxe) => {
          return state.taxes?.find(({ id }) => id == idItemTaxe);
        }
      );
    },
    taxesJustifications({ id, just }) {
      if (!isArray(just)) just = [just];

      reverseLookup = just;
      taxLookup = justificateTax({
        taxesList: this.taxesList({ idProduct: id, list: true }),
        order: orderProcessByID({ idItem: id }),
        idProduct: id,
      });

      if (!Object.entries(just).length) return taxLookup;

      forEach(
        arraysBiggerThan({
          mainArray: taxLookup,
          comparisonArray: reverseLookup,
        }),
        (item) => {
          let [taxNew, taxIsset] = taxes({
            taxLookup,
            reverseLookup,
            item,
          });

          if (taxNew.length) {
            taxesNewColletion.push(
              mergeWith(
                ...taxIsset,
                ...taxNew,
                function customizer(objValue, srcValue) {
                  if (isArray(objValue)) {
                    return objValue.concat(srcValue);
                  }
                }
              )
            );
            return false;
          }
          taxesNewColletion.push(item);
        }
      );

      return taxesNewColletion;
    },
    updateTaxesJustifications({ taxes, idTax, idProduct }) {
      let taxesList = this.taxesList({ idProduct, list: true });

      forEach(taxes, function (taxItem, index) {
        if (
          !!getProduct({ product: taxItem, idProduct, filter: "total" }).length
        ) {
          taxLookup.push(
            ...getProduct({ product: taxItem, idProduct, filter: "total" }).map(
              (tax) => {
                return {
                  idTaxes: idTax,
                  idTaxActive: tax.idProduct,
                  productActive: findIndex(taxItem.total, [
                    "idProduct",
                    tax.idProduct,
                  ]),
                };
              }
            )
          );
        }

        if (taxLookup.length) {
          taxNew = [
            ...update(
              taxes,
              `[${index}]total[${
                taxLookup[taxLookup.length - 1].productActive
              }]`,
              function () {
                return {
                  idProduct,
                  taxTotal: taxTotal({
                    taxesList: taxesList.filter(
                      (tax, index) =>
                        tax.id == taxLookup[taxLookup.length - 1].idTaxes[index]
                    ),
                    quantity: orderProcessByID({ idItem: idProduct }).quantity,
                    price: orderProcessByID({ idItem: idProduct }).price,
                  }),
                };
              }
            ),
          ];
          console.log("🚀 ~ file: item.js:235 ~ taxNew:", taxNew);
        }
      });

      return taxNew;
    },
    removeTaxJustifications({ taxes, id }) {
      if (!!id) return;
      forEach(taxes, (tax, index) => {
        if (taxResultLength({ tax, id })) {
          if (tax.total?.length > PRODUCT_MIN) {
            remove(tax.total, (taxItem) => {
              return (
                taxItem.idProduct ==
                tax.total[
                  findIndex(tax.total, function (taxItemIndex) {
                    return taxItemIndex.idProduct == id;
                  })
                ]
              );
            });
          } else {
            if (
              tax.total?.length == PRODUCT_MIN &&
              taxResultLength({ tax, id })
            )
              taxes[index] = {};
          }
        }
      });

      remove(taxes, (Tax) => {
        return isEmpty(Tax);
      });
    },
    newTax({ tax, id }) {
      return (orderProcessByID({ idItem: id }).taxe = tax ?? defaultTaxe);
    },
  };
};
