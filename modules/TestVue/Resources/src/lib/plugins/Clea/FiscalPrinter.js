import _ from "lodash";
import { TinyEmitter } from "tiny-emitter";
import { useConfigStore } from "../../store/configStore";

/**
 * @param {string} name
 * @param {any} store
 * @param {any[]} args
 * */

export default function () {}

function printInvoice(invoice, client, recibos) {
  try {
    invoice.items.forEach(({ quantity, price, name, taxes_rate }) => {
      let impuesto;

      if (taxes_rate.length == 0) impuesto = "E";
      else [impuesto] = taxes_rate;
      // else if(taxes_rate.includes(8)) impuesto = "R";
      // else if(taxes_rate.includes(16)) impuesto = "G";
      // else if(taxes_rate.includes(31)) impuesto = "A";
      // else impuesto = "P"

      window.CleaPrinter.addItem(quantity, impuesto, price, name, false);
    });

    window.CleaPrinter.setPrinterIp(useConfigStore().clea.current.ip);
    window.CleaPrinter.setPrinterPort(Number(useConfigStore().clea.port));

    window.CleaPrinter.setCustomer(
      (client.name ?? "Cliente Invitado").toUpperCase(),
      "V" + (client.tax_number ?? "0")
    );
    window.CleaPrinter.setPaymentInfo(invoice.totalOrder, "P", null, null);
    window.CleaPrinter.setInvoiceNum(invoice.id);
    const printIsSuccess = window.CleaPrinter.print();

    if (!printIsSuccess) throw new Error(window.CleaPrinter.getErrorMessage());

    recibos.forEach((recibo) => {
      recibo.split("\n").forEach((line) => {
        window.CleaPrinter.addNonFiscal(
          String(line).replace(/[\t]/g, Array(4).fill(" ").join(""))
        );
      });
      window.CleaPrinter.print();
    });
  } catch (error) {
    console.error("HA OCURRIDO UN ERROR", error);
  }
}

function printZReport(type, data) {
  window.CleaPrinter.setPrinterIp(useConfigStore().clea.current.ip);
  window.CleaPrinter.setPrinterPort(Number(useConfigStore().clea.port));
  if (type == "date")
    return window.CleaPrinter.printZReportByDate(...data.dates.map(formatDate));
  if (type == "number") {
    return window.CleaPrinter.printZReportByNumber(...data.numbers.map(Number));
  }
  window.CleaPrinter.printZReport();
}
function printXReport() {
  window.CleaPrinter.printXReport();
}

/** @param {Date} date */
function getYear(date) {
  return String(date.getUTCFullYear()).substring(2);
}
/** @param {Date} date */
function getMonth(date) {
  return String(date.getUTCMonth()).padStart(2, 0);
}
/** @param {Date} date */
function getDay(date) {
  return String(date.getUTCDay()).padStart(2, 0);
}
/** @param {Date} date */
function formatDate(date) {
  return /\d{2}(\d{2})-(\d{2})-(\d{2})/g.exec(date).slice(1).join("");
}

/**
 * @param {TinyEmitter} emitter
 * */
export function init(emitter) {
  console.log("Apollo Clea Printer Initialice successfull");
  emitter.on("payment:completed", printInvoice);
  emitter.on("report:z", printZReport);
  emitter.on("report:x", printXReport);
}
