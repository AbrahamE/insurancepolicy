import { awaitAndThrow, MINUTE, SECOND, wait, } from "../utils/async"
import useAxios from "./useAxios"
const { axiosInstance } = useAxios()

export async function startPayment(transId) {
  // await this.#markLastTransactionTimestampInService(sessionName)

  await wait(20 * SECOND)

  return await Promise.race([ 
    awaitAndThrow(wait(2 * MINUTE), new Error('Tiempo limite excedido')),
    startLoopToGetTransaction(transId) 
  ])

  // return startLoopToGetTransaction(transId)
}

async function startLoopToGetTransaction(transId) {
  while (true) {

    const transactionResponse = await getTransaction(transId)

    if (!hasTransaction(transactionResponse)) {
      await wait(3 * SECOND)
      continue

    };
    if (!isApprovedTransaccion(transactionResponse))
      throw new Error('Transaccion no Aprobada compruebe los datos de la operacion y vuelva intentarlo')

    return transactionResponse.data.data.transactions;
  }
}
function isApprovedTransaccion(transResponse) {
  return transResponse.data.data.transactions.mensaje != "NO APROBADO"
}

function hasTransaction(transactionResponse) {
  return transactionResponse.status == 200
    && transactionResponse.data.data
    && transactionResponse.data.data.transactions;
}

function getTransaction(transId) {
  return axiosInstance.get('apollo-payment/sell/' + transId)
}
