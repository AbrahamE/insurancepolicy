import axios from "axios";

const axiosInstance = axios.create({
  baseURL: globalThis.window.url,
  headers: {
    "Content-Type": "multipart/form-data",
    "X-CSRF-TOKEN": globalThis.window.Laravel.csrfToken,
  },
});

const axiosInstanceFlat = axios.create({ baseURL: globalThis.window.url });

/** @param {import("axios").AxiosInstance} axiosInstance */
function buildUseAxios(axiosInstance){
  return {
    axiosInstance,
    axiosGet: async ({ url }) => {
      let res = await axiosInstance.get(url);
      return res;
    },
    axiosPost: async ({ url, data }) => {
      let res = await axiosInstance.post(url, data, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      return res;
    },
  };
}

export default () => buildUseAxios(axiosInstance);
export const useAxiosFlat = () => buildUseAxios(axiosInstanceFlat)

