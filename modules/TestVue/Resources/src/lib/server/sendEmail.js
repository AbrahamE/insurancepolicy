import useAxios from "./useAxios";
const { axiosInstance } = useAxios();

export default function sendReceiptToEmail(receiptId, email) {
  return axiosInstance.post(`pos/receipts/${receiptId}/send`, { email }, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
