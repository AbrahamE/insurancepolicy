import useAxios from "./useAxios";

const { axiosInstance } = useAxios();

export default function getReceipt(receiptId) {
  return axiosInstance.get(`pos/receipts/${receiptId}`, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
