import useAxios from "./useAxios";
import moment from "moment";

const { axiosInstance } = useAxios();

export default async function createOrder(products, client, payments) {

  const data = JSON.stringify({
    customer_id: client.id,
    type: "pos-order",
    company_id: window.company_id,
    status: "processed",
    issued_at: moment().format('yyyy-MM-DD HH:mm:ss'),
    due_at: moment().format('yyyy-MM-DD HH:mm:ss'),
    items: products.map((product) => ({
      item_id: product.id,
      price: product.price,
      quantity: product.quantity,
      discount: product.discount,
      discount_type: "",
      name: product.name,
      description: product.description,
      warehouse_id: product.warehouse_id,
      tax_ids: product.tax_ids
    })),
    payments,
    change: 0
  });
  return await axiosInstance.post('pos/orders', data, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
