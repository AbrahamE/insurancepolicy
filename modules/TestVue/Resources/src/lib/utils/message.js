const messagesByCode = {
  ERR_NETWORK:
    "Error de conexion, comprube la configuracion del terminal o su conexion local",
  DEFAULT_MESSAGE:
    "Ha ocurrido un error, compruebe sus datos o intente mas tarde",
  "Cannot read properties of null (reading 'ip')":
    "No hay Terminal Seleccionado seleccione uno para continuar",
};
export function errorMessageByCode(code) {
  return messagesByCode[code] ?? messagesByCode["DEFAULT_MESSAGE"];
}
