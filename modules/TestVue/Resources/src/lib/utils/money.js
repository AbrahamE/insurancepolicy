import Accounting from "accounting";
import { useCompanyStore } from "../store/companyStore";

const DEFAULT_PRESICION = 2;
const DEFAULT_CUANTITY = "0";
const DEFAULT_DECIMAL_CUANTITY = "00";
const DEFAULT_DECIMAL_SEPARATOR = ",";
const DEFAULT_THOUSANDS_SEPARATOR = ".";
const US_DECIMAL_SEPARATOR = ".";

export function unformat(value) {
  return Accounting.unformat(value, DEFAULT_DECIMAL_SEPARATOR);
}

export function parseQuantities(currentQuantity) {
  return currentQuantity
    .toString()
    .replace(DEFAULT_DECIMAL_SEPARATOR, US_DECIMAL_SEPARATOR);
}

export function formatQuantities(currentQuantity) {
  let [integers, decimals] = currentQuantity.split(US_DECIMAL_SEPARATOR);
  if (integers && decimals)
    if (
      decimals.slice(0, -1) == DEFAULT_DECIMAL_CUANTITY &&
      integers.slice(0, -1) == DEFAULT_DECIMAL_CUANTITY
    ) {
      return DEFAULT_DECIMAL_CUANTITY;
    }

  return [integers, decimals ?? DEFAULT_DECIMAL_CUANTITY].join(
    US_DECIMAL_SEPARATOR
  );
}

export function formatNumber(value, presicion = DEFAULT_PRESICION) {
  const currency = useCompanyStore().currency;
  return Accounting.formatMoney(
    value,
    currency.symbol,
    presicion,
    DEFAULT_THOUSANDS_SEPARATOR,
    DEFAULT_DECIMAL_SEPARATOR
  );
}

export function formatAmount(value) {
  const currency = useCompanyStore().currency;

  return currency.symbol_first
    ? currency.symbol + value
    : value + currency.symbol;
}

export function flattenAmounts(currentQuantity) {
  return currentQuantity.toString().replace(US_DECIMAL_SEPARATOR, "");
}

export function toNumber(value, presicion = DEFAULT_PRESICION) {
  const nums = new String(value?.replaceAll(/[^0-9]/g, ""));

  const integers = nums.substring(0, nums.length - presicion || 0);
  const decimals = nums.substring(nums.length - presicion);

  return String(
    integers.replaceAll(/^0+/g, "").padStart(1, "0") +
      "." +
      decimals.padStart(presicion, "0")
  );
}

export function addToQuantity(valueToAdd, currentQuantity) {
  let updatedDecimalQuantity = currentQuantity
    .toString()
    .replace(US_DECIMAL_SEPARATOR, "");

  updatedDecimalQuantity += valueToAdd;

  let decimals = updatedDecimalQuantity.slice(-DEFAULT_PRESICION).toString();
  let integers = updatedDecimalQuantity.slice(0, -DEFAULT_PRESICION).toString();

  return [Number(integers), decimals].join(US_DECIMAL_SEPARATOR);
}

export function sunToQuantity(valueToAdd, currentQuantity) {
  let updatedDecimalQuantity = parseFloat(currentQuantity);

  updatedDecimalQuantity += valueToAdd;

  let outputQuantity = updatedDecimalQuantity.toLocaleString("es-ES", {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
    decimalSeparator: DEFAULT_DECIMAL_SEPARATOR,
    thousandsSeparator: DEFAULT_THOUSANDS_SEPARATOR,
  });

  return outputQuantity;
}

export function sunToQuantityProduct(valueToAdd, currentQuantity) {
  let updatedDecimalQuantity = parseFloat(currentQuantity);
  updatedDecimalQuantity += valueToAdd;
  return updatedDecimalQuantity;
}

export function remToQuantity(currentQuantity) {
  let updatedDecimalQuantity = currentQuantity
    .toString()
    .replace(US_DECIMAL_SEPARATOR, "");
  console.log(
    "🚀 ~ file: money.js:113 ~ remToQuantity ~ updatedDecimalQuantity:",
    updatedDecimalQuantity
  );

  updatedDecimalQuantity =
    DEFAULT_CUANTITY + updatedDecimalQuantity.slice(0, -1);
  console.log(
    "🚀 ~ file: money.js:115 ~ remToQuantity ~ updatedDecimalQuantity:",
    updatedDecimalQuantity
  );

  let integers = updatedDecimalQuantity.slice(0, -DEFAULT_PRESICION).toString();
  console.log("🚀 ~ file: money.js:119 ~ remToQuantity ~ integers:", integers);
  let decimals = updatedDecimalQuantity.slice(-DEFAULT_PRESICION).toString();
  console.log("🚀 ~ file: money.js:119 ~ remToQuantity ~ decimals:", decimals);

  return [Number(integers), decimals].join(US_DECIMAL_SEPARATOR);
}

export function getAllNumbers() {
  return value.replaceAll(/[^0-9]/g, "");
}
