export function wait(ms) {
  return new Promise((res) => setTimeout(res, ms))
}
export const sleep = wait;
export const SECOND = 1000
export const MINUTE = SECOND * 60

export const awaitAndThrow = (promise, exception) => promise.then(() => {
  throw exception
})
