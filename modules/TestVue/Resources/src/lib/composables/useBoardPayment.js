import { reactive, computed, defineComponent } from "vue";
import { UilCancel } from "@iconscout/vue-unicons";
import { UilRefresh } from "@iconscout/vue-unicons";

const useBoardPayment = () => {
  const board$ = reactive({
    boadPayment: [
      [
        { id: 7, name: new String("7"), value: 7, active: false, fun: null },
        { id: 8, name: new String("8"), value: 8, active: false, fun: null },
        { id: 9, name: new String("9"), value: 9, active: false, fun: null },
        // {
        //   id: 3,
        //   name: new String("+5"),
        //   value: "incremente.10",
        //   active: true,
        //   fun: null,
        // },
      ],
      [
        { id: 4, name: new String("4"), value: 4, active: false, fun: null },
        { id: 5, name: new String("5"), value: 5, active: false, fun: null },
        { id: 6, name: new String("6"), value: 6, active: false, fun: null },
        // {
        //   id: 13,
        //   name: new String("+10"),
        //   value: "discount",
        //   active: false,
        //   fun: null,
        // },
      ],
      [
        { id: 1, name: new String("1"), value: 1, active: false, fun: null },
        { id: 2, name: new String("2"), value: 2, active: false, fun: null },
        { id: 3, name: new String("3"), value: 3, active: false, fun: null },
        // {
        //   id: 14,
        //   name: new String("+50"),
        //   value: "price",
        //   active: false,
        //   fun: null,
        // },
      ],
      [
        {
          id: 11,
          name: defineComponent(UilCancel),
          value: "remove",
          active: false,
          fun: null,
        },
        { id: 0, name: new String("0"), value: 0, active: false, fun: null },
        {
          id: 10,
          name: defineComponent(UilRefresh),
          value: "reset",
          active: false,
          fun: null,
        },
        {
          id: 12,
          name: new String("+100"),
          value: "price",
          active: false,
          fun: null,
        },
      ],
    ],
  });

  return {
    boardPayment: computed(() => board$.boadPayment),
    count: computed(() => count$),
  };
};

export default useBoardPayment;
