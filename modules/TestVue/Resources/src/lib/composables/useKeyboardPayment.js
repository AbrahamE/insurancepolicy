import { range } from "lodash";
import {
  addToQuantity,
  sunToQuantity,
  remToQuantity,
  parseQuantities,
  formatQuantities,
} from "../utils/money";
import { useKeyboardStore } from "../store/keyboardStore";
import { usePaymentStore } from "../store/paymentStore";
import { defineComponent } from "vue";
import { UilCancel } from "@iconscout/vue-unicons";

const useBoardPayment = () => {
  const KeyboardStore = useKeyboardStore();
  const PaymentStore = usePaymentStore();

  const onAddQuantityPrice = (value) => {
    let updatedQuantity = addToQuantity(
      value,
      PaymentStore.getActivePaymentMethod.amount
    );
    return updatedQuantity;
  };

  const onSunQuantityPrice = (value) => {
    let updatedQuantity = sunToQuantity(
      value,
      PaymentStore.getActivePaymentMethod.amount
    );
    return updatedQuantity;
  };

  const onRemoveQuantityPrice = (value) => {
    let updatedQuantity = remToQuantity(
      PaymentStore.getActivePaymentMethod.amount
    );
    return updatedQuantity;
  };

  const getNumericKeysFromRange = (start, end) =>
    range(start, end).map((number) => ({
      id: number,
      name: String(number),
      value: number,
      active: false,
      fun: onAddQuantityPrice,
    }));

  const getSumKey = (title, value) => ({
    id: title,
    name: String(title),
    value,
    active: false,
    fun: onSunQuantityPrice,
  });

  const keyboardKeys = [
    [...getNumericKeysFromRange(7, 10), getSumKey("+5", 5)],
    [...getNumericKeysFromRange(4, 7), getSumKey("+10", 10)],
    [...getNumericKeysFromRange(1, 4), getSumKey("+50", 50)],
    [
      {
        id: 10,
        name: defineComponent(UilCancel),
        value: "remove",
        active: false,
        fun: onRemoveQuantityPrice,
      },
      {
        id: 0,
        name: String("0"),
        value: 0,
        active: false,
        fun: onAddQuantityPrice,
      },
      {
        id: 11,
        name: String("00"),
        value: "00",
        active: false,
        fun: onAddQuantityPrice,
      },
      getSumKey("+100", 100),
    ],
  ];

  function onClickHandler(key) {
    if (key.fun) {
      let value = formatQuantities(parseQuantities(key.fun(key.value)));

      KeyboardStore.onSetPrice({
        price: value,
      });

      PaymentStore.onNewPriceSelected({
        id: PaymentStore.getActivePaymentMethod.id,
        value,
      });
    }
    return key;
  }

  return {
    KeyboardKeysConfig: keyboardKeys,
    onClickHandler,
    onAddQuantityPrice,
    onSunQuantityPrice,
  };
};

export default useBoardPayment;
