import { ref, computed } from "vue";

import Echo from "laravel-echo";

export default useScreenShare = () => {
  let broadcaster = ref("pusher");
  let key = ref("local");
  let wsHost = ref("127.0.0.1");
  let wsPort = ref(6001);
  let cluster = ref("");
  let forceTLS = ref(false);
  let disableStats = ref(true);

  globalThis.window.Echo = new Echo({
    broadcaster: broadcaster.value,
    key: key.value,
    wsHost: wsHost.value,
    wsPort: wsPort.value,
    cluster: cluster.value,
    forceTLS: forceTLS.value,
    disableStats: disableStats.value,
  });

  return {
    onConnect: () => {
      globalThis.window.Echo.connector.socket.on("connect", function () {
        this.isConnected = true;
      });
    },
    onDisconnect: () => {
      globalThis.window.Echo.connector.socket.on("disconnect", function () {
        this.isConnected = false;
      });
    },
    onContactUpdated: ({ fn, params }) => {
      globalThis.window.Echo.private("contacts").listen(
        "ContactUpdated",
        (event) => {
          console.log(event);
          if (typeof fn == "function") fn(params);
        }
      );
    },
    getSettings: () => {
      return {
        broadcaster: computed(() => broadcaster.value),
        key: computed(() => key.value),
        wsHost: computed(() => wsHost.value),
        wsPort: computed(() => wsPort.value),
        cluster: computed(() => cluster.value),
        forceTLS: computed(() => forceTLS.value),
        disableStats: computed(() => disableStats.value),
      };
    },
    setSettings: ({
      broadcaster,
      key,
      wsHost,
      wsPort,
      cluster,
      forceTLS,
      disableStats,
    }) => {
      key.value = key ?? key.value;
      wsHost.value = wsHost ?? wsHost.value;
      wsPort.value = wsPort ?? wsPort.value;
      cluster.value = cluster ?? cluster.value;
      forceTLS.value = forceTLS ?? forceTLS.value;
      broadcaster.value = broadcaster ?? broadcaster.value;
      disableStats.value = disableStats ?? disableStats.value;

      return this.getSettings();
    },
  };
};
