import { defineComponent, reactive, ref } from "vue";
import { range, filter } from "lodash";
import {
  addToQuantity,
  sunToQuantityProduct,
  remToQuantity,
} from "../utils/money";

import { UilCancel } from "@iconscout/vue-unicons";
import { UilRefresh } from "@iconscout/vue-unicons";

import { useKeyboardStore } from "../store/keyboardStore";
import { useOrderStore } from "../store/orderStore";

const useKeyboardShopping = () => {
  let modifier = ref("quantity");
  const DEFAULT_MODIFIER = "quantity";
  const KeyboardStore = useKeyboardStore();
  const orderStore = useOrderStore();

  const closureKeyboardAction = {
    quantity: KeyboardStore.onSetQuantities,
    discount: KeyboardStore.onSetDiscount,
    price: KeyboardStore.onSetPrice,
  };
  const closureOrderAction = {
    quantity: orderStore.onEditAmountItemOrder,
    discount: orderStore.onAplicateDiscountItemOrder,
    price: orderStore.onEditPriceItemOrder,
  };

  const onChangelQuantities = (value) => {
    let updatedQuantity = closureKeyboardAction[modifier.value]({
      [modifier.value]:
        modifier.value == DEFAULT_MODIFIER
          ? sunToQuantityProduct(value, KeyboardStore[modifier.value])
          : addToQuantity(value, KeyboardStore[modifier.value]),
    });
    onRunClosuresFunctionOrderStore(updatedQuantity);
    return updatedQuantity;
  };

  const onChangelAction = ({ modifier$ }) => {
    keyboardKeys.map((key) => {
      return (
        filter(key, function (o) {
          if (o.value == modifier$) {
            o.active = true;
            return o;
          } else o.active = false;
          return o;
        }) || key
      );
    });
    modifier.value = modifier$;
  };

  const onRemoveQuantityPrice = () => {
    let updatedQuantity = remToQuantity(KeyboardStore[modifier.value]);
    onRunClosuresFunctionKeyboardStore(updatedQuantity);
    onRunClosuresFunctionOrderStore(updatedQuantity);
    return updatedQuantity;
  };

  const onResetQuantities = () => {
    KeyboardStore.onResetQuantities();
    // orderStore.onResetQuantities();
    orderStore.onResetItemOrder({ id: orderStore.getItemActive.id });
  };

  const onRunClosuresFunctionOrderStore = (updatedQuantity) => {
    closureOrderAction[modifier.value]({
      id: orderStore.getItemActive.id,
      count: updatedQuantity,
    });
  };

  const onRunClosuresFunctionKeyboardStore = (updatedQuantity) => {
    closureKeyboardAction[modifier.value]({
      [modifier.value]: updatedQuantity,
    });
  };

  const getNumericKeysFromRange = (start, end) =>
    range(start, end).map((number) => ({
      id: number,
      name: String(number),
      value: number,
      active: false,
      fun: onChangelQuantities,
    }));

  const keyboardKeys = reactive([
    [
      ...getNumericKeysFromRange(7, 10),
      {
        id: 3,
        name: String("cant"),
        value: "quantity",
        active: true,
        fun: (value) => onChangelAction({ modifier$: value }),
      },
    ],
    [
      ...getNumericKeysFromRange(4, 7),
      {
        id: 13,
        name: String("desc"),
        value: "discount",
        active: false,
        fun: (value) => onChangelAction({ modifier$: value }),
      },
    ],
    [
      ...getNumericKeysFromRange(1, 4),
      {
        id: 14,
        name: String("prec"),
        value: "price",
        active: false,
        fun: (value) => onChangelAction({ modifier$: value }),
      },
    ],
    [
      {
        id: 11,
        name: defineComponent(UilCancel),
        value: "remove",
        active: false,
        fun: onRemoveQuantityPrice,
      },
      {
        id: 0,
        name: String("0"),
        value: "0",
        active: false,
        fun: onChangelQuantities,
      },
      {
        id: 10,
        name: String("00"),
        value: "00",
        active: false,
        fun: onChangelQuantities,
      },
      {
        id: 12,
        name: defineComponent(UilRefresh),
        value: "reset",
        active: false,
        fun: onResetQuantities,
      },
    ],
  ]);

  function onClickHandler(key) {
    if (orderStore.getItemActive) if (key.fun) key.fun(key.value);
    return key;
  }

  return {
    KeyboardKeysConfig: keyboardKeys,
    onClickHandler,
    onChangelAction,
  };
};

export default useKeyboardShopping;
