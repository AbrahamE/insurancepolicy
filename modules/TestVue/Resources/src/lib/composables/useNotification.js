import { h } from "vue";
import { ElNotification } from "element-plus";

export const useNotification = () => {
  return {
    create: ({ title, message, type, elemente, style }) => {
      ElNotification({
        title,
        message: h(
          elemente ?? "span",
          style ?? {
            style: "color: teal",
          },
          message
        ),
        type: type ?? "info",
      });
    },
  };
};
