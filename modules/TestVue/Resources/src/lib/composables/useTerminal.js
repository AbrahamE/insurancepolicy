import { useAxiosFlat } from "./../server/useAxios";
import { startPayment } from "./../server/apolloPaymentVerificator";
import { useConfigStore, Methods } from "./../store/configStore";

export const useTerminal = () => {
  const { axiosPost } = useAxiosFlat();
  const configStore = useConfigStore();

  return {
    onPurchase: async (data) => {
      const url = `${configStore.terminalProtocol}://${configStore.currentTerminal.ip}:${configStore.terminalPort}`;
      const res = await axiosPost({
        url: `${url}/${Methods.PURCHASE}`,
        data: {
          operacion: Methods.PURCHASE.toUpperCase(),
          ...data,
        },
      });
      return await startPayment(res.data.id_trans);
    },
    onAnnulment: () => {
      const url = `${configStore.terminalProtocol}://${configStore.currentTerminal.ip}:${configStore.terminalPort}`;
      const res = axiosPost({
        url: `${url}/${Methods.PURCHASE}`,
        data: {
          operacion: Methods.ANNULMENT.toUpperCase(),
        },
      });
      console.log(res);
      return res;
    },
    onClosing: () => {
      const url = `${configStore.terminalProtocol}://${configStore.currentTerminal.ip}:${configStore.terminalPort}`;
      const res = axiosPost({
        url: `${url}/${Methods.PURCHASE}`,
        data: {
          operacion: Methods.CLOSING.toUpperCase(),
        },
      });
      console.log(res);
      return res;
    },
  };
};
