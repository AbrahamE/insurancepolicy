import { defineStore } from "pinia";
import { parseQuantities, formatQuantities, toNumber } from "../utils/money";

export const useKeyboardStore = defineStore("keyBoard", {
  state: () => ({
    price: "0.00",
    discount: "0.00",
    quantity: 0,
    quantitiesDefault: {
      price: "0.00",
      discount: "0.00",
      quantity: 0,
    },
  }),
  getters: {
    getPrice: (state) => state.price,
    getDiscount: (state) => state.discount,
    getQuantity: (state) => state.quantity,
    getQuantitesDefault: (state) => state.quantitiesDefault,
  },
  actions: {
    onSetPrice({ price }) {
      this.price = formatQuantities(price);
      return this.price;
    },
    onSetDiscount({ discount }) {
      this.discount = formatQuantities(discount);
      return this.discount;
    },
    onSetQuantities({ quantity }) {
      this.quantity = Number(quantity);
      return this.quantity;
    },
    onResetQuantities() {
      this.quantity = this.quantitiesDefault.quantity ?? "0.00";
      this.price = this.quantitiesDefault.price ?? "0.00";
      this.discount = this.quantitiesDefault.discount ?? "0.00";
    },
    onSetAmountDefault({ price, discount, quantity }) {
      this.price = formatQuantities(
        parseQuantities(price.toString().concat(".00"))
      );
      this.discount = formatQuantities(
        parseQuantities(discount.toString().concat(".00"))
      );
      this.quantity = Number(quantity);
    },
    onSetQuantitiesDefault({ price, discount, quantity }) {
      this.quantitiesDefault = {
        price: formatQuantities(
          parseQuantities(price.toString().concat(".00"))
        ),
        discount: formatQuantities(
          parseQuantities(discount.toString().concat(".00"))
        ),
        quantity: Number(quantity),
      };
    },
  },
});
