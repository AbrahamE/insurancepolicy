import { defineStore } from "pinia";
import useAxios from "../server/useAxios";
import { MINUTE, SECOND } from "../utils/async";

export const Methods = {
  PURCHASE: "compra",
  ANNULMENT: "anulacion",
  CLOSING: "cierre",
};
export const RefreshTimes = Object.freeze([
  { time: 30 * SECOND, label: "30 Segundos" },
  { time: MINUTE, label: "1 Minuto" },
  { time: 2 * MINUTE, label: "2 Minutos" },
  { time: 5 * MINUTE, label: "5 Minutos" },
  { time: 0, label: "Nunca" },
]);

export const useConfigStore = defineStore("config", {
  state: () => ({
    terminalList: [],
    clea: {
      printers: [],
      port: null,
      currentPrinter: null,
    },
    currentTerminal: null,
    terminalPort: 8085,
    terminalProtocol: "https",
    general: {
      informationRefreshInterval: MINUTE,
    },
    isActiveApollo: false,
  }),
  getters: { getUrlConnectionPoint: (state) => state.urlConnectionPoint },
  actions: {},
  persist: {
    enabled: false,
    strategies: [{ storage: localStorage }],
  },
});

// useConfigStore().$subscribe((mutation, state) => {
//   // import { MutationType } from 'pinia'
//   mutation.type // 'direct' | 'patch object' | 'patch function'
//   // same as cartStore.$id
//   mutation.storeId // 'cart'
//   // only available with mutation.type === 'patch object'
//   mutation.payload // patch object passed to cartStore.$patch()

//   // persist the whole state to the local storage whenever it changes
//   // useAxios().axiosPost({url = "pos/", state})

//   // localStorage.setItem('', JSON.stringify(state))
// })
