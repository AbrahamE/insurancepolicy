import { defineStore } from "pinia";
import { Totals } from "../plugins/Amounts/calculateAmountsTaxes";

import moment from "moment";

export const useProductStore = defineStore("product", {
  state: () => ({
    products: [],
    searchCategories: [],
    searchText: "",
  }),
  getters: {
    leakedProducts(state) {
      const searchText = state.searchText.toLowerCase();
      const leakedProducts = state.products
        .filter(
          ({ name, pos_ean_upc_barcode }) =>
            name.toLowerCase().includes(searchText) ||
            pos_ean_upc_barcode.toLowerCase().includes(searchText)
        )
        .filter(
          (product) =>
            state.searchCategories.length == 0 ||
            state.searchCategories.includes(product.category.name)
        );
      return leakedProducts ?? [];
    },
    getCategories: ({ products }) => [
      ...new Set(products.map(({ category }) => category.name)),
    ],
  },
  actions: {},
  persist: {
    enabled: false,
    strategies: [{ storage: localStorage }],
  },
});
