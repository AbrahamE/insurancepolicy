import { defineStore } from "pinia";
import { calculateAmountsTaxes } from "../plugins/Amounts/calculateAmountsTaxes";

import moment from "moment";
const DEFAULT_COUNT = "0";
const DEFAULT_INDEX = "0";

const useOrderStore = defineStore("order", {
  state: () => ({
    process: 0,
    _processing: false,
    processId: 0,
    orders: [],
    openOrders: [
      {
        id: 0,
        active: true,
        name: moment().format("MMM-DD-YY, h:mm"),
      },
    ],
    products: [],
    clients: [],
    createClient: {},
    taxes: [],
  }),
  getters: {
    getProcess: (state) => state.process,
    getTotalOrders: (state) =>
      state.getCurrent() ? state.getCurrent().totalOrder || "0.00" : "0.00",
    getTotalDiscount: (state) =>
      state.getCurrent() ? state.getCurrent().totalDiscount || "0.00" : "0.00",
    getSubTotalOrders: (state) =>
      state.getCurrent() ? state.getCurrent().subtotalOrder || "0.00" : "0.00",
    getTaxesTotalOrders: (state) =>
      state.getCurrent() ? state.getCurrent().taxtotalOrder || "0.00" : "0.00",
    getJustificateTaxesOrders: (state) =>
      state.getCurrent() ? state.getCurrent().taxesJustifications : [],
    getOrders: (state) => state.orders,
    getOpenOrders: (state) => state.openOrders,
    getClient: (state) =>
      state.getCurrent() ? state.getCurrent().client.name : "",
    getProductsCategory: (state) => {
      let category = [];
      state.products.map((product) => category.push(product.category.name));
      return [...new Set(category)];
    },
    getItemActive: (state) =>
      state.getCurrent()
        ? state.getCurrent().items?.find((item) => item.edit)
        : {},
    getBack: (state) => state.getCurrent()?.back,
    getBackActive: (state) =>
      state
        .getCurrent()
        ?.back.find((item) => item.id == state.getItemActive.id) ??
      (state.getBack[DEFAULT_INDEX] || []),
  },
  actions: {
    totals() {
      return calculateAmountsTaxes(this);
    },
    /* functions to calculate total, subtotal and taxes */
    onTotalAmount() {
      this.onSubTotalAmount();
      this.onTotalTaxe();
      this.onTotalDiscount();
      return (this.getCurrent().totalOrder = this.totals()
        .pricesTotal()
        .toFixed(2));
    },
    onSubTotalAmount() {
      return (this.getCurrent().subtotalOrder = this.totals()
        .pricesSubTotal()
        .toFixed(2));
    },
    onTotalDiscount() {
      return (this.getCurrent().totalDiscount = this.totals()
        .pricesDiscount()
        .toFixed(2));
    },
    onTotalTaxe() {
      return (this.getCurrent().taxtotalOrder = this.totals()
        .pricesTaxeTotal()
        .toFixed(2));
    },
    onNewTaxes({ tax, id }) {
      if (!this.getCurrent()) return;
      this.totals().newTax({ tax, id });
      this.onAddJustificateTax(id);
      this.onTotalAmount();
    },
    onAddJustificateTax(id) {
      this.getCurrent().taxesJustifications = this.totals().taxesJustifications(
        {
          just: this.getCurrent().taxesJustifications,
          id,
        }
      );
    },
    onUpdateJustificateTax({ idProduct, idsTax }) {
      this.getCurrent().taxesJustifications =
        this.totals().updateTaxesJustifications({
          taxes: this.getCurrent().taxesJustifications,
          idTax: idsTax,
          idProduct: idProduct,
        }) ?? [];
    },
    onRemoveJustificateTax({ id }) {
      this.totals().removeTaxJustifications({
        taxes: this.getCurrent().taxesJustifications,
        id,
      });
    },
    onGetTaxesItem(id, list) {
      this.getCurrent().taxesJustifications = this.totals().taxesJustifications(
        {
          just: this.getCurrent().taxesJustifications,
          id,
        }
      );
      return this.totals().selectedItemTaxes({ idItem: id, list });
    },
    onTaxesList(id, list) {
      return this.totals().taxesList({ idProduct: id, list });
    },
    onCreateOrder() {
      this.orders.push({
        id: this.processId,
        date: moment().format("MMM-DD-YY, h:mm"),
        totalOrder: "0.00",
        totalDiscount: "0.00",
        subtotalOrder: "0.00",
        taxtotalOrder: "0.00",
        itemSelect: 0,
        taxesJustifications: [],
        items: [],
        back: [],
        client: {},
        purchasingController: {
          count: "0",
          // onDelete: (ctx$) => {
          //   if (state.purchasingController.count === DEFAULT_COUNT) return;

          //   let _count;
          //   if (ctx$ === TYPE_OPTION_AMOUNT) {
          //     _count = parseFloat(state.purchasingController.count);
          //     state.purchasingController.count = (_count - 1).toString();
          //   }

          //   if (ctx$ === TYPE_OPTION_DISCOUNT || ctx$ === TYPE_OPTION_PRICE) {
          //     if (state.purchasingController.count.length != 1) {
          //       this.purchasingController.count = state.count.slice(0, state.count.length - 1);
          //     } else {
          //       this.purchasingController.count = DEFAULT_COUNT;
          //     }
          //   }
          // },
          // onReset: (ctx$) =>
          //   ctx$
          //     ? (state.purchasingController.count = ctx$.toString())
          //     : (state.purchasingController.count = DEFAULT_COUNT),
        },
      });
    },
    resetCurrentPurchaseController(num) {
      this.getCurrent().purchasingController.count = num
        ? num.toString()
        : DEFAULT_COUNT;
    },
    onOpenOrder() {
      this.openOrders.push({
        name: moment().format("MMM-DD-YY, h:mm"),
        active: false,
        id: this.processId,
      });
      this.onProcessiOrder(this.processId);
    },
    onQuickSale() {
      if (!this.getCurrent()) this.onCreateOrder();
      this.processId++;
      this.onCreateOrder();
      this.onOpenOrder();
    },
    onDeleteOrder(_dctx) {
      if (!_dctx) return;

      this.orders = this.orders.filter((_ictx) => {
        return _ictx.id !== _dctx;
      });

      this.openOrders = this.openOrders.filter((_ictx) => {
        return _ictx.id !== _dctx;
      });
      this.process--;
      this.processId--;
    },
    onDeleteItemOrder(_dctx) {
      const _items = this.getCurrent().items?.filter(
        (_ictx) => _ictx.id != _dctx
      );
      const _itemsBack = this.getCurrent().back?.filter(
        (_ictx) => _ictx.id != _dctx
      );

      this.onEditItemOrder(_dctx - 1);
      this.onTotalAmount();
      this.getCurrent().items = _items;
      this.getCurrent().back = _itemsBack;
    },
    onProcessiOrder(_pctx) {
      if (!this.orders[_pctx]) this.onCreateOrder();
      this.process = _pctx;

      this.openOrders.map((_ictx) => {
        _ictx.id == _pctx
          ? !_ictx.active
            ? (_ictx.active = !_ictx.active)
            : (_ictx.active = true)
          : (_ictx.active = false);
      });

      return this.process;
    },
    onAddClientOrder(_Clctx) {
      if (!this.getCurrent()) this.onCreateOrder();
      this.getCurrent().client = _Clctx;
    },
    onEditItemOrder(idItem) {
      this.getCurrent().items?.forEach((item) => {
        if (item.id != idItem) item.edit = false;
        else item.edit = true;
      });

      // this.resetCurrentPurchaseController();
    },
    onRunFunction({ count, _fun, id }) {
      if (!id && id != 0) return;
      const KeyBoardFunction = {
        amount: this.onEditAmountItemOrder,
        discount: this.onAplicateDiscountItemOrder,
        price: this.onEditPriceItemOrder,
      };
      KeyBoardFunction[_fun]({ id, count });
      this.onTotalAmount();
    },
    onEditPriceItemOrder({ id, count }) {
      this.findItemInCurrent({ id }).price = Number(count);
      this.onTotalAmount();
    },
    onEditAmountItemOrder({ id, count }) {
      this.findItemInCurrent({ id }).quantity = Number(count);
      this.onTotalAmount();
    },
    onAplicateDiscountItemOrder({ id, count }) {
      this.findItemInCurrent({ id }).discount = Number(count);
      this.onTotalAmount();
    },
    onResetItemOrder({ id }) {
      const _ictx = this.findItemInCurrent({ id });
      const back = this.getBack.find((ictx) => ictx.id == id);
      _ictx.price = back.price;
      _ictx.quantity = back.quantity;
      _ictx.discount = back.discount;
      this.onTotalAmount();
    },
    onItemBack() {
      if (!this.getCurrent()) return;
      const back = this.getCurrent().items?.map((item) => {
        return {
          id: item.id,
          price: item.price,
          quantity: item.quantity,
          discount: item.discount,
        };
      });

      return [...JSON.parse(JSON.stringify(back))];
    },
    hasOrders() {
      return this.orders.length > 0;
    },
    getCurrent() {
      return this.orders[this.process];
    },
    findItemInCurrent(item) {
      if (this.hasOrders())
        return this.getCurrent().items.find((item$) => item$.id === item.id);
    },
    // findItemInCurrentActive(item) {
    //   if (this.hasOrders())
    //     return this.getCurrent().items.find((item) => item.id == item.getItemActive.id) ?? {}
    // },
    createPurchasingController(state) {
      return;
    },
    incrementItem(product) {
      this.$patch(() => {
        const item = this.getCurrent().items.find(
          (item) => item.id === product.id
        );
        if (!item) return;
        item.quantity++;
        this.onTotalAmount();
        this.onUpdateJustificateTax({
          idProduct: product.id,
          idsTax: product.tax_ids,
        });
      });
    },
    decrementItem(product) {
      let decrement = false;
      this.$patch((state) => {
        const item = state.orders[state.process].items.find(
          (item) => item.id === product.id
        );
        if (!item) return;
        if (item.quantity <= 0) {
          this.onDeleteItemOrder(item.id);
          this.onEditItemOrder(item.id - 1);
          this.onRemoveJustificateTax({ id: product.id, product: product });
        } else {
          item.quantity--;
          this.onUpdateJustificateTax({
            idProduct: product.id,
            idsTax: product.tax_ids,
          });
        }
        this.onTotalAmount();
        return (decrement = true);
      });

      return decrement;
    },
    createProduct(product) {
      this.$patch((state) => {
        if (!this.getCurrent()) this.onOpenOrder();
        if (!this.getCurrent()) this.onCreateOrder();

        /** @var {use} */
        const order = state;

        // product.taxes_rate = product.tax_ids
        //   .map((id) => this.taxes?.find((tax) => id == tax.id))
        //   .map(({ types_fiscal }) => types_fiscal)
        //   .filter((v) => v);

        // const item = order.items[order.items.push(product)];

        order.back = this.onItemBack();

        // const hasPurchasingController =
        //   Object.keys(order.purchasingController).length > 0;
        // if (!hasPurchasingController)
        //   order.purchasingController = {
        //     ...this.createPurchasingController(item),
        //   };

        this.onEditItemOrder(product.id);
        this.onTotalAmount();
      });
    },
    chooseProduct(product) {
      let newProduct = {
        quantity: 1,
        edit: false,
        discount: 0,
        taxe: product.tax_ids ?? [],
        ...product,
      };
      if (this.findItemInCurrent(newProduct)) this.incrementItem(newProduct);
      else this.createProduct(newProduct);
    },
  },
  persist: {
    enabled: false,
    strategies: [{ storage: localStorage }],
  },
});

// let totals = Totals(useOrderStore);
// onUpdated(() => (totals = Totals(useOrderStore)));

export { useOrderStore };
