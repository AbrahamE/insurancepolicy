import { defineStore } from "pinia";
import { useTerminal } from "../composables/useTerminal";
import { parseQuantities } from "../utils/money";

import { useConfigStore } from "./configStore";

export const usePaymentStore = defineStore("payments", {
  state: () => ({
    availablePaymentMethods: [
      {
        id: 1,
        name: "EFECTIVO",
        staticAmounts: ["5", "10", "20", "50", "100"],
        icon: "cash",
        active: true,
        serialize: (payment) => ({
          type: "cash",
          amount: parseQuantities(payment.amount.toString()),
        }),
      },
      {
        id: 2,
        name: "POR TARJETA",
        icon: "save",
        active: false,
        additionalFields: [
          {
            name: "C.I.",
            id: "cedula",
            type: "number",
            required: true,
          },
        ],
        handler: ({ amount, inputs }) =>
          useConfigStore().isActiveApollo
            ? useTerminal().onPurchase({
                ...inputs,
                monto: amount.toString(),
              })
            : true,
        serialize: (payment) => ({
          type: "card",
          amount: parseQuantities(payment.amount.toString()),
          reference: payment.result.referencia,
          currrency_code: "Bs. ",
        }),
      },
    ],
    selectedPaymentMethods: [],
  }),
  getters: {
    getPaymentAmount({ selectedPaymentMethods }) {
      return selectedPaymentMethods
        .map(({ amount }) => amount)
        .reduce((acc, amount) => amount, 0);
    },
    getTotalPaymentAmount({ selectedPaymentMethods }) {
      return selectedPaymentMethods
        .map(({ amount }) => Number(parseQuantities(amount)))
        .reduce((acc, amount) => acc + amount, 0);
    },
    getPaymentMethodsValidsForHandler({ selectedPaymentMethods }) {
      return selectedPaymentMethods.filter((pm) => !pm.completed);
    },
    paymentReceipts({ selectedPaymentMethods }) {
      selectedPaymentMethods
        .map((p) => ({
          ref: p.result?.referencia,
          receipt: p.result?.recibo,
        }))
        .filter((p) => p.result?.recibo);
    },
    getActivePaymentMethod() {
      return this.selectedPaymentMethods.find((pm) => pm.active);
    },
  },
  actions: {
    getPaymentTypeDef(type) {
      return this.availablePaymentMethods.find(({ id }) => id == type);
    },

    getStaticAmounts({ paymentType }) {
      return this.getPaymentTypeDef(paymentType)?.staticAmounts;
    },

    getAdditionalFields(paymentType) {
      return this.getPaymentTypeDef(paymentType)?.additionalFields;
    },

    onNewPriceSelected({ id, value }) {
      this.selectedPaymentMethods.find((pm) => pm.id == id).amount =
        value.toString();
    },

    addPaymentMethod(order) {
      const amount = order?.totalOrder - this.getPaymentAmount;
      this.selectedPaymentMethods.push({
        id: this.selectedPaymentMethods.length + 1,
        amount:
          this.getPaymentAmount == 0
            ? parseQuantities(order?.totalOrder)
            : parseQuantities(amount.toString()),
        paymentType: 1,
        inputs: { cedula: order?.client?.tax_number },
        active: true,
      });
    },

    addNewPaymentMethod({ id }, order) {
      this.availablePaymentMethods.forEach((payament) => {
        if (id == payament.id) {
          if (!payament.active) payament.active = !payament.active;
        } else {
          if (payament.active) payament.active = !payament.active;
        }
      });

      this.selectedPaymentMethods.push({
        id: this.selectedPaymentMethods.length + 1,
        amount: 0,
        paymentType: id,
        inputs: { cedula: order?.client?.tax_number },
        active: false,
      });
      this.onAvtivePaymentMethod({
        id: this.selectedPaymentMethods.length,
      });
    },
    onAvtivePaymentMethod({ id }) {
      this.selectedPaymentMethods.forEach((payament) => {
        if (id == payament.id) {
          if (!payament.active) payament.active = !payament.active;
        } else {
          if (payament.active) payament.active = !payament.active;
        }
      });
    },
    removePaymentMethod(idx) {
      this.selectedPaymentMethods.splice(idx, 1);
      this.onAvtivePaymentMethod({ id: this.selectedPaymentMethods.length });
    },
    async handlePaymentMethods() {
      const results = [];

      for (const method of this.getPaymentMethodsValidsForHandler) {
        const { handler } = this.getPaymentTypeDef(method.paymentType);
        if (!handler) {
          method.completed = true;
          continue;
        }

        const paymentResult = await handler(method);
        method.completed = true;
        method.result = paymentResult;
        results.push(paymentResult);
      }
      return results;
    },
    cancelPayments() {
      this.selectedPaymentMethods = this.selectedPaymentMethods((pm) => ({
        ...pm,
        completed: false,
      }));
    },
    getSerializedPayments() {
      return this.selectedPaymentMethods.map((payment) =>
        this.getPaymentTypeDef(payment.paymentType).serialize(payment)
      );
    },
  },
  // persist: {
  //   enabled: false,
  //   strategies: [{ storage: localStorage }],
  // },
});
