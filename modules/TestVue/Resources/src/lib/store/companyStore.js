import { defineStore } from "pinia";

export const useCompanyStore = defineStore("company", {
  state: () => ({
    userName: "",
    companyName: "",
    currency: {},
    translations: {},
    taxes: {},
    useBarcodeScanner: false,
    inventoryIntegration: false,
    warehouse: {},
    admin: false,
  }),
  getters: {},
  actions: {},
  persist: {
    enabled: true,
    strategies: [{ storage: localStorage }],
  },
});
