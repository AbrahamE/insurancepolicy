import { createRouter, createWebHistory } from "vue-router";

import RPayment from "../view/ReadyPayment.vue";
import Ppayment from "../view/PrePayment.vue";
import ReceiptPayment from "../view/ReceiptPayment.vue";

export const router = createRouter({
  history: createWebHistory(
    new URL(globalThis.window.url).pathname + "/pos/pos"
  ),
  routes: [
    { path: "/", component: Ppayment },
    { path: "/pay", component: RPayment },
    { path: "/receipt", component: ReceiptPayment },
  ],
});
