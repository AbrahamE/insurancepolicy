@extends('layouts.admin')

@section('content')
<h1>Hello World</h1>

<p>
  This view is loaded from module: {!! config('test-vue.name') !!}
</p>

<div id="app">

</div> 

@stop

@section('footer')

<script src="{{ asset('modules/TestVue/Resources/assets/js/test-vue.min.js?v=' . module_version('test-vue')) }}"></script>
@stop
