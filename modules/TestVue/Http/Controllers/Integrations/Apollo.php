<?php

namespace Modules\TestVue\Http\Controllers;

use App\Abstracts\Http\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class Apollo extends PaymentController
{
  const BASE_URL = 'https://services.cleatsi.com:9092/api/merchant';

  const METHOD_LAST_TRANSACTION_DATE = 'getLastTransactionDate';
  const METHOD_GET_TRANSACTION = 'getTransactions';
  const METHOD_KEY = 'method';

  const REFERENCE_KEY = 'reference';

  const ERROR_STATUS = 'error';
  const SUCCESS_STATUS = 'success';

  function __construct()
  {
    $this->alias = 'ApolloCLEA';
  }


  public function saveLastPaymentReference(Request $req)
  {
    $req->validate([
      'session' => 'required'
    ]);
    try {
      $this->saveReference($this->getLastPaymentTimestamp());
    } catch (\Exception) {
      return response(json_encode([
        "status" => self::ERROR_STATUS,
        "message" => ''
      ], Response::HTTP_OK));
    }

    return response(json_encode([
      "status" => self::SUCCESS_STATUS,
      "message" => ''
    ], Response::HTTP_OK));
  }

  public function checkNewPaymentsFromLastReference(Request $req)
  {
    $req->validate([
      'amount' => 'required',
      'identity' => 'required'
    ]);
    $transactionInfo = $req->only(['amount', 'identity']);
    try {
      $this->searchLastPaymentFromReference($this->retriveReference(), $transactionInfo);
      return response(json_encode([
        "status" => self::SUCCESS_STATUS,
        "message" => ''
      ], Response::HTTP_OK));
    } catch (\Exception) {
      return response(json_encode([
        "status" => self::ERROR_STATUS,
        "message" => ''
      ], Response::HTTP_OK));
    }
  }

  private function getLastPaymentTimestamp()
  {
    $lastPaymentTimestampBody = $this->getLastPaymentTimestampFromServices()->json();

    throw_if(
      !$lastPaymentTimestampBody || !is_array($lastPaymentTimestampBody),
      parameters: 'No se ha podido parsear el cuerpo de la peticion'
    );
    return $lastPaymentTimestampBody['data']['date'];
  }

  private function getLastPaymentTimestampFromServices()
  {
    $body = [self::METHOD_KEY => self::METHOD_LAST_TRANSACTION_DATE];
    return Http::post(self::BASE_URL, json_encode($body));
  }
  private function getLastPaymentFromService($reference, $identity, $amount)
  {
    $body = [
      'fecha' => $reference,
      'cedula' => $identity,
      'monto' => $amount,
      self::METHOD_KEY => self::METHOD_GET_TRANSACTION
    ];
    return Http::post(self::BASE_URL, json_encode($body));
  }
  private function searchLastPaymentFromReference($reference, $transactionInfo)
  {
    $response = $this->getLastPaymentFromService($reference, $transactionInfo['identity'], $transactionInfo['amount'])->json();

    throw_if(
      !$response || !is_array($response),
      parameters: 'No se ha podido parsear el cuerpo de la peticion'
    );
    return $response;
  }

  private function saveReference($reference)
  {
    session($this->alias + '_reference', $reference);
  }

  private function retriveReference()
  {
    return session($this->alias + '_reference');
  }
}
