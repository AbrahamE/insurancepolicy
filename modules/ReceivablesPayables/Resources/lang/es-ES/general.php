<?php

return [

    'name'                  => 'Cuentas por cobrar y por pagar',
    'description'           => 'Articulos para cuentas por cobrar y por pagar.',

    'closest_receivables'   => 'Cuentas por cobrar más cercanas',
    'closest_payables'      => 'Pagos más cercanos',
    'receivables_overdue'   => 'Créditos vencidos',
    'payables_overdue'      => 'Cuentas por pagar vencidas',
    'overdue_1_30'          => '1-30 días de retraso',
    'overdue_30_60'         => '30-60 días de retraso',
    'overdue_60_90'         => '60-90 días de retraso',
    'overdue_90_un'         => '> 90 días de retraso',

];
