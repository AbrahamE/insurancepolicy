<?php

return [

    'reports' => [

        'name' => [
            'profit_loss'       => 'Pérdidas y ganancias (COA)',
            'income_summary'    => 'Resumen de ingresos (COA)',
            'expense_summary'   => 'Resumen de gastos (COA)',
            'income_expense'    => 'Ingresos frente a gastos (COA)',
        ],

        'description' => [
            'general_ledger'    => 'Lista detallada de todas las transacciones y su total.',
            'balance_sheet'     => 'Una instantánea de su negocio.',
            'trial_balance'     => 'Balance de todo su plan de cuentas.',
            'profit_loss'       => 'Pérdidas y ganancias trimestrales por plan de cuentas.',
            'income_summary'    => 'Resumen de los ingresos mensuales por cuadro de cuentas.',
            'expense_summary'   => 'Resumen mensual de los gastos por cuadro de cuentas.',
            'income_expense'    => 'Ingresos mensuales frente a gastos por cuadro de cuentas.',
            'journal_report'    => 'Lista detallada de todos los asientos.',
        ],
    ],

];
