<?php

return [

    'name'                  => 'Contabilidad',
    'description'           => 'Plan de Cuentas, Libro Diario, Libro Mayor, y más',

    'accounting'            => 'Contabilidad',
    'chart_of_accounts'     => 'Catálogo de Cuenta|Catálogo de Cuentas',
    'coa'                   => 'COA',
    'journal_entry'         => 'Libro Diario',
    'general_ledger'        => 'Libro Mayor',
    'balance_sheet'         => 'Balance de situación',
    'trial_balance'         => 'Balance de comprobación',
    'journal_report'        => 'Libro Diario',
    'account'               => 'Cuenta',
    'debit'                 => 'Debe',
    'credit'                => 'Crédito',
    'total_type'            => 'Total :type',
    'totals_balance'        => 'Totales y Saldo de Cierre',
    'balance_change'        => 'Cambio de Saldo',
    'bank_cash'             => 'Banco y efectivo',
    'default_type'          => 'Por defecto :type',
    'current_year_earnings' => 'Ganancias del año actual',
    'liabilities_equities'  => 'Pasivos y acciones',
    'ledgers'               => 'Libro mayor|Libros mayores',
    'bank_accounts'         => 'Cuenta|Cuentas',
    'tax_rates'             => 'Tasa de impuestos|Tasas de impuestos',
    'edit_account'          => 'Editar :type Cuenta',
    'issued'                => 'Emitido',
    'sub'                   => 'Sub',
    'parents'               => 'Padre|Padres',
    'journals'              => 'Diario|Diarios',
    'entries'               => 'Entrada|Entradas',

    'accounts'   => [
        'receivable'        => 'Cuentas por Cobrar',
        'payable'           => 'Cuentas por Pagar',
        'sales'             => 'Ventas',
        'expenses'          => 'Gastos Generales',
        'sales_discount'    => 'Descuento por Ventas',
        'purchase_discount' => 'Descuento por Compras',
        'owners_contribution' => 'Contribución de los Propietarios',
    ],

    'document'   => [
        'detail'               => 'Una cuenta de :class se utiliza para llevar una contabilidad adecuada de su :type y para mantener la exactitud de sus informes.',
    ],

    'empty'     => [
        'manual_journal'       => 'Un asiento de diario es el acto de mantener o hacer registros de cualquier transacción. El asiento puede constar de varios registros, cada uno de los cuales es un débito o un crédito.'
    ],

];
