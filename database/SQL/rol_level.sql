ALTER TABLE akaunting.pqf_roles ADD level INT(10) NOT NULL DEFAULT '1' AFTER name;


INSERT INTO `akaunting`.`pqf_permissions` (`name`, `display_name`, `description`) VALUES 
('create-modify-display-users-with-role-levels-greater-yours', 'Create, Modify, Display, Users With Role Levels Greater Yours', 'Create, Modify, Display, Users With Role Levels Greater Yours'),
('create-modify-display-users-with-equal-role', 'Create Modify Display Users With Equal Role', 'Create Modify Display Users With Equal Role'),
('create-modify-display-users-of-another-company', 'Create Modify Display Users Of Another Company', 'Create Modify Display Users Of Another Company');


