import axios from "axios"

const awaitAndThrow = (promise, exception) => promise.then(() => {
  throw exception
})
const SECOND = 1000;
const MINUTE = 60 * SECOND;
const ERROR_MESSAGES = {
  "Failed to fetch": "Ha ocurrido un error enviando la anulacion al terminal",
};
// const HOUR = 60 * MINUTE;

async function getCurrentTerminalIp(){

  const config = await axios.get((globalThis.url + '/pos/config')).then(({data}) => data);
  if(!config) throw Error('Ho hay terminales configurados')

  return config?.currentTerminal?.ip
}
function validateReponse(response){
  return (response.data.data.transaction)

}
async function sendVoidToTerminal(referencia){
  const terminalIp = await getCurrentTerminalIp()
  const operacion = "ANULACION";

  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({ referencia, operacion });

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
  };

  const _fetch = fetch(`https://${terminalIp}:8085/compra`, requestOptions)
    .then(response => response.json())

  return (await _fetch).id_trans;
}

async function startLoopToGetTransaction(transId) {
  while (true) {

    const transactionResponse = await getTransaction(transId)


    if (!hasTransaction(transactionResponse)) {
      await wait(3 * SECOND)
      continue

    };
    if (!isApprovedTransaccion(transactionResponse))
      throw new Error('Transaccion no Aprobada compruebe los datos de la operacion y vuelva intentarlo')

    return transactionResponse.data.data.transactions;
  }
}

function isApprovedTransaccion(transResponse) {
  return transResponse.data.data.transactions.mensaje != "NO APROBADO"
}

function hasTransaction(transactionResponse) {
  return transactionResponse.status == 200
    && transactionResponse.data.data
    && transactionResponse.data.data.transactions;
}

function getTransaction(transId) {
  return axios.get(globalThis.url + '/apollo-payment/void/' + transId)
}
const wait = (ms) => new Promise(res => setTimeout(res, ms))

export default async function startVoidTransactionProcess(transaction){
  debugger
  const reference = await sendVoidToTerminal(transaction.reference)
  await wait(20 * SECOND);

  
  const voidTransaction = await Promise.race([ 
    awaitAndThrow(wait(2 * MINUTE), new Error('Tiempo limite excedido')),
    startLoopToGetTransaction(reference) 
  ])
  
  return voidTransaction;
}

export function errorMessage(){

}
