/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("../../bootstrap");

import Vue from "vue";

import DashboardPlugin from "../../plugins/dashboard-plugin";

import Global from "../../mixins/global";

import Form from "../../plugins/form";
import BulkAction from "../../plugins/bulk-action";

// plugin setup
Vue.use(DashboardPlugin);

const app = new Vue({
  el: "#main-body",

  mixins: [Global],

  data: function() {
    console.log("🚀 ~ file: customers.js:40 ~ onCanLogin ~ event:", Global);
    return {
      form: new Form("customer"),
      bulk_action: new BulkAction("customers"),
      can_login: false,
    };
  },

  mounted() {
    this.form.create_user = false;
  },

  methods: {
    onSubmit(event) {
      debugger;
      console.log("🚀 ~ file: customers.js:41 ~ onSubmit ~ event:", event);

      // this.form = event;

      // this.loading = true;

      // let data = this.form.data();

      // FormData.prototype.appendRecursive = function(data, wrapper = null) {
      //   for (var name in data) {
      //     if (wrapper) {
      //       if (
      //         (typeof data[name] == "object" ||
      //           data[name].constructor === Array) &&
      //         data[name] instanceof File != true &&
      //           data[name] instanceof Blob != true
      //       ) {
      //         this.appendRecursive(data[name], wrapper + "[" + name + "]");
      //       } else {
      //         this.append(wrapper + "[" + name + "]", data[name]);
      //       }
      //     } else {
      //       if (
      //         (typeof data[name] == "object" ||
      //           data[name].constructor === Array) &&
      //         data[name] instanceof File != true &&
      //           data[name] instanceof Blob != true
      //       ) {
      //         this.appendRecursive(data[name], name);
      //       } else {
      //         this.append(name, data[name]);
      //       }
      //     }
      //   }
      // };

      // let form_data = new FormData();
      // form_data.appendRecursive(data);

      // window
      //   .axios({
      //     method: this.form.method,
      //     url: this.form.action,
      //     data: form_data,
      //     headers: {
      //       "X-CSRF-TOKEN": window.Laravel.csrfToken,
      //       "X-Requested-With": "XMLHttpRequest",
      //       "Content-Type": "multipart/form-data",
      //     },
      //   })
      //   .then((response) => {
      //     this.form.loading = false;

      //     if (response.data.success) {
      //       let data = this.form.data();

      //       this.company.name = data.name;
      //       this.company.email = data.email;
      //       this.company.tax_number = data.tax_number;
      //       this.company.phone = data.phone;
      //       this.company.address = data.address;

      //       this.company_form.show = false;

      //       this.company_form.html = "";
      //       this.company_html = null;

      //       this.$emit("changed", data);

      //       let documentClasses = document.body.classList;

      //       documentClasses.remove("modal-open");
      //     }
      //   })
      //   .catch((error) => {
      //     this.form.loading = false;
      //     console.log(error);
      //     this.company_html = error.message;
      //   });
    },
  },
});
