<div class="row">
    <div class="col-100">
        <div class="text">
            <h3>
                {{ $textDocumentTitle ?? '' }}
            </h3>

            @if ($textDocumentSubheading ?? '')
                <h5>
                    {{ $textDocumentSubheading ?? '' }}
                </h5>
            @endif
        </div>
    </div>
</div>

<div class="col-100">
    <div class="text d-flex col-100 text-center justify-content-center">

        <strong>{{ trans('invoices.template.invoice') }}</strong>
        <div class="ml-1">
            @stack('order_number_input_start')
            @if (!($hideOrderNumber ?? ''))
                @if ($document->order_number)
                    <strong>{{ trans($textOrderNumber) }}</strong>
                    <span class="float-right text-red">{{ $document->order_number }}</span><br><br>
                @endif
            @endif
            @stack('order_number_input_end')

            @stack('invoice_number_input_start')
            @if (!($hideDocumentNumber ?? ''))
                <strong>{{ trans($textDocumentNumber ?? '') }}</strong>
                <span class="float-right text-red">{{ $document->document_number }}</span><br><br>
            @endif
            @stack('invoice_number_input_end')
        </div>
    </div>
</div>

<fieldset class="row fragment">
    <legend class="text">{{ trans('invoices.template.client') }}</legend>

    <div class="col-70">
        <div class="text br-1">
            @stack('name_input_start')
            @if (!($hideContactName ?? ''))
                <div class="d-flex col-100 justify-content-between">
                    <p class="mt-0">{{ trans('invoices.template.descriptions') }}: </p>
                    <strong class="pr-2">{{ $document->contact_name }}</strong>
                </div>
            @endif
            @stack('name_input_end')

            @stack('tax_number_input_start')
            @if (!($hideContactTaxNumber ?? ''))
                @if ($document->contact_tax_number)
                    {{ trans('general.tax_number') }}: <strong>{{ $document->contact_tax_number }}</strong>
                    <br><br>
                @endif
            @endif
            @stack('tax_number_input_end')

            @stack('phone_input_start')
            @if (!($hideContactPhone ?? ''))
                @if ($document->contact_phone)
                    {{ trans('invoices.template.contact_phone') }}:
                    <strong>{{ $document->contact_phone }}</strong>
                    <br><br>
                @endif
            @endif
            @stack('phone_input_end')


            @stack('address_input_start')

            <div class="d-flex col-100 justify-content-between">
                <p class="mt-0 r-2 col-100">
                    {{ trans('invoices.template.tax_residence') }}:
                </p>
                <strong>
                    @if (!($hideContactAddress ?? ''))
                        {!! nl2br($document->contact_address) !!}
                        <br>
                        {!! $document->contact_location !!}
                        <br><br>
                    @endif
                </strong>
            </div>

            @stack('address_input_end')

        </div>
    </div>

    <div class="col-30">
        <div class="text company pl-2 pr-2">
            @stack('issued_at_input_start')
            @if (!($hideIssuedAt ?? ''))
                <strong>{{ trans($textIssuedAt ?? 'general.date') }}:</strong>
                <span class="float-right">@date($document->issued_at)</span><br><br>
            @endif
            @stack('issued_at_input_end')
        </div>
    </div>
</fieldset>

@if (!($hideItems ?? ''))
    <div class="row">
        <div class="col-100">
            <div class="text">
                <table class="text-sm border" style="border-radius: 5px !important;">
                    <thead
                        style="background-color:{{ $backgroundColor ?? '#fff' }} !important; -webkit-print-color-adjust: exact;">
                        <tr>
                            <th style="width: 5%" class="item border-bottom p-none text-left ">
                                {{ trans('invoices.template.quantity') }}
                            </th>
                            <th style="width: 60%;" class="item border-left border-bottom p-none text-left">
                                {{ trans('invoices.template.description') }}
                            </th>
                            <th class="item border-left border-bottom p-none text-left">
                                {{ trans('invoices.template.unit_price') }}
                            </th>
                            <th class="item border-left border-bottom p-none text-left">
                                {{ trans('invoices.total_price') }}
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        @if ($document->items->count())
                            @foreach ($document->items as $item)
                                <x-documents.template.line-item-light type="{{ $type }}" :item="$item"
                                    :document="$document" hide-items="{{ $hideItems ?? '' }}"
                                    hide-name="{{ $hideName ?? '' }}" hide-discount="{{ $hideDiscount ?? '' }}"
                                    hide-description="{{ $hideDescription ?? '' }}"
                                    hide-quantity="{{ $hideQuantity ?? '' }}" hide-price="{{ $hidePrice ?? '' }}"
                                    hide-amount="{{ $hideAmount ?? '' }}" />
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center empty-items">
                                    {{ trans('documents.empty_items') }}
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                <strong>{{ trans('invoices.template.note') }}: </strong>{{ $document->notes }}
            </div>
        </div>
    </div>
@endif

<div class="row d-block">
    <div class="col-58">
        <div class="text company">
            @stack('notes_input_start')
            @if ($hideNote ?? '')
                @if ($document->notes)
                    <strong>{{ trans_choice('general.notes', 2) }}</strong><br><br>
                    {!! nl2br($document->notes) !!}
                @endif
            @endif
            @stack('notes_input_end')
        </div>
    </div>
    <div class="col-50 float-left text-right border d-flex"
        style="text-align: center;padding: 27px 15px;border-radius: 5px !important;justify-content: center;align-items: center;">
        <div class="text company pr-2" style="width: 100%; border-top: 1px dashed;">
            <strong>{{ trans('invoices.template.signature') }}</strong>
        </div>
    </div>

    <div class="col-50 float-right text-right border"
        style="text-align: center; padding: 6px 15px; border-radius: 5px !important;">
        <div class="text company pr-2">

            @foreach ($document->totals_sorted as $total)
                @if ($total->code != 'total')
                    @stack($total->code . '_total_tr_start')
                    <div class="d-flex justify-content-between"  style="width: 100%; border-bottom: 1px dashed;">
                        <strong>{{ trans($total->title) }}:</strong>
                        <span>@money($total->amount, $document->currency_code, true)</span>
                    </div>
                    @stack($total->code . '_total_tr_end')
                @else
                    @if ($document->paid)
                        @stack('paid_total_tr_start')
                        <div class="d-flex justify-content-between" style="width: 100%;">
                            <strong>{{ trans('invoices.paid') }}:</strong>
                            <span>- @money($document->paid, $document->currency_code, true)</span>
                        </div>
                        @stack('paid_total_tr_end')
                    @endif
                    @stack('grand_total_tr_start')
                    <div class="d-flex justify-content-between" style="width: 100%; border-top: 1px dashed;">
                        <strong>{{ trans($total->name) }}:</strong>
                        <span>@money($document->amount_due, $document->currency_code, true)</span>
                    </div>
                    @stack('grand_total_tr_end')
                @endif
            @endforeach
        </div>
    </div>
</div>

@if (!($hideFooter ?? ''))
    @if ($document->footer)
        <div class="row mt-7">
            <div class="col-100 py-2"
                style="background-color:{{ $backgroundColor ?? '#fff' }} !important; -webkit-print-color-adjust: exact;">
                <div class="text pl-2">
                    <strong class="text-white">{!! nl2br($document->footer) !!}</strong>
                </div>
            </div>
        </div>
    @endif
@endif
