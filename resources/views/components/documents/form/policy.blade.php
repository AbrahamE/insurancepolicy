{!! Form::open([
    'route' => isset($params) ? [$routeStore, $params] : $routeStore,
    'id' => $formId,
    // 'methods' => 'POST'
    // '@submit.prevent' => $formSubmit,
    '@keydown' => 'form.errors.clear($event.target.name)',
    'files' => true,
    'role' => 'form',
    'class' => 'form-loading-button',
    'novalidate' => true,
]) !!}
<div x-data="form" class='row'>
    <div class="col-12 col-xl-12 mx-auto">
        <div class="card border-top border-0 border-4 border-white">
            <div class="card-body">
                <div class="border p-4 rounded">
                    <div class="card-title d-flex align-items-center">
                        <div><i class="bx bxs-user me-1 font-22 text-white"></i>
                        </div>
                        <h5 class="mb-0 text-white">Nuevo cliente</h5>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="name" class="col-form-label">
                                    Nombre</label>
                                {!! Form::text('name', $document['name'] ?? '', [
                                    'id' => 'name',
                                    'class' => 'form-control',
                                    '',
                                    'placeholder' => 'nombre',
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="last_name" class="col-form-label">
                                    Apellido</label>
                                {!! Form::text('last_name', $document['last_name'] ?? '', [
                                    'id' => 'last_name',
                                    'class' => 'form-control',
                                    '',
                                    'placeholder' => 'Apellido',
                                ]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="fdn" class="col-form-label">
                                    Fecha de nacimiento</label>
                                {!! Form::text('fdn', $document['fdn'] ?? '', [
                                    'id' => 'fdn',
                                    'class' => 'form-control',
                                    '',
                                    'placeholder' => 'Fecha de nacimiento',
                                ]) !!}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="phone" class="col-form-label">Telefono</label>
                                {!! Form::text('phone', $document['phone'] ?? '', [
                                    'id' => 'phone',
                                    'class' => 'form-control',
                                    '',
                                    'placeholder' => 'Telefono',
                                ]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="email" class="col-form-label">Correo</label>
                                {{ Form::emailDefault(
                                    'email',
                                    '',
                                    '',
                                    [
                                        'placeholder' => trans('general.email'),
                                    ],
                                    $document['email'] ?? '',
                                    'has-feedback',
                                    'input-group-alternative',
                                ) }}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="gender" class="col-form-label">
                                    Genero</label>
                                {{ Form::select(
                                    'gender',
                                    [
                                        '' => '',
                                        'masculino' => 'masculino',
                                        'femenino' => 'femenino',
                                    ],
                                    $document['gender'] ?? '',
                                    ['class' => 'select-gender  form-select form-select-lg ', 'aria-label' => '.form-select-lg example'],
                                ) }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="applicant" class="col-form-label">
                                    Aplicante</label>
                                {{ Form::select('applicant', ['' => '', '1' => 'si', '0' => 'no'], $document['applicant'] ?? '', [
                                    'class' => 'select-applicant form-select form-select-lg',
                                    'aria-label' => '.form-select-lg example',
                                ]) }}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="legal_status" class="col-form-label">
                                    Estatus legal
                                </label>
                                {{ Form::select(
                                    'you_do_taxes',
                                    [
                                        '' => '',
                                        '1' => 'Ciudadano Naciona',
                                        '2' => 'Naturalizado',
                                        '3' => 'Residente',
                                        '4' => 'Permisos de Trabajo',
                                        '5' => 'En Progreso',
                                        '6' => 'Sin Status',
                                    ],
                                    $document['you_do_taxes'] ?? '',
                                    [
                                        'class' => 'select-you-do-taxes  form-select form-select-lg ',
                                        'x-model' => 'you_do_taxes',
                                        'aria-label' => '.form-select-lg example',
                                    ],
                                ) }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="dependence" class="col-form-label">
                                    ¿Cuanto depende?
                                </label>
                                {{ Form::select(
                                    'dependence',
                                    [
                                        '' => '',
                                        '0' => 'No dependen',
                                        '1' => '1 depende',
                                        '2' => '2 depende',
                                        '3' => '3 depende',
                                        '4' => '4 depende',
                                        '5' => '5 depende',
                                        '6' => '6 depende',
                                    ],
                                    $document['dependence'] ?? '',
                                    [
                                        'aria-label' => '.form-select-lg example',
                                        'class' => 'select-applicant form-select form-select-lg ',
                                        'x-model' => 'dependence',
                                    ],
                                ) }}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="you_do_taxes" class="col-form-label">
                                    ¿Como realizas los impuestos?
                                </label>
                                {{ Form::select(
                                    'legal_status',
                                    [
                                        '' => '',
                                        '1' => 'Solter@',
                                        '2' => 'Casad@',
                                    ],
                                    $document['legal_status'] ?? '',
                                    [
                                        'class' => 'select-gender form-select form-select-lg ',
                                        'x-model' => 'legal_status',
                                        'aria-label' => '.form-select-lg example',
                                    ],
                                ) }}
                            </div>
                        </div>
                    </div>

                    <div x-show="you_do_taxes === '1'">
                        <x-documents.partials.TypeStatusLegal.national-citizen />
                    </div>
                    <div x-show="you_do_taxes === '2'">
                        <x-documents.partials.TypeStatusLegal.naturalized />
                    </div>
                    <div x-show="you_do_taxes === '3'">
                        <x-documents.partials.TypeStatusLegal.resident />
                    </div>
                    <div x-show="you_do_taxes === '4'">
                        <x-documents.partials.TypeStatusLegal.work-permits />
                    </div>
                    <div x-show="you_do_taxes === '5'">
                        <x-documents.partials.TypeStatusLegal.in-progress />
                    </div>
                    <div x-show="you_do_taxes === '6'">
                        <x-documents.partials.TypeStatusLegal.no-status />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true" x-show="showModal" x-on:click="showModal = false">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Estatus legal</h5>
                    <button x-on:click="showModal = false" type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div x-show="you_do_taxes === '1'">
                        <div class="row">
                            <div class="col-6">
                                <x-documents.partials.TypeStatusLegal.national-citizen />
                            </div>
                            <div class="col-6">
                                <img src={{ asset('public/img/SSN.png') }} class="w-100">
                            </div>
                        </div>
                    </div>
                    <div x-show="you_do_taxes === '2'">
                        <div class="row">
                            <div class="col-6">
                                <x-documents.partials.TypeStatusLegal.naturalized />
                            </div>
                            <div class="col-6">
                                <img src={{ asset('public/img/naturalized.png') }} class="w-100">
                            </div>
                        </div>
                    </div>
                    <div x-show="you_do_taxes === '3'">
                        <div class="row">
                            <div class="col-6">
                                <x-documents.partials.TypeStatusLegal.resident />
                            </div>
                            <div class="col-6">
                                <img src={{ asset('public/img/SSN.png') }} class="w-100">
                                <img src={{ asset('public/img/resident.png') }} class="w-100">
                            </div>
                        </div>
                    </div>
                    <div x-show="you_do_taxes === '4'">
                        <div class="row">
                            <div class="col-6">
                                <x-documents.partials.TypeStatusLegal.work-permits />
                            </div>
                            <div class="col-6">
                                <img src={{ asset('public/img/workPermits.png') }} class="w-100">
                            </div>
                        </div>
                    </div>
                    <div x-show="you_do_taxes === '5'">
                        <x-documents.partials.TypeStatusLegal.in-progress />
                    </div>
                    <div x-show="you_do_taxes === '6'">
                        <x-documents.partials.TypeStatusLegal.no-status />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" x-on:click="showModal = false" class="btn btn-light"
                        data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    @if (isset($document))
        @if ($document['spouse'])
            <div x-show="legal_status === '2'" class="col-12 col-xl-12 mx-auto">
                <x-documents.partials.spouse-data :spouse="$document['spouse']" />
            </div>
        @endif
    @else
        <div x-show="legal_status === '2'" class="col-12 col-xl-12 mx-auto">
            <x-documents.partials.spouse-data />
        </div>
    @endif

    <div x-show="dependence !== '' && dependence !== '0'" class="col-12 col-xl-12 mx-auto">
        <div class="card border-top border-0 border-4 border-white">
            <div class="card-body">
                <div class="border p-4 rounded">
                    <div class="card-title d-flex align-items-center">
                        <div><i class="bx bxs-user me-1 font-22 text-white"></i>
                        </div>
                        <h5 class="mb-0 text-white">Dependientes</h5>
                    </div>
                    <hr />
                    @if (isset($document))
                        @if ($document['dependents'])
                            @foreach ($document['dependents'] as $dependent)
                                <x-documents.partials.dependents :dependent="$dependent" />
                            @endforeach
                        @endif
                    @else
                        <template x-for="dependenci in Dependenci">
                            <div>
                                <h6 class="mt-3 text-white">Dependiente
                                    <span x-text='dependenci'></span>
                                </h6>
                                <x-documents.partials.dependents />
                            </div>
                        </template>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div x-show="legal_status === '1' || legal_status === '2'" class="col-12 col-xl-12 mx-auto">
        <div class="card border-top border-0 border-4 border-white">
            <div class="card-body">
                <div class="border p-4 rounded">
                    <div class="card-title d-flex align-items-center">
                        <div><i class="bx bxs-user me-1 font-22 text-white"></i>
                        </div>
                        <h5 class="mb-0 text-white">Ingresos</h5>
                    </div>
                    <hr />
                    @if (isset($document))
                        @if ($document['income'])
                            @foreach ($document['income'] as $income)
                                <x-documents.partials.shared-revenue :income="$income" />
                            @endforeach
                        @endif
                    @else
                        <template x-for="income in Income">
                            <x-documents.partials.shared-revenue />
                        </template>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-xl-12 mx-auto">
        @if (isset($document))
            <x-documents.partials.policy :company="$document['company']" :category="$document['category_policy']" :identity="$document['identity']" :plan="$document['plan']"
                :type="$document['type']" :price="$document['price']" />
        @else
            <x-documents.partials.policy />
        @endif
    </div>

    <div class="col-12 col-xl-12 mx-auto">
        @if (isset($document))
            <x-documents.partials.address :address="$document['address']" :city="$document['city']" :zipcode="$document['zip_code']" :departament="$document['departament']"
                :state="$document['state']" />
        @else
            <x-documents.partials.address />
        @endif
    </div>

    <div class="col-12">
        <div class="card border-top border-0 border-4 border-white">
            <div class="card-body">
                <div class="border p-4 rounded">
                    <div class="card-title d-flex align-items-center">
                        <div><i class="bx bxs-user me-1 font-22 text-white"></i>
                        </div>
                        <h5 class="mb-0 text-white">Informacion Bank</h5>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-12 mb-3">
                            <label for="type_bank" class="col-form-label">
                                Tipo
                            </label>
                            {{ Form::select('type_bank', ['1' => 'Bancario', '2' => 'Credito / Debito', '3' => 'Ambos (Credito / Debito)'], $document['type_bank'] ?? '', ['class' => 'select-type-bank form-select form-select-lg ', 'x-model' => 'type_bank', 'aria-label' => '.form-select-lg example']) }}
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        @if (isset($document))
                            @if ($document['bank'])
                                <div x-show="type_bank === '1'">
                                    <x-documents.partials.BankInformation.banking :bank="$document['bank']['bank']" :route="$document['bank']['route_bank']"
                                        :account="$document['bank']['account_number_bank']" />
                                </div>
                            @endif

                            @if ($document['credit_bank'])
                                <div x-show="type_bank === '2'">
                                    <x-documents.partials.BankInformation.credit-or-debit :card="$document['credit_bank']['card_number_bank']"
                                        :namebank="$document['credit_bank']['name_bank']" :vec="$document['credit_bank']['vec_bank']" :cvc="$document['credit_bank']['cvc_bank']" />
                                </div>
                            @endif

                            @if ($document['bank'] && $document['credit_bank'])
                                <div x-show="type_bank === '3'">
                                    <x-documents.partials.BankInformation.banking :bank="$document['bank']['bank']" :route="$document['bank']['route_bank']"
                                        :account="$document['bank']['account_number_bank']" />
                                    <x-documents.partials.BankInformation.credit-or-debit :card="$document['credit_bank']['card_number_bank']"
                                        :namebank="$document['credit_bank']['name_bank']" :vec="$document['credit_bank']['vec_bank']" :cvc="$document['credit_bank']['cvc_bank']" />
                                </div>
                            @endif
                        @else
                            <div x-show="type_bank === '1'">
                                <x-documents.partials.BankInformation.banking />
                            </div>
                            <div x-show="type_bank === '2'">
                                <x-documents.partials.BankInformation.credit-or-debit />
                            </div>
                            <div x-show="type_bank === '3'">
                                <x-documents.partials.BankInformation.banking />
                                <x-documents.partials.BankInformation.credit-or-debit />
                            </div>
                        @endif

                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-12">
                            <div class="d-grid">
                                {{-- {!! Form::button('<div class="aka-loader"></div> <span> Enviar </span>', [
                                    // ':disabled' => 'form.loading',
                                    'type' => 'submit',
                                    'class' => 'btn btn-light',
                                    'data-loading-text' => 'enviar',
                                ]) !!} --}}
                                <button type='submit' class="btn btn-light">
                                    <div class="aka-loader"></div> <span> Enviar </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}

@push('scripts_start')
    <script>
        // $('.form-select').select2();
        document.addEventListener('alpine:init', () => {
            Alpine.data('form', () => ({
                legal_status: "{{ isset($document) ? $document['legal_status'] : '0' }}" ?? '0',
                dependence: "{{ isset($document) ? $document['dependence'] : '' }}" ?? '',
                incomes: 1,
                type_bank: "{{ isset($document) ? $document['type_bank'] : '1' }}" ?? '1',
                showModal: false,
                you_do_taxes: "{{ isset($document) ? $document['you_do_taxes'] : '0' }}" ?? '0',
                ssn: "{{ isset($document) ? $document['ssn'] : '' }}" ?? '',
                number_doc: "{{ isset($document) ? $document['number_doc'] : '' }}" ?? '',
                number_register: "{{ isset($document) ? $document['number_register'] : '' }}" ?? '',
                number_card: "{{ isset($document) ? $document['number_card'] : '' }}" ?? '',
                category: "{{ isset($document) ? $document['category'] : '' }}" ?? '',
                expire_card: "{{ isset($document) ? $document['expire_card'] : '' }}" ?? '',
                note: "{{ isset($document) ? $document['note'] : '' }}" ?? '',
                get Income() {
                    return parseInt(this.legal_status);
                },
                get Dependenci() {
                    return parseInt(this.dependence);
                }
            }))
        })

        flatpickr("#fdn", {
            enableTime: false,
            dateFormat: "m/d/Y",
        });

        $('.select-you-do-taxes').change(function(event) {
            $('#exampleModalCenter').modal('toggle')
        });

        $('#{{ $formId }}').submit(function(event) {
            event.preventDefault(); // Evita que la página se recargue al enviar el formulario

            var headers = {
                headers: {
                    "Content-Type": "multipart/form-data",
                    "X-CSRF-TOKEN": globalThis.window.Laravel.csrfToken,
                },
            };

            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize(),
                success: function(response) {
                    window.location.href = response.redirect;
                }
            });
        });
    </script>
@endpush
