@php
$document_items = 'false';

if ($items) {
    $document_items = json_encode($items);
} elseif (old('items')) {
    $document_items = json_encode(old('items'));
}

@endphp

<script type="text/javascript">
    var document_items = {!! $document_items !!};
    var document_default_currency = '{{ $currency_code }}';
    var document_currencies = {!! $currencies !!};
    var document_taxes = {!! $taxes !!};
    var template = "{{ setting('invoice.template') }}";
    var max_items = 10;
    var limit = template == 'light' || template == 'lightCC' ? true : false;
    var limit_message = "{{ trans('general.limit_items') }}";
</script>

<script src="{{ asset($scriptFile . '?v=' . $version) }}"></script>
