<h1>Credito / Debito</h1>
<div class="row">
    <div class="col-6">
        <div class="mb-3">
            <label for="card_number_bank" class="col-form-label">
                Numero de Targeta</label>
            {!! Form::text('credit_bank[card_number_bank]', $card ?? '', [
                'id' => 'card_number_bank',
                'class' => 'form-control',
                '',
                'placeholder' => 'Banco',
            ]) !!}
        </div>
        <div class="mb-3">
            <label for="name_bank" class="col-form-label">
                Nombre</label>
            {!! Form::text('credit_bank[name_bank]', $namebank ?? '', [
                'id' => 'name_bank',
                'class' => 'form-control',
                '',
                'placeholder' => 'name_bank',
            ]) !!}
        </div>
        <div class="mb-3">
            <label for="inputEnterYourName" class="col-form-label">
                F.VEC</label>
            {!! Form::text('credit_bank[vec_bank]', $vec ?? '', [
                'id' => 'vec_bank',
                'class' => 'form-control',
                '',
                'placeholder' => 'vec_bank',
            ]) !!}
        </div>
        <div class="mb-3">
            <label for="cvc_bank" class="col-form-label">
                CVC</label>
            {!! Form::text('credit_bank[cvc_bank]', $cvc ?? '', [
                'id' => 'cvc_bank',
                'class' => 'form-control',
                '',
                'placeholder' => 'cvc_bank',
            ]) !!}
        </div>
    </div>
    
    <div class="col-6">
        <img class="w-100" src={{ asset('public/img/credit.png') }} alt="" srcset="">
        <div >

            {{-- <div class="card radius-10">
                <div class="card-body">
                    
                </div> 
            </div> --}}
        </div>
    </div>
</div>
