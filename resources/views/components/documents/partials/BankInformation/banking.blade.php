<h1>Bancario</h1>

<div class="col-12">
    <div class="mb-3">
        <label for="bank" class="col-form-label">
            Banco</label>
        {!! Form::text('bank[bank]', $bank ?? '', [
            'id' => 'bank',
            'class' => 'form-control',
            '',
            'placeholder' => 'Banco',
        ]) !!}
    </div>

    <div class="mb-3">
        <label for="route" class="col-form-label">
            ruta</label>
        {!! Form::text('bank[route_bank]', $route ?? '', [
            'id' => 'route_bank',
            'class' => 'form-control',
            '',
            'placeholder' => 'ruta',
        ]) !!}
    </div>
    
    <div class="mb-3">
        <label for="account_number_bank" class="col-form-label">
            Numero de cuenta</label>
        {!! Form::text('bank[account_number_bank]', $account ?? '', [
            'id' => 'account_number_bank',
            'class' => 'form-control',
            '',
            'placeholder' => 'Numero de cuenta',
        ]) !!}
    </div>
</div>
