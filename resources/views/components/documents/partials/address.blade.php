<div class="card border-top border-0 border-4 border-white">
    <div class="card-body">
        <div class="border p-4 rounded">
            <div class="card-title d-flex align-items-center">
                <div><i class="bx bxs-user me-1 font-22 text-white"></i>
                </div>
                <h5 class="mb-0 text-white">Direccion</h5>
            </div>
            <hr />
            <div class="row">
                <div class="col-6">
                    <div class="mb-3">
                        <label for="address" class="col-form-label">
                            Direccion</label>
                        {!! Form::text('address', $address ?? '', [
                            'id' => 'address',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Direccion',
                        ]) !!}
                    </div>
                    <div class="mb-3">
                        <label for="city" class=" col-form-label">
                            Ciudad
                        </label>
                        {!! Form::text('city', $city ?? '', [
                            'id' => 'city',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Ciudad',
                        ]) !!}
                    </div>
                    <div class="mb-3">
                        <label for="zip_code" class="col-form-label">Zip Code</label>
                        {!! Form::text('zip_code', $zipcode ?? '', [
                            'id' => 'zip_code',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'zip code',
                        ]) !!}

                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3">
                        <label for="departament" class="col-form-label">
                            Dpto./Oficina/otro</label>
                        {!! Form::text('departament', $departament ?? '', [
                            'id' => 'departament',
                            'class' => 'form-control',
                            '',
                            'placeholder' => ' Dpto./Oficina/otro',
                        ]) !!}
                    </div>

                    <div class="mb-3">
                        <label for="inputPhoneNo2" class="col-form-label">Estado</label>
                        {!! Form::text('state', $state ?? '', [
                            'id' => 'state',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Estado',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
