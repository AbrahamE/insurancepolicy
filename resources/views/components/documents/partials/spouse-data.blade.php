<div class="card border-top border-0 border-4 border-white">
    <div class="card-body">
        <div class="border p-4 rounded">
            <div class="card-title d-flex align-items-center">
                <div><i class="bx bxs-user me-1 font-22 text-white"></i>
                </div>
                <h5 class="mb-0 text-white">Datos de Conyuge</h5>
            </div>
            <hr />
            <div class="row">
                <div class="col-6">
                    <div class="mb-3">
                        <label for="spouse_name" class="col-form-label">
                            Nombre
                        </label>
                        {!! Form::text('spouse[name]', $spouse['name'] ?? '', [
                            'id' => 'spouse_name',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Nombre',
                        ]) !!}
                    </div>

                    <div class="mb-3">
                        <label for="FDN" class=" col-form-label">
                            Fecha de nacimiento
                        </label>
                        {!! Form::text('spouse[fdn]', $spouse['fdn'] ?? '', [
                            'id' => 'fdn',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Fecha de nacimiento',
                        ]) !!}
                    </div>

                    <div class="mb-3">
                        <label for="spouse_email" class="col-form-label">Correo</label>
                        {{ Form::emailGroup('spouse[email]', '', 'envelope', ['placeholder' => trans('general.email')], $spouse['email'] ?? '', 'has-feedback', 'input-group-alternative') }}
                    </div>
                    <div class="mb-3">
                        <label for="spouse_applicant" class="col-form-label">
                            Aplicante</label>
                        {{ Form::select('spouse[applicant]', [
                            "" => "",
                            '1' => 'Si', '0' => 'No'], $spouse['applicant'] ?? '', ['class' => 'select-applicant form-select form-select-lg mb-3', 'aria-label' => '.form-select-lg example']) }}
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3">
                        <label for="spouse_last_name" class="col-form-label">
                            Apellido</label>
                        {!! Form::text('spouse[last_name]', $spouse['last_name'] ?? '', [
                            'id' => 'spouse_last_name',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Cantidad',
                        ]) !!}
                    </div>
                    <div class="mb-3">
                        <label for="spouse_phone" class=" col-form-label">Telefono</label>
                        {!! Form::text('spouse[phone]', $spouse['phone'] ?? '', [
                            'id' => 'spouse_phone',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Telefono',
                        ]) !!}
                    </div>
                    <div class="mb-3">
                        <label for="spouse_gender" class="col-form-label">
                            Genero</label>
                        {{ Form::select('spouse[gender]', [
                            "" => "",
                            'masculino' => 'masculino', 'femenino' => 'femenino'], $spouse['gender'] ?? '', ['class' => 'select-spouse-gender form-select form-select-lg mb-3', 'aria-label' => '.form-select-lg example']) }}
                    </div>
                </div>
            </div>
            <div class="row border-top">
                <div class="col-6">
                    <div class="mb-3">
                        <label for="spouse_ssn" class="col-form-label">
                            SSN</label>
                        {!! Form::text('spouse[ssn]', $spouse['ssn'] ?? '', [
                            'id' => 'spouse[spouse_ssn]',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'SSN',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    flatpickr("#fdn", {
        enableTime: false,
        dateFormat: "m/d/Y",
    });
</script>
