<div class="row border-top">
    <div class="col-6">
        <div class="mb-3">
            <label for="SSN" class="col-form-label">
                SSN
            </label>
            {!! Form::text('ssn', null, [
                'id' => 'ssn',
                'class' => 'form-control',
                '',
                'placeholder' => 'ssn',
                'x-model' => 'ssn'
            ]) !!}
        </div>
    </div>
    <div class="col-6">
        <div class="mb-3">
            <label for="number_doc" class="col-form-label">
                N Registro
            </label>
            {!! Form::text('number_register', null, [
                'id' => 'number_register',
                'class' => 'form-control',
                '',
                'placeholder' => 'N Registro',
                'x-model' => 'number_register'
            ]) !!}
        </div>
    </div>
    <div class="col-6">
        <div class="mb-3">
            <label for="SSN" class="col-form-label">
                N card
            </label>
            {!! Form::text('number_card', null, [
                'id' => 'number_card',
                'class' => 'form-control',
                '',
                'placeholder' => 'N card',
                'x-model' => 'number_card'
            ]) !!}
        </div>
    </div>
    <div class="col-6">
        <div class="mb-3">
            <label for="category" class="col-form-label">
                Categoria
            </label>
            {!! Form::text('category', null, [
                'id' => 'category',
                'class' => 'form-control',
                '',
                'placeholder' => 'N Categoria',
                'x-model' => 'category'
            ]) !!}
        </div>
    </div>
    <div class="col-6">
        <div class="mb-3">
            <label for="SSN" class="col-form-label">
                Card expire
            </label>
            {!! Form::text('expire_card', null, [
                'id' => 'expire_card',
                'class' => 'form-control',
                '',
                'placeholder' => 'card expire',
                'x-model' => 'expire_card'
            ]) !!}
        </div>
    </div>
</div>
