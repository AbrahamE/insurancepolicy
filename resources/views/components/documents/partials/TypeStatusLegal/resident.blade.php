<div class="row border-top">
    <div class="col-12">
        <div class="mb-3">
            <label for="SSN" class="col-form-label">
                SSN</label>
            {!! Form::text('ssn', null, [
                'id' => 'ssn',
                'class' => 'form-control',
                '',
                'placeholder' => 'ssn',
                'x-model' => 'ssn',
            ]) !!}
        </div>
    </div>
    <div class="col-12">
        <div class="mb-3">
            <label for="number_register" class="col-form-label">
                N Registro</label>
            {!! Form::text('number_register', null, [
                'id' => 'number_register',
                'class' => 'form-control',
                '',
                'placeholder' => 'N Registro',
                'x-model' => 'number_register',
            ]) !!}
        </div>
    </div>
    <div class="col-12">
        <div class="mb-3">
            <label for="SSN" class="col-form-label">
                N card</label>
            {!! Form::text('number_card', null, [
                'id' => 'number_card',
                'class' => 'form-control',
                '',
                'placeholder' => ' N card',
                'x-model' => 'number_card',
            ]) !!}
        </div>
    </div>
</div>
