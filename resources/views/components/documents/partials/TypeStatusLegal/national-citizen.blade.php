<div class="row border-top">
    <div class="col-6">
        <div class="mb-3">
            <label for="SSN" class="col-form-label">
                SSN</label>
            {!! Form::text('ssn', null, [
                'id' => 'ssn',
                'class' => 'form-control',
                '',
                'placeholder' => 'ssn',
                'x-model' => 'ssn'
            ]) !!}
        </div>
    </div>
</div>