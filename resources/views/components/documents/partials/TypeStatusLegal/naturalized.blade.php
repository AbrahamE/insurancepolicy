<div class="row border-top">
    <div class="col-6">
        <div class="mb-3">
            <label for="SSN" class="col-form-label">
                SSN</label>
            {!! Form::text('ssn', null, [
                'id' => 'ssn',
                'class' => 'form-control',
                '',
                'placeholder' => 'ssn',
                'x-model' => 'ssn'
            ]) !!}
        </div>
    </div>
    <div class="col-6">
        <div class="mb-3">
            <label for="number_doc" class="col-form-label">
                N Doc</label>
            {!! Form::text('number_doc', null, [
                'id' => 'number_doc',
                'class' => 'form-control',
                '',
                'placeholder' => 'N Doc',
                'x-model' => 'number_doc'
            ]) !!}
        </div>
    </div>
    <div class="col-6">
        <div class="mb-3">
            <label for="SSN" class="col-form-label">
                N Registro</label>
            {!! Form::text('number_register', null, [
                'id' => 'number_register',
                'class' => 'form-control',
                '',
                'placeholder' => 'N registro',
                'x-model' => 'number_register'
            ]) !!}
        </div>
    </div>
</div>
