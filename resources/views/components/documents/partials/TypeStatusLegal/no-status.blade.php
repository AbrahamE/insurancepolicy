<div class="row border-top">
    <div class="col-12">
        <div class="mb-3">
            <label for="note" class="col-form-label">
                Nota
            </label>
            {!! Form::text('note', null, [
                'id' => 'note',
                'class' => 'form-control',
                '',
                'placeholder' => 'Nota',
                'x-model' => 'note'
            ]) !!}
        </div>
    </div>
</div>