<div class="border-bottom border-0 border-4 border-white ">
    <div class="row">
        <div class="col-6">
            <div class="mb-3">
                <label for="dependents_name" class="col-form-label">
                    Nombre</label>
                {!! Form::text('dependents[name][]', $dependent['name'] ?? '', [
                    'id' => 'dependents_name',
                    'class' => 'form-control',
                    '',
                    'placeholder' => 'Nombre',
                ]) !!}
            </div>

            <div class="mb-3">
                <label for="dependents_fdn" class=" col-form-label">
                    Fecha de nacimiento
                </label>
                {!! Form::text('dependents[fdn][]', $dependent['fdn'] ?? '', [
                    'id' => 'dependents_fdn',
                    'class' => 'form-control',
                    '',
                    'placeholder' => 'Fecha de nacimiento',
                ]) !!}
            </div>

            <div class="mb-3">
                <label for="dependents_email" class="col-form-label">Correo</label>
                {{ Form::emailDefault('dependents[email][]', '', 'envelope', ['placeholder' => trans('general.email')], $dependent['email'] ?? '', 'has-feedback', 'input-group-alternative') }}
            </div>
            <div class="mb-3">
                <label for="inputConfirmPassword2" class="col-form-label">
                    Aplicante</label>
                {{ Form::select(
                    'dependents[aplicante][]',
                    [
                        '' => '',
                        '1' => 'Si',
                        '0' => 'No',
                    ],
                    $dependent['aplicante'] ?? '',
                    ['class' => 'select-dependents-aplicante form-select form-select-lg mb-3'],
                ) }}
            </div>

            <div class="mb-3">
                <label for="inputConfirmPassword2" class="col-form-label">
                    Relacion</label>
                {{ Form::select(
                    'dependents[relation][]',
                    [
                        '' => '',
                        '1' => 'hijo',
                        '2' => 'padre',
                        '3' => 'sobrino',
                        '4' => 'hermano',
                    ],
                    $dependent['relation'] ?? '',
                    ['class' => 'select-dependents-relation form-select form-select-lg mb-3'],
                ) }}
            </div>
        </div>
        <div class="col-6">
            <div class="mb-3">
                <label for="inputEnterYourName" class="col-form-label">
                    Apellido</label>
                {!! Form::text('dependents[last_name][]', $dependent['last_name'] ?? '', [
                    'id' => 'dependents_last_name',
                    'class' => 'form-control',
                    '',
                    'placeholder' => 'Apellido',
                ]) !!}
            </div>
            <div class="mb-3">
                <label for="dependents_gender" class="col-form-label">
                    Genero</label>
                {{ Form::select(
                    'dependents[gender][]',
                    [
                        '' => '',
                        'masculino' => 'masculino',
                        'femenino' => 'femenino',
                    ],
                    $dependent['gender'] ?? '',
                    ['class' => 'select-dependents-relation form-select form-select-lg mb-3'],
                ) }}
            </div>

            <div class="mb-3">
                <label for="dependents_phone" class=" col-form-label">Telefono</label>
                {!! Form::text('dependents[phone][]', $dependent['phone'] ?? '', [
                    'id' => 'dependents_phone',
                    'class' => 'form-control',
                    '',
                    'placeholder' => 'Telefono',
                ]) !!}
            </div>

            <div class="mb-3">
                <label for="dependents_legal_status" class="col-form-label">
                    Estatus legal</label>
                {{ Form::select(
                    'dependents[legal_status][]',
                    [
                        '' => '',
                        '1' => 'Solter@',
                        '2' => 'Casad@',
                    ],
                    $dependent['legal_status'] ?? '',
                    ['class' => 'select-type-taxes form-select form-select-lg mb-3'],
                ) }}
            </div>
        </div>
    </div>
    <div class="row border-top">
        <div class="col-6">
            <div class="mb-3">
                <label for="name" class="col-form-label">
                    SSN</label>
                {!! Form::text('dependents[ssn][]', $dependent['ssn'] ?? '', [
                    'id' => 'dependents_ssn',
                    'class' => 'form-control',
                    '',
                    'placeholder' => 'SSN',
                ]) !!}
            </div>
        </div>
    </div>
</div>


<script>
    flatpickr("#dependents_fdn", {
        enableTime: false,
        dateFormat: "m/d/Y",
    });
</script>
