<div class="border-bottom border-0 border-4 border-white">
    <div class="row">
        <div class="col-6">
            <div class="mb-3">
                <label for="your_income_is" class="col-form-label">
                    Sus ingresos son
                </label>

                {{ Form::select('income[your_income_is][]', ['' => '', '0' => 'No ingresos', '1' => 'Semanal', '2' => 'Quincenal', '3' => 'Mensual', '4' => 'Otro'], $income['your_income_is'] ?? '', ['class' => 'select-your-income-is form-select form-select-lg mb-3']) }}
            </div>

            <div class="mb-3">
                <label for="1099_or_w2" class=" col-form-label">
                    ¿Usted trabaja con la 1099 o W2?
                </label>

                {{ Form::select(
                    'income[1099_or_w2][]',
                    [
                        '' => '',
                        '1' => '1099',
                        '2' => 'W2',
                        '3' => 'Ninguno',
                    ],
                    $income['1099_or_w2'] ?? '',
                    ['class' => 'select-your-income-is form-select form-select-lg mb-3', 'aria-label' => '.form-select-lg example'],
                ) }}
            </div>
        </div>
        <div class="col-6">
            <div class="mb-3">
                <label for="amount" class="col-form-label">
                    ingreso persona
                </label>
                {!! Form::text('income[amount][]', $income['amount'] ?? '', [
                    'id' => 'amount',
                    'class' => 'form-control',
                    '',
                    'placeholder' => 'Cantidad',
                ]) !!}
            </div>

            <div class="mb-3">
                <label for="total_annual" class=" col-form-label">Icome aunal</label>
                {!! Form::text('income[total_annual][]', $income['total_annual'] ?? '', [
                    'id' => 'total_annual',
                    'class' => 'form-control',
                    '',
                    'placeholder' => 'Total aunal',
                ]) !!}
            </div>
        </div>
    </div>
</div>
