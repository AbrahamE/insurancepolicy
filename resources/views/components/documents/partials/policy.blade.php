<div class="card border-top border-0 border-4 border-white">
    <div class="card-body">
        <div class="border p-4 rounded">
            <div class="card-title d-flex align-items-center">
                <div><i class="bx bxs-user me-1 font-22 text-white"></i>
                </div>
                <h5 class="mb-0 text-white">Poliza</h5>
            </div>
            <hr />
            <div class="row">
                <div class="col-6">
                    <div class="mb-3">
                        <label for="company" class=" col-form-label">
                            Compañia
                        </label>
                        {{ Form::select(
                            'company',
                            [
                                '' => '',
                                '0' => 'Florida Blue',
                                '1' => 'Ambetter',
                                '2' => 'Oscar',
                                '3' => 'Aetna CVS',
                                '4' => 'Molina',
                                '5' => 'Cigna',
                                '6' => 'Florida Health Solutions',
                                '7' => 'BCBS Texas',
                                '8' => 'Select Health',
                                '9' => 'BCBS Georgia',
                                '10' => 'Otro',
                            ],
                            $company ?? '',
                            [
                                'aria-label' => '.form-select-lg example',
                                'class' => 'select-applicant form-select form-select-lg ',
                            ],
                        ) }}

                    </div>
                    <div class="mb-3">
                        <label for="category_policy" class="col-form-label">
                            Categoria
                        </label>
                        {{ Form::select(
                            'category_policy',
                            [
                                '' => '',
                                '0' => 'bronze',
                                '1' => 'silver',
                                '2' => 'golden',
                                '3' => 'platinum',
                            ],
                            $category ?? '',
                            [
                                'aria-label' => '.form-select-lg example',
                                'class' => 'select-applicant form-select form-select-lg ',
                            ],
                        ) }}
                    </div>
                    <div class="mb-3">
                        <label for="identity" class="col-form-label">ID</label>
                        {!! Form::text('identity', $identity ?? '', [
                            'id' => 'identity',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'ID',
                        ]) !!}
                    </div>
                </div>

                <div class="col-6">
                    <div class="mb-3">
                        <label for="plan" class="col-form-label">Plan</label>
                        {!! Form::text('plan', $plan ?? '', [
                            'id' => 'plan',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Plan',
                        ]) !!}
                    </div>

                    <div class="mb-3">
                        <label for="type" class="col-form-label">Tipo</label>
                        {!! Form::text('type', $type ?? '', [
                            'id' => 'type',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Tipo',
                        ]) !!}
                    </div>

                    <div class="mb-3">
                        <label for="price" class="col-form-label">Precio</label>
                        {!! Form::text('price', $price ?? '', [
                            'id' => 'price',
                            'class' => 'form-control',
                            '',
                            'placeholder' => 'Precio',
                        ]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
