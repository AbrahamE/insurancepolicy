<form wire:click.stop class="align-content-center flex-grow-1 justify-content-center m-0 search-bar" id="navbar-search-main" autocomplete="off">
    <div class="position-relative search-bar-box">
        <input type="text" name="search" wire:model.debounce.500ms="keyword" class="form-control search-control"
            autocomplete="off" placeholder="{{ trans('general.search') }}">
        <span class="position-absolute top-50 search-show translate-middle-y"><i class='bx bx-search'></i></span>
        <span class="position-absolute top-50 search-close translate-middle-y"><i class='bx bx-x'></i></span>

        @if ($results)
            <div class="dropdown-menu dropdown-menu-xl dropdown-menu-center show" ref="menu">
                <div class="list-group list-group-flush">
                    @foreach ($results as $result)
                        <a class="list-group-item list-group-item-action" href="{{ $result->href }}">
                            <div class="row align-items-center">
                                <div class="col ml--2">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div>
                                            <div class="name">{{ $result->name }}</div>
                                        </div>
                                        <div class="text-muted">
                                            <span class="type">{{ $result->type }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</form>

@push('scripts_end')
    <script type="text/javascript">
        $(window).click(function() {
            if (Livewire.components.getComponentsByName('common.search')[0].data.results.length > 0) {
                Livewire.emit('resetKeyword');
            }
        });
    </script>
@endpush
