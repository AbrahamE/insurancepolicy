@extends('layouts.admin')

@section('title', trans('general.title.edit', ['type' => trans_choice('general.permissions', 1)]))

@section('content')
    <div class="card border-top border-0 border-4 border-white">
        <div class="card-body">
            <div class="border rounded p-4">
                {!! Form::model($permission, [
                    'id' => 'permission',
                    'method' => 'PATCH',
                    'route' => ['permissions.update', $permission->id],
                    '@submit.prevent' => 'onSubmit',
                    '@keydown' => 'form.errors.clear($event.target.name)',
                    'files' => true,
                    'role' => 'form',
                    'class' => 'form-loading-button',
                    'novalidate' => true,
                ]) !!}

                    <div class="row">
                        {{ Form::textGroup('display_name', trans('general.name'), 'font') }}

                        {{ Form::textGroup('name', trans('general.code'), 'code') }}

                        {{ Form::textareaGroup('description', trans('general.description')) }}
                    </div>
                    @can('update-auth-permissions')
                        <div class="card-footer">
                            <div class="row save-buttons">
                                {{ Form::saveButtons('permissions.index') }}
                            </div>
                        </div>
                    @endcan
                {!! Form::close() !!}
            </div>

        </div>
    @endsection

    @push('scripts_start')
        <script src="{{ asset('public/js/auth/permissions.js?v=' . version('short')) }}"></script>
    @endpush
