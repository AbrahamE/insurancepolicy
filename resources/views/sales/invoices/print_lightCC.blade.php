@extends('layouts.print')

@section('title', trans_choice('general.invoices', 1) . ': ' . $invoice->document_number)

@section('content')
    <div style="margin-left: 50px; margin-right: 50px; margin-top: 140px; padding-bottom: 90px">
        <x-documents.template.light-cc type="invoice" :document="$invoice" sheet="ORIGINAL" />
    </div>
    <div style="margin-left: 50px; margin-right: 50px; margin-top: 140px">
        <x-documents.template.light-cc type="invoice" :document="$invoice" sheet="COPIA" />
    </div>
@endsection
