@extends('layouts.admin')

@section('title', 'Nueva Polisa de seguro')

@section('content')
    <x-documents.form.policy routeStore="policys.update" :params="$document['id']" formId="documentUpdate" type="policy" formSubmit="onSubmit"
        :document="$document ?? []" />
@endsection

@push('scripts_start')
    <script src="{{ asset('js/policy/customers.js') }}"></script>
@endpush
