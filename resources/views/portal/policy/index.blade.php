@extends('layouts.admin')

@section('title', 'Polisas Creadas')

@section('new_button')
    <a href="{{ route('policys.new') }}" class="btn btn-light ">{{ trans('general.add_new') }}</a>
@endsection

@section('content')
    {{-- <x-documents.index.content 
    type="invoice"
    :documents="$invoices"
    hide-bulk-action
    hide-contact-name
    hide-actions
    hide-empty-page
    form-card-header-route="portal.invoices.index"
    route-button-show="portal.invoices.show"
    class-document-number="col-xs-4 col-sm-4 col-md-3 disabled-col-aka"
    class-amount="col-xs-4 col-sm-2 col-md-2 text-right"
    class-issued-at="col-sm-3 col-md-3 d-none d-sm-block"
    class-due-at="col-md-2 d-none d-md-block"
    class-status="col-xs-4 col-sm-3 col-md-2 text-center"
    search-string-model="App\Models\Portal\Sale\Invoice"
/> --}}
    {{-- 
    @php
        var_dump($policy->toArray());
    @endphp --}}

    <div class="row">
        <div class="col-12 col-xl-12 mx-auto">
            <div class="card border-top border-0 border-4 border-white">
                <div class="card-body">
                    <div class="table-responsive border p-4 rounded">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#N Polisa</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellido</th>
                                    <th scope="col">Fecha de Nasimiento</th>
                                    <th scope="col">Ssn</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($policy as $p)
                                    <tr>
                                        <td>{{ $p->id }}</td>
                                        <td>{{ $p->name }}</td>
                                        <td>{{ $p->last_name }}</td>
                                        <td>{{ $p->fdn }}</td>
                                        <td>{{ $p->ssn }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown"
                                                    aria-expanded="false">
                                                  <i class="fa fa-ellipsis-v"> </i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-end">
                                                    <a class="dropdown-item" href="javascript:;">Ver</a>
                                                    <a class="dropdown-item" href="{{ route('policys.edit', $p->id) }}">Actualizar</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item" href="javascript:;">Eliminar</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts_start')
    <script src="{{ asset('js/policy/customers.js') }}"></script>
@endpush
