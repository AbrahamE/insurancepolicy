@extends('layouts.admin')

@section('title', 'Nueva Polisa de seguro')

@section('content')
    <x-documents.form.policy routeStore="policys.create" formId="document" type="policy" formSubmit="onSubmit" />
@endsection

@push('scripts_start')
    <script src="{{ asset('js/policy/customers.js') }}"></script>
@endpush
