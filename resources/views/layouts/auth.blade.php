<html lang="{{ app()->getLocale() }}">

@include('partials.auth.head')

<body
    style="background-image: url('{{ asset('public/images/bg-themes/1.png') }}');
     background-size: 100% 100%;
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    transition: background .2s;"
    class="login-page">
    @stack('body_start')

    <div class="main-content mt-4">
        <div class="header">
            <div class="container">
                <div class="header-body text-center">
                    <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 col-md-8">
                            {{-- <img class="mb-5" src="{{ asset('public/img/logo-danube-white.png') }}" width="45%"
                                alt="Akaunting" /> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @stack('login_box_start')
        <div class="container mt-8">
            <div class="row justify-content-center mt-8">
                <div class="col-lg-5 col-md-7">

                    {{-- <div class="text-center text-white mb-4">
                        <small>@yield('message')</small>
                    </div> --}}
                    <div class="card border-top border-0 border-4 border-white">
                        <div class="card-body">
                            <div class="border p-4 rounded">
                                <div id="app">
                                    @stack('login_content_start')

                                    @yield('content')

                                    @stack('login_content_end')
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="card mb-0 login-card-bg">
                                <div class="card-body px-lg-5 py-lg-5">
                                   

                                   
                                </div>
                            </div> --}}
                </div>
            </div>
        </div>
        @stack('login_box_end')

        @yield('forgotten-password')

        {{-- <footer>
            <div class="container mt-5 mb-4">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-12">
                        <div class="copyright text-center text-white">
                            <small>
                                {{ trans('footer.powered') }}: <a href="{{ trans('footer.link') }}" target="_blank"
                                    class="text-white">{{ trans('footer.software') }}</a>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </footer> --}}

    </div>

    @stack('body_end')

    @include('partials.auth.scripts')
</body>

</html>
