<html lang="{{ app()->getLocale() }}">

@include('partials.admin.head')

@mobile

    <body
        style="background-image: url('{{ asset('public/images/bg-themes/1.png') }}');    
        background-size: 100% 100%;
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        transition: background .2s;"
        id="leftMenu" class="g-sidenav-hidden">
    @elsemobile

        <body
            style="background-image: url('{{ asset('public/images/bg-themes/1.png') }}');    
            background-size: 100% 100%;
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            transition: background .2s;"
            id="leftMenu" class="g-sidenav-show">
        @endmobile

        @stack('body_start')


        <div class="wrapper" id="panel">

            @include('partials.admin.menu')
            @include('partials.admin.navbar')
            {{-- @include('partials.components.header') --}}

            {{-- @include('partials.components.nav') --}}


            <div class="page-wrapper" id="panel">
                <div class="page-content" id="main-body">

                    @include('partials.admin.header')

                    <div class="container-fluid content-layout mt--6">

                        @include('partials.admin.content')

                        {{-- @include('partials.admin.footer') --}}

                    </div>

                </div>
            </div>

        </div>

        @stack('body_end')
        <!--start switcher-->
        <div class="switcher-wrapper">
            <div class="d-flex align-items-center justify-content-center switcher-btn"> 
                <i class='fa fa-cog fa-spin'></i>
            </div>
            <div class="switcher-body">
                <div class="d-flex align-items-center">
                    <h5 class="mb-0 text-uppercase">Theme Customizer</h5>
                    <button type="button" class="btn-close ms-auto close-switcher" aria-label="Close"></button>
                </div>
                <hr />
                <p class="mb-0">Gaussian Texture</p>
                <hr>

                <ul class="switcher">
                    <li id="theme1"></li>
                    <li id="theme2"></li>
                    <li id="theme3"></li>
                    <li id="theme4"></li>
                    <li id="theme5"></li>
                    <li id="theme6"></li>
                </ul>
                <hr>
                <p class="mb-0">Gradient Background</p>
                <hr>

                <ul class="switcher">
                    <li id="theme7"></li>
                    <li id="theme8"></li>
                    <li id="theme9"></li>
                    <li id="theme10"></li>
                    <li id="theme11"></li>
                    <li id="theme12"></li>
                    <li id="theme13"></li>
                    <li id="theme14"></li>
                    <li id="theme15"></li>
                </ul>
            </div>
        </div>
        <!--end switcher-->

        @include('partials.admin.scripts')
    </body>

</html>
