@stack('menu_start')
<div class="sidebar-wrapper">
    <div class="sidebar-header">

        <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i></div>
        <div class="dropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                  
                    <a class="d-flex align-items-center nav-link text-white" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="avatar menu-avatar background-unset">
                            <img class="border-radius-none border-0 mr-1 mr-1 image-style user-img" alt="Akaunting"
                                src="{{ asset('public/img/logo-danube-dashboard.png') }}">
                        </span>
                        <span
                            class="nav-link-text long-texts pl-2 mwpx-100">{{ Str::limit(setting('company.name'), 22) }}</span>
                        @can('read-common-companies')
                            <i class="fas fa-sort-down pl-2"></i>
                        @endcan
                    </a>
                    @can('read-common-companies')
                        <div class="dropdown-menu dropdown-menu-right menu-dropdown menu-dropdown-width">
                            @foreach ($companies as $com)
                                <a href="{{ route('companies.switch', $com->id) }}" class="dropdown-item">
                                    <i class="fas fa-building"></i>
                                    <span>{{ Str::limit($com->name, 18) }}</span>
                                </a>
                            @endforeach
                            @can('update-common-companies')
                                <div class="dropdown-divider"></div>
                                <a href="{{ route('companies.index') }}" class="dropdown-item">
                                    <i class="fas fa-cogs"></i>
                                    <span>{{ trans('general.title.manage', ['type' => trans_choice('general.companies', 2)]) }}</span>
                                </a>
                            @endcan
                        </div>
                    @endcan
                </li>
            </ul>
        </div>
        <div class="ml-auto left-menu-toggle-position overflow-hidden">
            <div class="sidenav-toggler d-none d-xl -block left-menu-toggle" data-action="sidenav-unpin"
                data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
            </div>
        </div>
    </div>
    <nav class="topbar-nav">
        <ul class="metismenu" id="menu">
            {{-- <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-home-circle'></i>
                </div>
                <div class="menu-title">Dashboard</div>
            </a> --}}
            <li>
                <a href="{{ url('/' . company_id() . '/insurance/policys/create') }}">
                    {{-- '/' . company_id() . '/insurance/policys' --}}
                    <i class="bx bx-right-arrow-alt"></i>forms
                </a>
            </li>
            <li>
                <a href="{{ url('/' . company_id() . '/insurance/policys') }}">
                    <i class="bx bx-right-arrow-alt"></i>cliente
                </a>
            </li>
        </ul>
    </nav>
    {{-- <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs" id="sidenav-main">
        <div class="scrollbar-inner">
            <div class="sidenav-header d-flex align-items-center">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <span class="avatar menu-avatar background-unset">
                                <img class="border-radius-none border-0 mr-3" alt="Akaunting"
                                    src="{{ asset('public/img/logo-danube-dashboard.png') }}">
                            </span>
                            <span class="nav-link-text long-texts pl-2 mwpx-100">{{ Str::limit(setting('company.name'), 22) }}</span>
                            @can('read-common-companies')
                                <i class="fas fa-sort-down pl-2"></i>
                            @endcan
                        </a>


                        @can('read-common-companies')
                            <div class="dropdown-menu dropdown-menu-right menu-dropdown menu-dropdown-width">
                                @foreach ($companies as $com)
                                    <a href="{{ route('companies.switch', $com->id) }}" class="dropdown-item">
                                        <i class="fas fa-building"></i>
                                        <span>{{ Str::limit($com->name, 18) }}</span>
                                    </a>
                                @endforeach
                                @can('update-common-companies')
                                    <div class="dropdown-divider"></div>
                                    <a href="{{ route('companies.index') }}" class="dropdown-item">
                                        <i class="fas fa-cogs"></i>
                                        <span>{{ trans('general.title.manage', ['type' => trans_choice('general.companies', 2)]) }}</span>
                                    </a>
                                @endcan
                            </div>
                        @endcan
                    </li>
                </ul>
                <div class="ml-auto left-menu-toggle-position overflow-hidden">
                    <div class="sidenav-toggler d-none d-xl-block left-menu-toggle" data-action="sidenav-unpin"
                        data-target="#sidenav-main">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </div>
                </div>
            </div>
            {!! menu('admin') !!}

         

        </div>
    </nav> --}}
    {{-- <div class="mobile-topbar-header">
        <div>
            <img src="{{ asset('assets/images/logo-icon.png') }}" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">Dashtreme</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
        </div>
    </div>
    <nav class="topbar-nav">
        <ul class="metismenu" id="menu">
            <li>
                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class='bx bx-home-circle'></i>
                    </div>
                    <div class="menu-title">Dashboard</div>
                </a>
                <ul>
                    <li> <a href="{{ url('index') }}"><i class="bx bx-right-arrow-alt"></i>Default</a>
                    </li>
                    <li> <a href="{{ url('dashboard-eCommerce') }}"><i class="bx bx-right-arrow-alt"></i>eCommerce</a>
                    </li>
                    <li> <a href="{{ url('dashboard-sales') }}"><i class="bx bx-right-arrow-alt"></i>Sales</a>
                    </li>
                    <li> <a href="{{ url('dashboard-analytics') }}"><i class="bx bx-right-arrow-alt"></i>Analytics</a>
                    </li>
                    <li> <a href="{{ url('dashboard-alternate') }}"><i class="bx bx-right-arrow-alt"></i>Alternate</a>
                    </li>
                    <li> <a href="{{ url('dashboard-digital-marketing') }}"><i class="bx bx-right-arrow-alt"></i>Digital
                            Marketing</a>
                    </li>
                    <li> <a href="{{ url('dashboard-human-resources') }}"><i class="bx bx-right-arrow-alt"></i>Human
                            Resources</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class="bx bx-category"></i>
                    </div>
                    <div class="menu-title">Application</div>
                </a>
                <ul>
                    <li> <a href="{{ url('app-emailbox') }}"><i class="bx bx-right-arrow-alt"></i>Email</a>
                    </li>
                    <li> <a href="{{ url('app-chat-box') }}"><i class="bx bx-right-arrow-alt"></i>Chat Box</a>
                    </li>
                    <li> <a href="{{ url('app-file-manager') }}"><i class="bx bx-right-arrow-alt"></i>File Manager</a>
                    </li>
                    <li> <a href="{{ url('app-contact-list') }}"><i class="bx bx-right-arrow-alt"></i>Contatcs</a>
                    </li>
                    <li> <a href="{{ url('app-to-do') }}"><i class="bx bx-right-arrow-alt"></i>Todo List</a>
                    </li>
                    <li> <a href="{{ url('app-invoice') }}"><i class="bx bx-right-arrow-alt"></i>Invoice</a>
                    </li>
                    <li> <a href="{{ url('app-fullcalender') }}"><i class="bx bx-right-arrow-alt"></i>Calendar</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon"><i class="bx bx-line-chart"></i>
                    </div>
                    <div class="menu-title">Charts</div>
                </a>
                <ul>
                    <li> <a href="{{ url('charts-apex-chart') }}"><i class="bx bx-right-arrow-alt"></i>Apex</a>
                    </li>
                    <li> <a href="{{ url('charts-chartjs') }}"><i class="bx bx-right-arrow-alt"></i>Chartjs</a>
                    </li>
                    <li> <a href="{{ url('charts-highcharts') }}"><i class="bx bx-right-arrow-alt"></i>Highcharts</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon"><i class='bx bx-bookmark-heart'></i>
                    </div>
                    <div class="menu-title">Components</div>
                </a>
                <ul>
                    <li> <a href="{{ url('widgets') }}"><i class="bx bx-right-arrow-alt"></i>Widgets</a>
                    </li>
                    <li> <a href="{{ url('component-alerts') }}"><i class="bx bx-right-arrow-alt"></i>Alerts</a>
                    </li>
                    <li> <a href="{{ url('component-accordions') }}"><i
                                class="bx bx-right-arrow-alt"></i>Accordions</a>
                    </li>
                    <li> <a href="{{ url('component-buttons') }}"><i class="bx bx-right-arrow-alt"></i>Buttons</a>
                    </li>
                    <li> <a href="{{ url('component-cards') }}"><i class="bx bx-right-arrow-alt"></i>Cards</a>
                    </li>
                    <li> <a href="{{ url('component-list-groups') }}"><i class="bx bx-right-arrow-alt"></i>List
                            Groups</a>
                    </li>
                    <li> <a href="{{ url('component-media-object') }}"><i class="bx bx-right-arrow-alt"></i>Media
                            Objects</a>
                    </li>
                    <li> <a href="{{ url('component-modals') }}"><i class="bx bx-right-arrow-alt"></i>Modals</a>
                    </li>
                    <li> <a href="{{ url('component-navs-tabs') }}"><i class="bx bx-right-arrow-alt"></i>Navs &
                            Tabs</a>
                    </li>
                    <li> <a href="{{ url('component-navbar') }}"><i class="bx bx-right-arrow-alt"></i>Navbar</a>
                    </li>
                    <li> <a href="{{ url('component-popovers-tooltips') }}"><i
                                class="bx bx-right-arrow-alt"></i>Popovers & Tooltips</a>
                    </li>
                    <li> <a href="{{ url('component-progress-bars') }}"><i
                                class="bx bx-right-arrow-alt"></i>Progress</a>
                    </li>
                    <li> <a href="{{ url('component-spinners') }}"><i class="bx bx-right-arrow-alt"></i>Spinners</a>
                    </li>
                    <li> <a href="{{ url('component-notifications') }}"><i
                                class="bx bx-right-arrow-alt"></i>Notifications</a>
                    </li>
                    <li> <a href="{{ url('component-avtars-chips') }}"><i class="bx bx-right-arrow-alt"></i>Avatrs &
                            Chips</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon"><i class="bx bx-lock"></i>
                    </div>
                    <div class="menu-title">Authentication</div>
                </a>
                <ul>
                    <li> <a href="{{ url('authentication-signin') }}" target="_blank"><i
                                class="bx bx-right-arrow-alt"></i>Sign In</a>
                    </li>
                    <li> <a href="{{ url('authentication-signup') }}" target="_blank"><i
                                class="bx bx-right-arrow-alt"></i>Sign Up</a>
                    </li>
                    <li> <a href="{{ url('authentication-signin-with-header-footer') }}" target="_blank"><i
                                class="bx bx-right-arrow-alt"></i>Sign In with Header & Footer</a>
                    </li>
                    <li> <a href="{{ url('authentication-signup-with-header-footer') }}" target="_blank"><i
                                class="bx bx-right-arrow-alt"></i>Sign Up with Header & Footer</a>
                    </li>
                    <li> <a href="{{ url('authentication-forgot-password') }}" target="_blank"><i
                                class="bx bx-right-arrow-alt"></i>Forgot Password</a>
                    </li>
                    <li> <a href="{{ url('authentication-reset-password') }}" target="_blank"><i
                                class="bx bx-right-arrow-alt"></i>Reset Password</a>
                    </li>
                    <li> <a href="{{ url('authentication-lock-screen') }}" target="_blank"><i
                                class="bx bx-right-arrow-alt"></i>Lock Screen</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon icon-color-6"> <i class="bx bx-donate-blood"></i>
                    </div>
                    <div class="menu-title">Pages</div>
                </a>
                <ul>
                    <li> <a href="{{ url('user-profile') }}"><i class="bx bx-right-arrow-alt"></i>User Profile</a>
                    </li>
                    <li> <a href="{{ url('timeline') }}"><i class="bx bx-right-arrow-alt"></i>Timeline</a>
                    </li>
                    <li> <a href="{{ url('pricing-table') }}"><i class="bx bx-right-arrow-alt"></i>Pricing</a>
                    </li>
                    <li> <a class="has-arrow" href="javascript:;"><i class="bx bx-right-arrow-alt"></i>Errors</a>
                        <ul>
                            <li> <a href="{{ url('errors-404-error') }}"><i class="bx bx-right-arrow-alt"></i>404
                                    Error</a>
                            </li>
                            <li> <a href="{{ url('errors-500-error') }}"><i class="bx bx-right-arrow-alt"></i>500
                                    Error</a>
                            </li>
                            <li> <a href="{{ url('errors-coming-soon') }}"><i
                                        class="bx bx-right-arrow-alt"></i>Coming Soon</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:;">
                    <div class="parent-icon"><i class='bx bx-message-square-edit'></i>
                    </div>
                    <div class="menu-title">Forms</div>
                </a>
                <ul>
                    <li> <a href="{{ url('form-elements') }}"><i class="bx bx-right-arrow-alt"></i>Form Elements</a>
                    </li>
                    <li> <a href="{{ url('form-input-group') }}"><i class="bx bx-right-arrow-alt"></i>Input
                            Groups</a>
                    </li>
                    <li> <a href="{{ url('form-layouts') }}"><i class="bx bx-right-arrow-alt"></i>Forms Layouts</a>
                    </li>
                    <li> <a href="{{ url('form-validations') }}"><i class="bx bx-right-arrow-alt"></i>Form
                            Validation</a>
                    </li>
                    <li> <a href="{{ url('form-wizard') }}"><i class="bx bx-right-arrow-alt"></i>Form Wizard</a>
                    </li>
                    <li> <a href="{{ url('form-text-editor') }}"><i class="bx bx-right-arrow-alt"></i>Text Editor</a>
                    </li>
                    <li> <a href="{{ url('form-file-upload') }}"><i class="bx bx-right-arrow-alt"></i>File Upload</a>
                    </li>
                    <li> <a href="{{ url('form-date-time-pickes') }}"><i class="bx bx-right-arrow-alt"></i>Date
                            Pickers</a>
                    </li>
                    <li> <a href="{{ url('form-select2') }}"><i class="bx bx-right-arrow-alt"></i>Select2</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav> --}}
</div>
@stack('menu_end')
