@stack($name . '_input_start')

<div class="form-group {{ $col }}{{ isset($attributes['required']) ? ' required' : '' }}{{ isset($attributes['readonly']) ? ' readonly' : '' }}{{ isset($attributes['disabled']) ? ' disabled' : '' }}"
    @if (isset($attributes['show'])) v-if="{{ $attributes['show'] }}" @endif
    @if (isset($attributes[':disabled'])) :class="[{'disabled' : {{ $attributes[':disabled'] }}}, {'has-error': {{ isset($attributes['v-error']) ? $attributes['v-error'] : 'form.errors.get("' . $name . '")' }}}]"
        @else
        :class="[{'has-error': {{ isset($attributes['v-error']) ? $attributes['v-error'] : 'form.errors.get("' . $name . '")' }}}]" @endif>

    <div class="col-12">
        @if (!empty($text))
            {!! Form::label($name, $text, ['class' => 'form-label']) !!}
        @endif

        <div class="input-group input-group-merge {{ $group_class }}">
            {!! Form::text(
                $name,
                $value,
                array_merge(
                    [
                        'class' => 'form-control border-end-0',
                        'data-name' => $name,
                        'data-value' => $value,
                        'placeholder' => trans('general.form.enter', ['field' => $text]),
                        'v-model' => !empty($attributes['v-model'])
                            ? $attributes['v-model']
                            : (!empty($attributes['data-field'])
                                ? 'form.' . $attributes['data-field'] . '.' . $name
                                : 'form.' . $name),
                    ],
                    $attributes,
                ),
            ) !!}
            <a href="javascript:;" class="input-group-text bg-transparent">
                <i class="bx bx-{{ $icon }}"></i>
            </a>
        </div>

    </div>
    <div class="invalid-feedback d-block"
        v-if="{{ isset($attributes['v-error']) ? $attributes['v-error'] : 'form.errors.has("' . $name . '")' }}"
        v-html="{{ isset($attributes['v-error-message']) ? $attributes['v-error-message'] : 'form.errors.get("' . $name . '")' }}">
    </div>
</div>

@stack($name . '_input_end')
