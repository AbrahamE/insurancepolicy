<?php

return [

    'payment_made'      => 'Pago realizado',
    'paid_to'           => 'Pagado/a a',
    'related_bill'      => 'Factura relacionada',
    'create_payment'    => 'Crear pago',

];
