<?php

return [
  
    'invoice_number'        => 'Número de Factura',
    'invoice_date'          => 'Fecha de Factura',
    'invoice_amount'        => 'Importe de la factura',
    'total_price'           => 'Precio Total',
    'due_date'              => 'Fecha de vencimiento',
    'order_number'          => 'Nº Pedido',
    'bill_to'               => 'Facturar a',

    'quantity'              => 'Cantidad',
    'price'                 => 'Precio',
    'sub_total'             => 'Subtotal',
    'discount'              => 'Descuento',
    'item_discount'         => 'Descuento de línea',
    'tax_total'             => 'Total Impuestos',
    'total'                 => 'Total ',

    'item_name'             => 'Nombre del artículo | Nombres de artículo',

    'show_discount'         => ':discount% Descuento',
    'add_discount'          => 'Agregar Descuento',
    'discount_desc'         => 'de subtotal',

    'payment_due'           => 'Vencimiento de pago',
    'paid'                  => 'Pagado',
    'histories'             => 'Historias',
    'payments'              => 'Pagos',
    'add_payment'           => 'Añadir pago',
    'mark_paid'             => 'Marcar Como Pagada',
    'mark_sent'             => 'Marcar Como Enviada',
    'mark_viewed'           => 'Marcar como visto',
    'mark_cancelled'        => 'Marcar como Cancelada',
    'download_pdf'          => 'Descargar PDF',
    'send_mail'             => 'Enviar Email',
    'all_invoices'          => 'Inicie sesión para ver todas las facturas',
    'create_invoice'        => 'Crear Factura',
    'send_invoice'          => 'Enviar Factura',
    'get_paid'              => 'Recibir Pago',
    'accept_payments'       => 'Aceptar Pagos Online',

    'template' => [
        'invoice'           => 'Factura',
        'description'       => 'Descripción',
        'descriptions'      => 'Nombre, Apellido o Razon Social',
        'contact_phone'     => 'Numero De Telefono',
        'tax_residence'     => 'Domicilio Fiscal',
        'client'            => 'Cliente',
        'quantity'          => 'cant',
        'unit_price'        => 'Precio unitario',
        'note'              => 'Nota',
        'signature'         => 'Firma'
    ],

    'messages' => [
        'negative_inventory_control' => 'No tiene permiso para Facturar con inventario en negativo.',
        'email_required'    => 'Ninguna dirección de correo electrónico para este cliente!',
        'draft'             => 'Esta es una factura <b>BORRADOR</b> y se reflejará en los gráficos luego de que sea enviada.',
        'no_items_stock'    => 'No tienes Articulos disponibles en el inventario.',
        'bank_off'          => 'Debe configurar una Cuenta predeterminada.',

        'status' => [
            'created'       => 'Creada el :date',
            'viewed'        => 'Visto',
            'send' => [
                'draft'     => 'No enviada',
                'sent'      => 'Enviada el :date',
            ],
            'paid' => [
                'await'     => 'Pendiente de pago',
            ],
        ],
    ],

];
