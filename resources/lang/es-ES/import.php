<?php

return [

    'import'                => 'Importar',
    'title'                 => 'Importar :type',
    'limitations'           => 'Tipo de archivo permitido: :extensions<br>Filas máximas permitidas:  :row_limit',
    'sample_file'           => 'Puedes <a target="_blank" href=":download_link"><strong>descargar</strong></a> el archivo de muestra y rellénalo con tus datos.',
    'message'               => 'Formatos permitidos: XLS, XLSX. Por favor, <a target="_blank" href=":link"> <strong>descargue</strong></a> el archivo de ejemplo.',

];
