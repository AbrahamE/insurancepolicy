<?php

return [

    'version'               => 'Versión',
    'powered'               => 'Powered By DANUBE',
    'link'                  => 'http://danube.services',
    'software'              => 'Software de Contabilidad Libre',

];
