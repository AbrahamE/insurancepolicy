<?php

return [

    'code'                  => 'Código',
    'rate'                  => 'Cotización',
    'default'               => 'Moneda Predeterminada',
    'decimal_mark'          => 'Punto decimal',
    'thousands_separator'   => 'Separador de miles',
    'precision'             => 'Precisión',
    'conversion'            => 'Conversión de divisas:  :price (:currency_code) en :currency_rate',
    'symbol' => [
        'symbol'            => 'Símbolo',
        'position'          => 'Posición de Símbolo',
        'before'            => 'Antes del importe',
        'after'             => 'Despues del importe',
    ]

];
