<?php

return [

    'revenue_received'      => 'Ingresos recibidos',
    'paid_by'               => 'Pagado Por',
    'related_invoice'       => 'Factura relacionada',
    'create_revenue'        => 'Crear Ingresos',

];
